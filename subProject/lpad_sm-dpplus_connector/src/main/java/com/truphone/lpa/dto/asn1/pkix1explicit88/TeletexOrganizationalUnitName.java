package com.truphone.lpa.dto.asn1.pkix1explicit88;

import org.openmuc.jasn1.ber.types.string.BerTeletexString;
















public class TeletexOrganizationalUnitName
  extends BerTeletexString
{
  private static final long serialVersionUID = 1L;
  
  public TeletexOrganizationalUnitName() {}
  
  public TeletexOrganizationalUnitName(byte[] value)
  {
    super(value);
  }
}
