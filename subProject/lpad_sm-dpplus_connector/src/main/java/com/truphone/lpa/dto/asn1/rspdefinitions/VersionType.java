package com.truphone.lpa.dto.asn1.rspdefinitions;

import org.openmuc.jasn1.ber.types.BerOctetString;




















public class VersionType
  extends BerOctetString
{
  private static final long serialVersionUID = 1L;
  
  public VersionType() {}
  
  public VersionType(byte[] value)
  {
    super(value);
  }
}
