package com.truphone.lpa.dto.asn1.pkix1implicit88;

import org.openmuc.jasn1.ber.types.BerOctetString;






















public class KeyIdentifier
  extends BerOctetString
{
  private static final long serialVersionUID = 1L;
  
  public KeyIdentifier() {}
  
  public KeyIdentifier(byte[] value)
  {
    super(value);
  }
}
