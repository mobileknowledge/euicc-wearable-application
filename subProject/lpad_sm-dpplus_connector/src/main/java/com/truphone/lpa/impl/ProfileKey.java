package com.truphone.lpa.impl;

public enum ProfileKey {
    ICCID,
    STATE,
    NAME,
    PROVIDER_NAME,
    PROFILE_NICKNAME,
    PROFILE_CLASS,
    PROFILE_ICON
}
