package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerTag;

















public class CtxParams1
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  private CtxParamsForCommonAuthentication ctxParamsForCommonAuthentication = null;
  
  public CtxParams1() {}
  
  public CtxParams1(byte[] code)
  {
    this.code = code;
  }
  
  public void setCtxParamsForCommonAuthentication(CtxParamsForCommonAuthentication ctxParamsForCommonAuthentication) {
    this.ctxParamsForCommonAuthentication = ctxParamsForCommonAuthentication;
  }
  
  public CtxParamsForCommonAuthentication getCtxParamsForCommonAuthentication() {
    return ctxParamsForCommonAuthentication;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (ctxParamsForCommonAuthentication != null) {
      codeLength += ctxParamsForCommonAuthentication.encode(os, false);
      
      os.write(160);
      codeLength++;
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, null);
  }
  
  public int decode(InputStream is, BerTag berTag) throws IOException
  {
    int codeLength = 0;
    BerTag passedTag = berTag;
    
    if (berTag == null) {
      berTag = new BerTag();
      codeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 32, 0)) {
      ctxParamsForCommonAuthentication = new CtxParamsForCommonAuthentication();
      codeLength += ctxParamsForCommonAuthentication.decode(is, false);
      return codeLength;
    }
    
    if (passedTag != null) {
      return 0;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (ctxParamsForCommonAuthentication != null) {
      sb.append("ctxParamsForCommonAuthentication: ");
      ctxParamsForCommonAuthentication.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    sb.append("<none>");
  }
}
