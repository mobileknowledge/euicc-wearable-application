package com.truphone.lpa.dto.asn1.rspdefinitions;

import org.openmuc.jasn1.ber.types.BerOctetString;




















public class Octet32
  extends BerOctetString
{
  private static final long serialVersionUID = 1L;
  
  public Octet32() {}
  
  public Octet32(byte[] value)
  {
    super(value);
  }
}
