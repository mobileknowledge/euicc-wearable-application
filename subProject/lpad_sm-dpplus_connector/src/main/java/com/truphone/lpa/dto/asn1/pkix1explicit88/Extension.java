//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerBoolean;
import org.openmuc.jasn1.ber.types.BerObjectIdentifier;
import org.openmuc.jasn1.ber.types.BerOctetString;

public class Extension implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  public byte[] code = null;
  private BerObjectIdentifier extnID = null;
  private BerBoolean critical = null;
  private BerOctetString extnValue = null;

  public Extension() {
  }

  public Extension(byte[] code) {
    this.code = code;
  }

  public void setExtnID(BerObjectIdentifier extnID) {
    this.extnID = extnID;
  }

  public BerObjectIdentifier getExtnID() {
    return this.extnID;
  }

  public void setCritical(BerBoolean critical) {
    this.critical = critical;
  }

  public BerBoolean getCritical() {
    return this.critical;
  }

  public void setExtnValue(BerOctetString extnValue) {
    this.extnValue = extnValue;
  }

  public BerOctetString getExtnValue() {
    return this.extnValue;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int codeLength;
    if (this.code == null) {
      codeLength = 0;
      codeLength = codeLength + this.extnValue.encode(os, true);
      if (this.critical != null) {
        codeLength += this.critical.encode(os, true);
      }

      codeLength += this.extnID.encode(os, true);
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }

      return codeLength;
    } else {
      for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
        os.write(this.code[codeLength]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;
    codeLength += totalLength;
    subCodeLength = subCodeLength + berTag.decode(is);
    if (berTag.equals(BerObjectIdentifier.tag)) {
      this.extnID = new BerObjectIdentifier();
      subCodeLength += this.extnID.decode(is, false);
      subCodeLength += berTag.decode(is);
      if (berTag.equals(BerBoolean.tag)) {
        this.critical = new BerBoolean();
        subCodeLength += this.critical.decode(is, false);
        subCodeLength += berTag.decode(is);
      }

      if (berTag.equals(BerOctetString.tag)) {
        this.extnValue = new BerOctetString();
        subCodeLength += this.extnValue.decode(is, false);
        if (subCodeLength == totalLength) {
          return codeLength;
        }
      }

      throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
    } else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    sb.append("\n");

    int i;
    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.extnID != null) {
      sb.append("extnID: ").append(this.extnID);
    } else {
      sb.append("extnID: <empty-required-field>");
    }

    if (this.critical != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("critical: ").append(this.critical);
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.extnValue != null) {
      sb.append("extnValue: ").append(this.extnValue);
    } else {
      sb.append("extnValue: <empty-required-field>");
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }
}
