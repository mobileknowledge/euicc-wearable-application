//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;

public class TBSCertList implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  public byte[] code = null;
  private Version version = null;
  private AlgorithmIdentifier signature = null;
  private Name issuer = null;
  private Time thisUpdate = null;
  private Time nextUpdate = null;
  private TBSCertList.RevokedCertificates revokedCertificates = null;
  private Extensions crlExtensions = null;

  public TBSCertList() {
  }

  public TBSCertList(byte[] code) {
    this.code = code;
  }

  public void setVersion(Version version) {
    this.version = version;
  }

  public Version getVersion() {
    return this.version;
  }

  public void setSignature(AlgorithmIdentifier signature) {
    this.signature = signature;
  }

  public AlgorithmIdentifier getSignature() {
    return this.signature;
  }

  public void setIssuer(Name issuer) {
    this.issuer = issuer;
  }

  public Name getIssuer() {
    return this.issuer;
  }

  public void setThisUpdate(Time thisUpdate) {
    this.thisUpdate = thisUpdate;
  }

  public Time getThisUpdate() {
    return this.thisUpdate;
  }

  public void setNextUpdate(Time nextUpdate) {
    this.nextUpdate = nextUpdate;
  }

  public Time getNextUpdate() {
    return this.nextUpdate;
  }

  public void setRevokedCertificates(TBSCertList.RevokedCertificates revokedCertificates) {
    this.revokedCertificates = revokedCertificates;
  }

  public TBSCertList.RevokedCertificates getRevokedCertificates() {
    return this.revokedCertificates;
  }

  public void setCrlExtensions(Extensions crlExtensions) {
    this.crlExtensions = crlExtensions;
  }

  public Extensions getCrlExtensions() {
    return this.crlExtensions;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int codeLength;
    if (this.code == null) {
      codeLength = 0;
      if (this.crlExtensions != null) {
        int sublength = this.crlExtensions.encode(os, true);
        codeLength += sublength;
        codeLength += BerLength.encodeLength(os, sublength);
        os.write(160);
        ++codeLength;
      }

      if (this.revokedCertificates != null) {
        codeLength += this.revokedCertificates.encode(os, true);
      }

      if (this.nextUpdate != null) {
        codeLength += this.nextUpdate.encode(os);
      }

      codeLength += this.thisUpdate.encode(os);
      codeLength += this.issuer.encode(os);
      codeLength += this.signature.encode(os, true);
      if (this.version != null) {
        codeLength += this.version.encode(os, true);
      }

      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }

      return codeLength;
    } else {
      for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
        os.write(this.code[codeLength]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;
    codeLength += totalLength;
    subCodeLength = subCodeLength + berTag.decode(is);
    if (berTag.equals(Version.tag)) {
      this.version = new Version();
      subCodeLength += this.version.decode(is, false);
      subCodeLength += berTag.decode(is);
    }

    if (berTag.equals(AlgorithmIdentifier.tag)) {
      this.signature = new AlgorithmIdentifier();
      subCodeLength += this.signature.decode(is, false);
      subCodeLength += berTag.decode(is);
      this.issuer = new Name();
      subCodeLength += this.issuer.decode(is, berTag);
      subCodeLength += berTag.decode(is);
      this.thisUpdate = new Time();
      subCodeLength += this.thisUpdate.decode(is, berTag);
      if (subCodeLength == totalLength) {
        return codeLength;
      } else {
        subCodeLength += berTag.decode(is);
        this.nextUpdate = new Time();
        int choiceDecodeLength = this.nextUpdate.decode(is, berTag);
        if (choiceDecodeLength != 0) {
          subCodeLength += choiceDecodeLength;
          if (subCodeLength == totalLength) {
            return codeLength;
          }

          subCodeLength += berTag.decode(is);
        } else {
          this.nextUpdate = null;
        }

        if (berTag.equals(TBSCertList.RevokedCertificates.tag)) {
          this.revokedCertificates = new TBSCertList.RevokedCertificates();
          subCodeLength += this.revokedCertificates.decode(is, false);
          if (subCodeLength == totalLength) {
            return codeLength;
          }

          subCodeLength += berTag.decode(is);
        }

        if (berTag.equals(128, 32, 0)) {
          subCodeLength += length.decode(is);
          this.crlExtensions = new Extensions();
          subCodeLength += this.crlExtensions.decode(is, true);
          if (subCodeLength == totalLength) {
            return codeLength;
          }
        }

        throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
      }
    } else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    boolean firstSelectedElement = true;
    int i;
    if (this.version != null) {
      sb.append("\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("version: ").append(this.version);
      firstSelectedElement = false;
    }

    if (!firstSelectedElement) {
      sb.append(",\n");
    }

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.signature != null) {
      sb.append("signature: ");
      this.signature.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("signature: <empty-required-field>");
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.issuer != null) {
      sb.append("issuer: ");
      this.issuer.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("issuer: <empty-required-field>");
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.thisUpdate != null) {
      sb.append("thisUpdate: ");
      this.thisUpdate.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("thisUpdate: <empty-required-field>");
    }

    if (this.nextUpdate != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("nextUpdate: ");
      this.nextUpdate.appendAsString(sb, indentLevel + 1);
    }

    if (this.revokedCertificates != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("revokedCertificates: ");
      this.revokedCertificates.appendAsString(sb, indentLevel + 1);
    }

    if (this.crlExtensions != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("crlExtensions: ");
      this.crlExtensions.appendAsString(sb, indentLevel + 1);
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }

  public static class RevokedCertificates implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final BerTag tag = new BerTag(0, 32, 16);
    public byte[] code = null;
    private List<TBSCertList.RevokedCertificates.SEQUENCE> seqOf = null;

    public RevokedCertificates() {
      this.seqOf = new ArrayList();
    }

    public RevokedCertificates(byte[] code) {
      this.code = code;
    }

    public List<TBSCertList.RevokedCertificates.SEQUENCE> getSEQUENCE() {
      if (this.seqOf == null) {
        this.seqOf = new ArrayList();
      }

      return this.seqOf;
    }

    public int encode(BerByteArrayOutputStream os) throws IOException {
      return this.encode(os, true);
    }

    public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
      int codeLength;
      if (this.code != null) {
        for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
          os.write(this.code[codeLength]);
        }

        return withTag ? tag.encode(os) + this.code.length : this.code.length;
      } else {
        codeLength = 0;

        for(int i = this.seqOf.size() - 1; i >= 0; --i) {
          codeLength += this.seqOf.get(i).encode(os, true);
        }

        codeLength += BerLength.encodeLength(os, codeLength);
        if (withTag) {
          codeLength += tag.encode(os);
        }

        return codeLength;
      }
    }

    public int decode(InputStream is) throws IOException {
      return this.decode(is, true);
    }

    public int decode(InputStream is, boolean withTag) throws IOException {
      int codeLength = 0;
      int subCodeLength = 0;
      if (withTag) {
        codeLength += tag.decodeAndCheck(is);
      }

      BerLength length = new BerLength();
      codeLength += length.decode(is);
      int totalLength = length.val;

      while(subCodeLength < totalLength) {
        TBSCertList.RevokedCertificates.SEQUENCE element = new TBSCertList.RevokedCertificates.SEQUENCE();
        subCodeLength += element.decode(is, true);
        this.seqOf.add(element);
      }

      if (subCodeLength != totalLength) {
        throw new IOException("Decoded SequenceOf or SetOf has wrong length. Expected " + totalLength + " but has " + subCodeLength);
      } else {
        codeLength += subCodeLength;
        return codeLength;
      }
    }

    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      this.encode(os, false);
      this.code = os.getArray();
    }

    public String toString() {
      StringBuilder sb = new StringBuilder();
      this.appendAsString(sb, 0);
      return sb.toString();
    }

    public void appendAsString(StringBuilder sb, int indentLevel) {
      sb.append("{\n");

      int i;
      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      if (this.seqOf == null) {
        sb.append("null");
      } else {
        Iterator<TBSCertList.RevokedCertificates.SEQUENCE> it = this.seqOf.iterator();
        if (it.hasNext()) {
          it.next().appendAsString(sb, indentLevel + 1);

          while(it.hasNext()) {
            sb.append(",\n");

            for( i = 0; i < indentLevel + 1; ++i) {
              sb.append("\t");
            }

            it.next().appendAsString(sb, indentLevel + 1);
          }
        }
      }

      sb.append("\n");

      for(i = 0; i < indentLevel; ++i) {
        sb.append("\t");
      }

      sb.append("}");
    }

    public static class SEQUENCE implements Serializable {
      private static final long serialVersionUID = 1L;
      public static final BerTag tag = new BerTag(0, 32, 16);
      public byte[] code = null;
      private CertificateSerialNumber userCertificate = null;
      private Time revocationDate = null;
      private Extensions crlEntryExtensions = null;

      public SEQUENCE() {
      }

      public SEQUENCE(byte[] code) {
        this.code = code;
      }

      public void setUserCertificate(CertificateSerialNumber userCertificate) {
        this.userCertificate = userCertificate;
      }

      public CertificateSerialNumber getUserCertificate() {
        return this.userCertificate;
      }

      public void setRevocationDate(Time revocationDate) {
        this.revocationDate = revocationDate;
      }

      public Time getRevocationDate() {
        return this.revocationDate;
      }

      public void setCrlEntryExtensions(Extensions crlEntryExtensions) {
        this.crlEntryExtensions = crlEntryExtensions;
      }

      public Extensions getCrlEntryExtensions() {
        return this.crlEntryExtensions;
      }

      public int encode(BerByteArrayOutputStream os) throws IOException {
        return this.encode(os, true);
      }

      public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
        int codeLength;
        if (this.code == null) {
          codeLength = 0;
          if (this.crlEntryExtensions != null) {
            codeLength += this.crlEntryExtensions.encode(os, true);
          }

          codeLength += this.revocationDate.encode(os);
          codeLength += this.userCertificate.encode(os, true);
          codeLength += BerLength.encodeLength(os, codeLength);
          if (withTag) {
            codeLength += tag.encode(os);
          }

          return codeLength;
        } else {
          for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
            os.write(this.code[codeLength]);
          }

          return withTag ? tag.encode(os) + this.code.length : this.code.length;
        }
      }

      public int decode(InputStream is) throws IOException {
        return this.decode(is, true);
      }

      public int decode(InputStream is, boolean withTag) throws IOException {
        int codeLength = 0;
        int subCodeLength = 0;
        BerTag berTag = new BerTag();
        if (withTag) {
          codeLength += tag.decodeAndCheck(is);
        }

        BerLength length = new BerLength();
        codeLength += length.decode(is);
        int totalLength = length.val;
        codeLength += totalLength;
        subCodeLength = subCodeLength + berTag.decode(is);
        if (berTag.equals(CertificateSerialNumber.tag)) {
          this.userCertificate = new CertificateSerialNumber();
          subCodeLength += this.userCertificate.decode(is, false);
          subCodeLength += berTag.decode(is);
          this.revocationDate = new Time();
          subCodeLength += this.revocationDate.decode(is, berTag);
          if (subCodeLength == totalLength) {
            return codeLength;
          } else {
            subCodeLength += berTag.decode(is);
            if (berTag.equals(Extensions.tag)) {
              this.crlEntryExtensions = new Extensions();
              subCodeLength += this.crlEntryExtensions.decode(is, false);
              if (subCodeLength == totalLength) {
                return codeLength;
              }
            }

            throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
          }
        } else {
          throw new IOException("Tag does not match the mandatory sequence element tag.");
        }
      }

      public void encodeAndSave(int encodingSizeGuess) throws IOException {
        BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
        this.encode(os, false);
        this.code = os.getArray();
      }

      public String toString() {
        StringBuilder sb = new StringBuilder();
        this.appendAsString(sb, 0);
        return sb.toString();
      }

      public void appendAsString(StringBuilder sb, int indentLevel) {
        sb.append("{");
        sb.append("\n");

        int i;
        for(i = 0; i < indentLevel + 1; ++i) {
          sb.append("\t");
        }

        if (this.userCertificate != null) {
          sb.append("userCertificate: ").append(this.userCertificate);
        } else {
          sb.append("userCertificate: <empty-required-field>");
        }

        sb.append(",\n");

        for(i = 0; i < indentLevel + 1; ++i) {
          sb.append("\t");
        }

        if (this.revocationDate != null) {
          sb.append("revocationDate: ");
          this.revocationDate.appendAsString(sb, indentLevel + 1);
        } else {
          sb.append("revocationDate: <empty-required-field>");
        }

        if (this.crlEntryExtensions != null) {
          sb.append(",\n");

          for(i = 0; i < indentLevel + 1; ++i) {
            sb.append("\t");
          }

          sb.append("crlEntryExtensions: ");
          this.crlEntryExtensions.appendAsString(sb, indentLevel + 1);
        }

        sb.append("\n");

        for(i = 0; i < indentLevel; ++i) {
          sb.append("\t");
        }

        sb.append("}");
      }
    }
  }
}
