package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;













public class ProfileInfoListRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public ProfileInfoListRequest() {}
  
  public static class SearchCriteria
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    public byte[] code = null;
    private OctetTo16 isdpAid = null;
    private Iccid iccid = null;
    private ProfileClass profileClass = null;
    
    public SearchCriteria() {}
    
    public SearchCriteria(byte[] code)
    {
      this.code = code;
    }
    
    public void setIsdpAid(OctetTo16 isdpAid) {
      this.isdpAid = isdpAid;
    }
    
    public OctetTo16 getIsdpAid() {
      return isdpAid;
    }
    
    public void setIccid(Iccid iccid) {
      this.iccid = iccid;
    }
    
    public Iccid getIccid() {
      return iccid;
    }
    
    public void setProfileClass(ProfileClass profileClass) {
      this.profileClass = profileClass;
    }
    
    public ProfileClass getProfileClass() {
      return profileClass;
    }
    
    public int encode(BerByteArrayOutputStream os) throws IOException
    {
      if (code != null) {
        for (int i = code.length - 1; i >= 0; i--) {
          os.write(code[i]);
        }
        return code.length;
      }
      
      int codeLength = 0;
      if (profileClass != null) {
        codeLength += profileClass.encode(os, false);
        
        os.write(149);
        codeLength++;
        return codeLength;
      }
      
      if (iccid != null) {
        codeLength += iccid.encode(os, true);
        return codeLength;
      }
      
      if (isdpAid != null) {
        codeLength += isdpAid.encode(os, false);
        
        os.write(79);
        codeLength++;
        return codeLength;
      }
      
      throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
    }
    
    public int decode(InputStream is) throws IOException {
      return decode(is, null);
    }
    
    public int decode(InputStream is, BerTag berTag) throws IOException
    {
      int codeLength = 0;
      BerTag passedTag = berTag;
      
      if (berTag == null) {
        berTag = new BerTag();
        codeLength += berTag.decode(is);
      }
      
      if (berTag.equals(64, 0, 15)) {
        isdpAid = new OctetTo16();
        codeLength += isdpAid.decode(is, false);
        return codeLength;
      }
      
      if (berTag.equals(Iccid.tag)) {
        iccid = new Iccid();
        codeLength += iccid.decode(is, false);
        return codeLength;
      }
      
      if (berTag.equals(128, 0, 21)) {
        profileClass = new ProfileClass();
        codeLength += profileClass.decode(is, false);
        return codeLength;
      }
      
      if (passedTag != null) {
        return 0;
      }
      
      throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
    }
    
    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      encode(os);
      code = os.getArray();
    }
    
    public String toString() {
      StringBuilder sb = new StringBuilder();
      appendAsString(sb, 0);
      return sb.toString();
    }
    
    public void appendAsString(StringBuilder sb, int indentLevel)
    {
      if (isdpAid != null) {
        sb.append("isdpAid: ").append(isdpAid);
        return;
      }
      
      if (iccid != null) {
        sb.append("iccid: ").append(iccid);
        return;
      }
      
      if (profileClass != null) {
        sb.append("profileClass: ").append(profileClass);
        return;
      }
      
      sb.append("<none>");
    }
  }
  

  public static final BerTag tag = new BerTag(128, 32, 45);
  
  public byte[] code = null;
  private SearchCriteria searchCriteria = null;
  private BerOctetString tagList = null;
  


  public ProfileInfoListRequest(byte[] code)
  {
    this.code = code;
  }
  
  public void setSearchCriteria(SearchCriteria searchCriteria) {
    this.searchCriteria = searchCriteria;
  }
  
  public SearchCriteria getSearchCriteria() {
    return searchCriteria;
  }
  
  public void setTagList(BerOctetString tagList) {
    this.tagList = tagList;
  }
  
  public BerOctetString getTagList() {
    return tagList;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    

    if (tagList != null) {
      codeLength += tagList.encode(os, false);
      
      os.write(92);
      codeLength++;
    }
    
    if (searchCriteria != null) {
      int sublength = searchCriteria.encode(os);
      codeLength += sublength;
      codeLength += BerLength.encodeLength(os, sublength);
      
      os.write(160);
      codeLength++;
    }
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    if (totalLength == 0) {
      return codeLength;
    }
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 32, 0)) {
      subCodeLength += length.decode(is);
      searchCriteria = new SearchCriteria();
      subCodeLength += searchCriteria.decode(is, null);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(64, 0, 28)) {
      tagList = new BerOctetString();
      subCodeLength += tagList.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    boolean firstSelectedElement = true;
    if (searchCriteria != null) {
      sb.append("\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("searchCriteria: ");
      searchCriteria.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }
    
    if (tagList != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("tagList: ").append(tagList);
      firstSelectedElement = false;
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
