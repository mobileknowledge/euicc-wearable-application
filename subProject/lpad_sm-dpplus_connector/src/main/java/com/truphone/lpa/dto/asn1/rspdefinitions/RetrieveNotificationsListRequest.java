package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerInteger;













public class RetrieveNotificationsListRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public RetrieveNotificationsListRequest() {}
  
  public static class SearchCriteria
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    public byte[] code = null;
    private BerInteger seqNumber = null;
    private NotificationEvent profileManagementOperation = null;
    
    public SearchCriteria() {}
    
    public SearchCriteria(byte[] code)
    {
      this.code = code;
    }
    
    public void setSeqNumber(BerInteger seqNumber) {
      this.seqNumber = seqNumber;
    }
    
    public BerInteger getSeqNumber() {
      return seqNumber;
    }
    
    public void setProfileManagementOperation(NotificationEvent profileManagementOperation) {
      this.profileManagementOperation = profileManagementOperation;
    }
    
    public NotificationEvent getProfileManagementOperation() {
      return profileManagementOperation;
    }
    
    public int encode(BerByteArrayOutputStream os) throws IOException
    {
      if (code != null) {
        for (int i = code.length - 1; i >= 0; i--) {
          os.write(code[i]);
        }
        return code.length;
      }
      
      int codeLength = 0;
      if (profileManagementOperation != null) {
        codeLength += profileManagementOperation.encode(os, false);
        
        os.write(129);
        codeLength++;
        return codeLength;
      }
      
      if (seqNumber != null) {
        codeLength += seqNumber.encode(os, false);
        
        os.write(128);
        codeLength++;
        return codeLength;
      }
      
      throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
    }
    
    public int decode(InputStream is) throws IOException {
      return decode(is, null);
    }
    
    public int decode(InputStream is, BerTag berTag) throws IOException
    {
      int codeLength = 0;
      BerTag passedTag = berTag;
      
      if (berTag == null) {
        berTag = new BerTag();
        codeLength += berTag.decode(is);
      }
      
      if (berTag.equals(128, 0, 0)) {
        seqNumber = new BerInteger();
        codeLength += seqNumber.decode(is, false);
        return codeLength;
      }
      
      if (berTag.equals(128, 0, 1)) {
        profileManagementOperation = new NotificationEvent();
        codeLength += profileManagementOperation.decode(is, false);
        return codeLength;
      }
      
      if (passedTag != null) {
        return 0;
      }
      
      throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
    }
    
    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      encode(os);
      code = os.getArray();
    }
    
    public String toString() {
      StringBuilder sb = new StringBuilder();
      appendAsString(sb, 0);
      return sb.toString();
    }
    
    public void appendAsString(StringBuilder sb, int indentLevel)
    {
      if (seqNumber != null) {
        sb.append("seqNumber: ").append(seqNumber);
        return;
      }
      
      if (profileManagementOperation != null) {
        sb.append("profileManagementOperation: ").append(profileManagementOperation);
        return;
      }
      
      sb.append("<none>");
    }
  }
  

  public static final BerTag tag = new BerTag(128, 32, 43);
  
  public byte[] code = null;
  private SearchCriteria searchCriteria = null;
  


  public RetrieveNotificationsListRequest(byte[] code)
  {
    this.code = code;
  }
  
  public void setSearchCriteria(SearchCriteria searchCriteria) {
    this.searchCriteria = searchCriteria;
  }
  
  public SearchCriteria getSearchCriteria() {
    return searchCriteria;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    

    if (searchCriteria != null) {
      int sublength = searchCriteria.encode(os);
      codeLength += sublength;
      codeLength += BerLength.encodeLength(os, sublength);
      
      os.write(160);
      codeLength++;
    }
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    if (totalLength == 0) {
      return codeLength;
    }
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 32, 0)) {
      subCodeLength += length.decode(is);
      searchCriteria = new SearchCriteria();
      subCodeLength += searchCriteria.decode(is, null);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    if (searchCriteria != null) {
      sb.append("\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("searchCriteria: ");
      searchCriteria.appendAsString(sb, indentLevel + 1);
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
