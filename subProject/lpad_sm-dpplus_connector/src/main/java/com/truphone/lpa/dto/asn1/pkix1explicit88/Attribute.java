//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;

public class Attribute implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  public byte[] code = null;
  private AttributeType type = null;
  private Attribute.Values values = null;

  public Attribute() {
  }

  public Attribute(byte[] code) {
    this.code = code;
  }

  public void setType(AttributeType type) {
    this.type = type;
  }

  public AttributeType getType() {
    return this.type;
  }

  public void setValues(Attribute.Values values) {
    this.values = values;
  }

  public Attribute.Values getValues() {
    return this.values;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int i;
    if (this.code == null) {
      int codeLength = 0;
      i = codeLength + this.values.encode(os, true);
      i += this.type.encode(os, true);
      i += BerLength.encodeLength(os, i);
      if (withTag) {
        i += tag.encode(os);
      }

      return i;
    } else {
      for(i = this.code.length - 1; i >= 0; --i) {
        os.write(this.code[i]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;
    codeLength += totalLength;
    subCodeLength = subCodeLength + berTag.decode(is);
    if (berTag.equals(AttributeType.tag)) {
      this.type = new AttributeType();
      subCodeLength += this.type.decode(is, false);
      subCodeLength += berTag.decode(is);
      if (berTag.equals(Attribute.Values.tag)) {
        this.values = new Attribute.Values();
        subCodeLength += this.values.decode(is, false);
        if (subCodeLength == totalLength) {
          return codeLength;
        }
      }

      throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
    } else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    sb.append("\n");

    int i;
    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.type != null) {
      sb.append("type: ").append(this.type);
    } else {
      sb.append("type: <empty-required-field>");
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.values != null) {
      sb.append("values: ");
      this.values.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("values: <empty-required-field>");
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }

  public static class Values implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final BerTag tag = new BerTag(0, 32, 17);
    public byte[] code = null;
    private List<AttributeValue> seqOf = null;

    public Values() {
      this.seqOf = new ArrayList();
    }

    public Values(byte[] code) {
      this.code = code;
    }

    public List<AttributeValue> getAttributeValue() {
      if (this.seqOf == null) {
        this.seqOf = new ArrayList();
      }

      return this.seqOf;
    }

    public int encode(BerByteArrayOutputStream os) throws IOException {
      return this.encode(os, true);
    }

    public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
      int codeLength;
      if (this.code != null) {
        for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
          os.write(this.code[codeLength]);
        }

        return withTag ? tag.encode(os) + this.code.length : this.code.length;
      } else {
        codeLength = 0;

        for(int i = this.seqOf.size() - 1; i >= 0; --i) {
          codeLength += this.seqOf.get(i).encode(os);
        }

        codeLength += BerLength.encodeLength(os, codeLength);
        if (withTag) {
          codeLength += tag.encode(os);
        }

        return codeLength;
      }
    }

    public int decode(InputStream is) throws IOException {
      return this.decode(is, true);
    }

    public int decode(InputStream is, boolean withTag) throws IOException {
      int codeLength = 0;
      int subCodeLength = 0;
      if (withTag) {
        codeLength += tag.decodeAndCheck(is);
      }

      BerLength length = new BerLength();
      codeLength += length.decode(is);
      int totalLength = length.val;

      while(subCodeLength < totalLength) {
        AttributeValue element = new AttributeValue();
        subCodeLength += element.decode(is, null);
        this.seqOf.add(element);
      }

      if (subCodeLength != totalLength) {
        throw new IOException("Decoded SequenceOf or SetOf has wrong length. Expected " + totalLength + " but has " + subCodeLength);
      } else {
        codeLength += subCodeLength;
        return codeLength;
      }
    }

    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      this.encode(os, false);
      this.code = os.getArray();
    }

    public String toString() {
      StringBuilder sb = new StringBuilder();
      this.appendAsString(sb, 0);
      return sb.toString();
    }

    public void appendAsString(StringBuilder sb, int indentLevel) {
      sb.append("{\n");

      int i;
      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      if (this.seqOf == null) {
        sb.append("null");
      } else {
        Iterator<AttributeValue> it = this.seqOf.iterator();
        if (it.hasNext()) {
          sb.append(it.next());

          while(it.hasNext()) {
            sb.append(",\n");

            for( i = 0; i < indentLevel + 1; ++i) {
              sb.append("\t");
            }

            sb.append(it.next());
          }
        }
      }

      sb.append("\n");

      for(i = 0; i < indentLevel; ++i) {
        sb.append("\t");
      }

      sb.append("}");
    }
  }
}
