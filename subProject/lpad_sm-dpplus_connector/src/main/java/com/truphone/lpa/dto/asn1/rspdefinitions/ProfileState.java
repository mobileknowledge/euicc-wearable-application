package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class ProfileState
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public ProfileState() {}
  
  public ProfileState(byte[] code)
  {
    super(code);
  }
  
  public ProfileState(BigInteger value) {
    super(value);
  }
  
  public ProfileState(long value) {
    super(value);
  }
}
