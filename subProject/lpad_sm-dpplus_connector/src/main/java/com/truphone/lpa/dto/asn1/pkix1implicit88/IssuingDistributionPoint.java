package com.truphone.lpa.dto.asn1.pkix1implicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerBoolean;

















public class IssuingDistributionPoint
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private DistributionPointName distributionPoint = null;
  private BerBoolean onlyContainsUserCerts = null;
  private BerBoolean onlyContainsCACerts = null;
  private ReasonFlags onlySomeReasons = null;
  private BerBoolean indirectCRL = null;
  private BerBoolean onlyContainsAttributeCerts = null;
  
  public IssuingDistributionPoint() {}
  
  public IssuingDistributionPoint(byte[] code)
  {
    this.code = code;
  }
  
  public void setDistributionPoint(DistributionPointName distributionPoint) {
    this.distributionPoint = distributionPoint;
  }
  
  public DistributionPointName getDistributionPoint() {
    return distributionPoint;
  }
  
  public void setOnlyContainsUserCerts(BerBoolean onlyContainsUserCerts) {
    this.onlyContainsUserCerts = onlyContainsUserCerts;
  }
  
  public BerBoolean getOnlyContainsUserCerts() {
    return onlyContainsUserCerts;
  }
  
  public void setOnlyContainsCACerts(BerBoolean onlyContainsCACerts) {
    this.onlyContainsCACerts = onlyContainsCACerts;
  }
  
  public BerBoolean getOnlyContainsCACerts() {
    return onlyContainsCACerts;
  }
  
  public void setOnlySomeReasons(ReasonFlags onlySomeReasons) {
    this.onlySomeReasons = onlySomeReasons;
  }
  
  public ReasonFlags getOnlySomeReasons() {
    return onlySomeReasons;
  }
  
  public void setIndirectCRL(BerBoolean indirectCRL) {
    this.indirectCRL = indirectCRL;
  }
  
  public BerBoolean getIndirectCRL() {
    return indirectCRL;
  }
  
  public void setOnlyContainsAttributeCerts(BerBoolean onlyContainsAttributeCerts) {
    this.onlyContainsAttributeCerts = onlyContainsAttributeCerts;
  }
  
  public BerBoolean getOnlyContainsAttributeCerts() {
    return onlyContainsAttributeCerts;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    

    if (onlyContainsAttributeCerts != null) {
      codeLength += onlyContainsAttributeCerts.encode(os, false);
      
      os.write(133);
      codeLength++;
    }
    
    if (indirectCRL != null) {
      codeLength += indirectCRL.encode(os, false);
      
      os.write(132);
      codeLength++;
    }
    
    if (onlySomeReasons != null) {
      codeLength += onlySomeReasons.encode(os, false);
      
      os.write(131);
      codeLength++;
    }
    
    if (onlyContainsCACerts != null) {
      codeLength += onlyContainsCACerts.encode(os, false);
      
      os.write(130);
      codeLength++;
    }
    
    if (onlyContainsUserCerts != null) {
      codeLength += onlyContainsUserCerts.encode(os, false);
      
      os.write(129);
      codeLength++;
    }
    
    if (distributionPoint != null) {
      int sublength = distributionPoint.encode(os);
      codeLength += sublength;
      codeLength += BerLength.encodeLength(os, sublength);
      
      os.write(160);
      codeLength++;
    }
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    if (totalLength == 0) {
      return codeLength;
    }
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 32, 0)) {
      subCodeLength += length.decode(is);
      distributionPoint = new DistributionPointName();
      subCodeLength += distributionPoint.decode(is, null);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 1)) {
      onlyContainsUserCerts = new BerBoolean();
      subCodeLength += onlyContainsUserCerts.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 2)) {
      onlyContainsCACerts = new BerBoolean();
      subCodeLength += onlyContainsCACerts.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 3)) {
      onlySomeReasons = new ReasonFlags();
      subCodeLength += onlySomeReasons.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 4)) {
      indirectCRL = new BerBoolean();
      subCodeLength += indirectCRL.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 5)) {
      onlyContainsAttributeCerts = new BerBoolean();
      subCodeLength += onlyContainsAttributeCerts.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    boolean firstSelectedElement = true;
    if (distributionPoint != null) {
      sb.append("\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("distributionPoint: ");
      distributionPoint.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }
    
    if (onlyContainsUserCerts != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("onlyContainsUserCerts: ").append(onlyContainsUserCerts);
      firstSelectedElement = false;
    }
    
    if (onlyContainsCACerts != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("onlyContainsCACerts: ").append(onlyContainsCACerts);
      firstSelectedElement = false;
    }
    
    if (onlySomeReasons != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("onlySomeReasons: ").append(onlySomeReasons);
      firstSelectedElement = false;
    }
    
    if (indirectCRL != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("indirectCRL: ").append(indirectCRL);
      firstSelectedElement = false;
    }
    
    if (onlyContainsAttributeCerts != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("onlyContainsAttributeCerts: ").append(onlyContainsAttributeCerts);
      firstSelectedElement = false;
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
