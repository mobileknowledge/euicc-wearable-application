package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;















public class CancelSessionResponseOk
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private EuiccCancelSessionSigned euiccCancelSessionSigned = null;
  private BerOctetString euiccCancelSessionSignature = null;
  
  public CancelSessionResponseOk() {}
  
  public CancelSessionResponseOk(byte[] code)
  {
    this.code = code;
  }
  
  public void setEuiccCancelSessionSigned(EuiccCancelSessionSigned euiccCancelSessionSigned) {
    this.euiccCancelSessionSigned = euiccCancelSessionSigned;
  }
  
  public EuiccCancelSessionSigned getEuiccCancelSessionSigned() {
    return euiccCancelSessionSigned;
  }
  
  public void setEuiccCancelSessionSignature(BerOctetString euiccCancelSessionSignature) {
    this.euiccCancelSessionSignature = euiccCancelSessionSignature;
  }
  
  public BerOctetString getEuiccCancelSessionSignature() {
    return euiccCancelSessionSignature;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += euiccCancelSessionSignature.encode(os, false);
    
    os.write(55);
    os.write(95);
    codeLength += 2;
    
    codeLength += euiccCancelSessionSigned.encode(os, true);
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(EuiccCancelSessionSigned.tag)) {
      euiccCancelSessionSigned = new EuiccCancelSessionSigned();
      subCodeLength += euiccCancelSessionSigned.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(64, 0, 55)) {
      euiccCancelSessionSignature = new BerOctetString();
      subCodeLength += euiccCancelSessionSignature.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccCancelSessionSigned != null) {
      sb.append("euiccCancelSessionSigned: ");
      euiccCancelSessionSigned.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("euiccCancelSessionSigned: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccCancelSessionSignature != null) {
      sb.append("euiccCancelSessionSignature: ").append(euiccCancelSessionSignature);
    }
    else {
      sb.append("euiccCancelSessionSignature: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
