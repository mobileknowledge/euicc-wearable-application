package com.truphone.lpa.dto.asn1.pkix1implicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;


















public class DistributionPoint
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private DistributionPointName distributionPoint = null;
  private ReasonFlags reasons = null;
  private GeneralNames cRLIssuer = null;
  
  public DistributionPoint() {}
  
  public DistributionPoint(byte[] code)
  {
    this.code = code;
  }
  
  public void setDistributionPoint(DistributionPointName distributionPoint) {
    this.distributionPoint = distributionPoint;
  }
  
  public DistributionPointName getDistributionPoint() {
    return distributionPoint;
  }
  
  public void setReasons(ReasonFlags reasons) {
    this.reasons = reasons;
  }
  
  public ReasonFlags getReasons() {
    return reasons;
  }
  
  public void setCRLIssuer(GeneralNames cRLIssuer) {
    this.cRLIssuer = cRLIssuer;
  }
  
  public GeneralNames getCRLIssuer() {
    return cRLIssuer;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    

    if (cRLIssuer != null) {
      codeLength += cRLIssuer.encode(os, false);
      
      os.write(162);
      codeLength++;
    }
    
    if (reasons != null) {
      codeLength += reasons.encode(os, false);
      
      os.write(129);
      codeLength++;
    }
    
    if (distributionPoint != null) {
      int sublength = distributionPoint.encode(os);
      codeLength += sublength;
      codeLength += BerLength.encodeLength(os, sublength);
      
      os.write(160);
      codeLength++;
    }
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    if (totalLength == 0) {
      return codeLength;
    }
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 32, 0)) {
      subCodeLength += length.decode(is);
      distributionPoint = new DistributionPointName();
      subCodeLength += distributionPoint.decode(is, null);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 1)) {
      reasons = new ReasonFlags();
      subCodeLength += reasons.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 32, 2)) {
      cRLIssuer = new GeneralNames();
      subCodeLength += cRLIssuer.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    boolean firstSelectedElement = true;
    if (distributionPoint != null) {
      sb.append("\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("distributionPoint: ");
      distributionPoint.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }
    
    if (reasons != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("reasons: ").append(reasons);
      firstSelectedElement = false;
    }
    
    if (cRLIssuer != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("cRLIssuer: ");
      cRLIssuer.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
