package com.truphone.lpa.dto.asn1.rspdefinitions;

import com.truphone.lpa.dto.asn1.pkix1explicit88.Certificate;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;














public class PrepareDownloadRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(128, 32, 33);
  
  public byte[] code = null;
  private SmdpSigned2 smdpSigned2 = null;
  private BerOctetString smdpSignature2 = null;
  private Octet32 hashCc = null;
  private Certificate smdpCertificate = null;
  
  public PrepareDownloadRequest() {}
  
  public PrepareDownloadRequest(byte[] code)
  {
    this.code = code;
  }
  
  public void setSmdpSigned2(SmdpSigned2 smdpSigned2) {
    this.smdpSigned2 = smdpSigned2;
  }
  
  public SmdpSigned2 getSmdpSigned2() {
    return smdpSigned2;
  }
  
  public void setSmdpSignature2(BerOctetString smdpSignature2) {
    this.smdpSignature2 = smdpSignature2;
  }
  
  public BerOctetString getSmdpSignature2() {
    return smdpSignature2;
  }
  
  public void setHashCc(Octet32 hashCc) {
    this.hashCc = hashCc;
  }
  
  public Octet32 getHashCc() {
    return hashCc;
  }
  
  public void setSmdpCertificate(Certificate smdpCertificate) {
    this.smdpCertificate = smdpCertificate;
  }
  
  public Certificate getSmdpCertificate() {
    return smdpCertificate;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += smdpCertificate.encode(os, true);
    
    if (hashCc != null) {
      codeLength += hashCc.encode(os, true);
    }
    
    codeLength += smdpSignature2.encode(os, false);
    
    os.write(55);
    os.write(95);
    codeLength += 2;
    
    codeLength += smdpSigned2.encode(os, true);
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(SmdpSigned2.tag)) {
      smdpSigned2 = new SmdpSigned2();
      subCodeLength += smdpSigned2.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(64, 0, 55)) {
      smdpSignature2 = new BerOctetString();
      subCodeLength += smdpSignature2.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(Octet32.tag)) {
      hashCc = new Octet32();
      subCodeLength += hashCc.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(Certificate.tag)) {
      smdpCertificate = new Certificate();
      subCodeLength += smdpCertificate.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (smdpSigned2 != null) {
      sb.append("smdpSigned2: ");
      smdpSigned2.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("smdpSigned2: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (smdpSignature2 != null) {
      sb.append("smdpSignature2: ").append(smdpSignature2);
    }
    else {
      sb.append("smdpSignature2: <empty-required-field>");
    }
    
    if (hashCc != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("hashCc: ").append(hashCc);
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (smdpCertificate != null) {
      sb.append("smdpCertificate: ");
      smdpCertificate.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("smdpCertificate: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
