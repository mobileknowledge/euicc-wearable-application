package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
















public class DeviceInfo
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private Octet8 tac = null;
  private DeviceCapabilities deviceCapabilities = null;
  private Octet8 imei = null;
  
  public DeviceInfo() {}
  
  public DeviceInfo(byte[] code)
  {
    this.code = code;
  }
  
  public void setTac(Octet8 tac) {
    this.tac = tac;
  }
  
  public Octet8 getTac() {
    return tac;
  }
  
  public void setDeviceCapabilities(DeviceCapabilities deviceCapabilities) {
    this.deviceCapabilities = deviceCapabilities;
  }
  
  public DeviceCapabilities getDeviceCapabilities() {
    return deviceCapabilities;
  }
  
  public void setImei(Octet8 imei) {
    this.imei = imei;
  }
  
  public Octet8 getImei() {
    return imei;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (imei != null) {
      codeLength += imei.encode(os, false);
      
      os.write(130);
      codeLength++;
    }
    
    codeLength += deviceCapabilities.encode(os, false);
    
    os.write(161);
    codeLength++;
    
    codeLength += tac.encode(os, false);
    
    os.write(128);
    codeLength++;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 0)) {
      tac = new Octet8();
      subCodeLength += tac.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 32, 1)) {
      deviceCapabilities = new DeviceCapabilities();
      subCodeLength += deviceCapabilities.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 2)) {
      imei = new Octet8();
      subCodeLength += imei.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (tac != null) {
      sb.append("tac: ").append(tac);
    }
    else {
      sb.append("tac: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (deviceCapabilities != null) {
      sb.append("deviceCapabilities: ");
      deviceCapabilities.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("deviceCapabilities: <empty-required-field>");
    }
    
    if (imei != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("imei: ").append(imei);
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
