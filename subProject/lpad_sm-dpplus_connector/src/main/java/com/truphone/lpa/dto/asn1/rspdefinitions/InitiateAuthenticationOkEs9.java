package com.truphone.lpa.dto.asn1.rspdefinitions;

import com.truphone.lpa.dto.asn1.pkix1explicit88.Certificate;
import com.truphone.lpa.dto.asn1.pkix1implicit88.SubjectKeyIdentifier;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;













public class InitiateAuthenticationOkEs9
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private TransactionId transactionId = null;
  private ServerSigned1 serverSigned1 = null;
  private BerOctetString serverSignature1 = null;
  private SubjectKeyIdentifier euiccCiPKIdToBeUsed = null;
  private Certificate serverCertificate = null;
  
  public InitiateAuthenticationOkEs9() {}
  
  public InitiateAuthenticationOkEs9(byte[] code)
  {
    this.code = code;
  }
  
  public void setTransactionId(TransactionId transactionId) {
    this.transactionId = transactionId;
  }
  
  public TransactionId getTransactionId() {
    return transactionId;
  }
  
  public void setServerSigned1(ServerSigned1 serverSigned1) {
    this.serverSigned1 = serverSigned1;
  }
  
  public ServerSigned1 getServerSigned1() {
    return serverSigned1;
  }
  
  public void setServerSignature1(BerOctetString serverSignature1) {
    this.serverSignature1 = serverSignature1;
  }
  
  public BerOctetString getServerSignature1() {
    return serverSignature1;
  }
  
  public void setEuiccCiPKIdToBeUsed(SubjectKeyIdentifier euiccCiPKIdToBeUsed) {
    this.euiccCiPKIdToBeUsed = euiccCiPKIdToBeUsed;
  }
  
  public SubjectKeyIdentifier getEuiccCiPKIdToBeUsed() {
    return euiccCiPKIdToBeUsed;
  }
  
  public void setServerCertificate(Certificate serverCertificate) {
    this.serverCertificate = serverCertificate;
  }
  
  public Certificate getServerCertificate() {
    return serverCertificate;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += serverCertificate.encode(os, true);
    
    codeLength += euiccCiPKIdToBeUsed.encode(os, true);
    
    codeLength += serverSignature1.encode(os, false);
    
    os.write(55);
    os.write(95);
    codeLength += 2;
    
    codeLength += serverSigned1.encode(os, true);
    
    codeLength += transactionId.encode(os, false);
    
    os.write(128);
    codeLength++;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 0)) {
      transactionId = new TransactionId();
      subCodeLength += transactionId.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(ServerSigned1.tag)) {
      serverSigned1 = new ServerSigned1();
      subCodeLength += serverSigned1.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(64, 0, 55)) {
      serverSignature1 = new BerOctetString();
      subCodeLength += serverSignature1.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(SubjectKeyIdentifier.tag)) {
      euiccCiPKIdToBeUsed = new SubjectKeyIdentifier();
      subCodeLength += euiccCiPKIdToBeUsed.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(Certificate.tag)) {
      serverCertificate = new Certificate();
      subCodeLength += serverCertificate.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (transactionId != null) {
      sb.append("transactionId: ").append(transactionId);
    }
    else {
      sb.append("transactionId: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (serverSigned1 != null) {
      sb.append("serverSigned1: ");
      serverSigned1.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("serverSigned1: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (serverSignature1 != null) {
      sb.append("serverSignature1: ").append(serverSignature1);
    }
    else {
      sb.append("serverSignature1: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccCiPKIdToBeUsed != null) {
      sb.append("euiccCiPKIdToBeUsed: ").append(euiccCiPKIdToBeUsed);
    }
    else {
      sb.append("euiccCiPKIdToBeUsed: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (serverCertificate != null) {
      sb.append("serverCertificate: ");
      serverCertificate.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("serverCertificate: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
