package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerTag;

















public class PendingNotification
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  private ProfileInstallationResult profileInstallationResult = null;
  private OtherSignedNotification otherSignedNotification = null;
  
  public PendingNotification() {}
  
  public PendingNotification(byte[] code)
  {
    this.code = code;
  }
  
  public void setProfileInstallationResult(ProfileInstallationResult profileInstallationResult) {
    this.profileInstallationResult = profileInstallationResult;
  }
  
  public ProfileInstallationResult getProfileInstallationResult() {
    return profileInstallationResult;
  }
  
  public void setOtherSignedNotification(OtherSignedNotification otherSignedNotification) {
    this.otherSignedNotification = otherSignedNotification;
  }
  
  public OtherSignedNotification getOtherSignedNotification() {
    return otherSignedNotification;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (otherSignedNotification != null) {
      codeLength += otherSignedNotification.encode(os, true);
      return codeLength;
    }
    
    if (profileInstallationResult != null) {
      codeLength += profileInstallationResult.encode(os, false);
      
      os.write(55);
      os.write(191);
      codeLength += 2;
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, null);
  }
  
  public int decode(InputStream is, BerTag berTag) throws IOException
  {
    int codeLength = 0;
    BerTag passedTag = berTag;
    
    if (berTag == null) {
      berTag = new BerTag();
      codeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 32, 55)) {
      profileInstallationResult = new ProfileInstallationResult();
      codeLength += profileInstallationResult.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(OtherSignedNotification.tag)) {
      otherSignedNotification = new OtherSignedNotification();
      codeLength += otherSignedNotification.decode(is, false);
      return codeLength;
    }
    
    if (passedTag != null) {
      return 0;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (profileInstallationResult != null) {
      sb.append("profileInstallationResult: ");
      profileInstallationResult.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (otherSignedNotification != null) {
      sb.append("otherSignedNotification: ");
      otherSignedNotification.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    sb.append("<none>");
  }
}
