package com.truphone.lpa.dto.asn1.rspdefinitions;

import org.openmuc.jasn1.ber.types.BerBitString;




















public class CertificateInfo
  extends BerBitString
{
  private static final long serialVersionUID = 1L;
  
  public CertificateInfo() {}
  
  public CertificateInfo(byte[] code)
  {
    super(code);
  }
  
  public CertificateInfo(byte[] value, int numBits) {
    super(value, numBits);
  }
}
