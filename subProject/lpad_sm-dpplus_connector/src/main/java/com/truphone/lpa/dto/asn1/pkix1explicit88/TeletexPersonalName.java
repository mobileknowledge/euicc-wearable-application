//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.string.BerTeletexString;

public class TeletexPersonalName implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 17);
  public byte[] code = null;
  private BerTeletexString surname = null;
  private BerTeletexString givenName = null;
  private BerTeletexString initials = null;
  private BerTeletexString generationQualifier = null;

  public TeletexPersonalName() {
  }

  public TeletexPersonalName(byte[] code) {
    this.code = code;
  }

  public void setSurname(BerTeletexString surname) {
    this.surname = surname;
  }

  public BerTeletexString getSurname() {
    return this.surname;
  }

  public void setGivenName(BerTeletexString givenName) {
    this.givenName = givenName;
  }

  public BerTeletexString getGivenName() {
    return this.givenName;
  }

  public void setInitials(BerTeletexString initials) {
    this.initials = initials;
  }

  public BerTeletexString getInitials() {
    return this.initials;
  }

  public void setGenerationQualifier(BerTeletexString generationQualifier) {
    this.generationQualifier = generationQualifier;
  }

  public BerTeletexString getGenerationQualifier() {
    return this.generationQualifier;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int codeLength;
    if (this.code == null) {
      codeLength = 0;
      if (this.generationQualifier != null) {
        codeLength += this.generationQualifier.encode(os, false);
        os.write(131);
        ++codeLength;
      }

      if (this.initials != null) {
        codeLength += this.initials.encode(os, false);
        os.write(130);
        ++codeLength;
      }

      if (this.givenName != null) {
        codeLength += this.givenName.encode(os, false);
        os.write(129);
        ++codeLength;
      }

      codeLength += this.surname.encode(os, false);
      os.write(128);
      ++codeLength;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }

      return codeLength;
    } else {
      for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
        os.write(this.code[codeLength]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;

    while(subCodeLength < totalLength) {
      subCodeLength += berTag.decode(is);
      if (berTag.equals(128, 0, 0)) {
        this.surname = new BerTeletexString();
        subCodeLength += this.surname.decode(is, false);
      } else if (berTag.equals(128, 0, 1)) {
        this.givenName = new BerTeletexString();
        subCodeLength += this.givenName.decode(is, false);
      } else if (berTag.equals(128, 0, 2)) {
        this.initials = new BerTeletexString();
        subCodeLength += this.initials.decode(is, false);
      } else if (berTag.equals(128, 0, 3)) {
        this.generationQualifier = new BerTeletexString();
        subCodeLength += this.generationQualifier.decode(is, false);
      }
    }

    if (subCodeLength != totalLength) {
      throw new IOException("Length of set does not match length tag, length tag: " + totalLength + ", actual set length: " + subCodeLength);
    } else {
      codeLength += subCodeLength;
      return codeLength;
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    sb.append("\n");

    int i;
    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.surname != null) {
      sb.append("surname: ").append(this.surname);
    } else {
      sb.append("surname: <empty-required-field>");
    }

    if (this.givenName != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("givenName: ").append(this.givenName);
    }

    if (this.initials != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("initials: ").append(this.initials);
    }

    if (this.generationQualifier != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("generationQualifier: ").append(this.generationQualifier);
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }
}
