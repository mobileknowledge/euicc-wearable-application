//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerAny;
import org.openmuc.jasn1.ber.types.BerInteger;

public class ExtensionAttribute implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  public byte[] code = null;
  private BerInteger extensionAttributeType = null;
  private BerAny extensionAttributeValue = null;

  public ExtensionAttribute() {
  }

  public ExtensionAttribute(byte[] code) {
    this.code = code;
  }

  public void setExtensionAttributeType(BerInteger extensionAttributeType) {
    this.extensionAttributeType = extensionAttributeType;
  }

  public BerInteger getExtensionAttributeType() {
    return this.extensionAttributeType;
  }

  public void setExtensionAttributeValue(BerAny extensionAttributeValue) {
    this.extensionAttributeValue = extensionAttributeValue;
  }

  public BerAny getExtensionAttributeValue() {
    return this.extensionAttributeValue;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int i;
    if (this.code == null) {
      int codeLength = 0;
      int sublength = this.extensionAttributeValue.encode(os);
      i = codeLength + sublength;
      i += BerLength.encodeLength(os, sublength);
      os.write(161);
      ++i;
      i += this.extensionAttributeType.encode(os, false);
      os.write(128);
      ++i;
      i += BerLength.encodeLength(os, i);
      if (withTag) {
        i += tag.encode(os);
      }

      return i;
    } else {
      for(i = this.code.length - 1; i >= 0; --i) {
        os.write(this.code[i]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;
    codeLength += totalLength;
    subCodeLength = subCodeLength + berTag.decode(is);
    if (berTag.equals(128, 0, 0)) {
      this.extensionAttributeType = new BerInteger();
      subCodeLength += this.extensionAttributeType.decode(is, false);
      subCodeLength += berTag.decode(is);
      if (berTag.equals(128, 32, 1)) {
        subCodeLength += length.decode(is);
        this.extensionAttributeValue = new BerAny();
        subCodeLength += this.extensionAttributeValue.decode(is, null);
        if (subCodeLength == totalLength) {
          return codeLength;
        }
      }

      throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
    } else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    sb.append("\n");

    int i;
    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.extensionAttributeType != null) {
      sb.append("extensionAttributeType: ").append(this.extensionAttributeType);
    } else {
      sb.append("extensionAttributeType: <empty-required-field>");
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.extensionAttributeValue != null) {
      sb.append("extensionAttributeValue: ").append(this.extensionAttributeValue);
    } else {
      sb.append("extensionAttributeValue: <empty-required-field>");
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }
}
