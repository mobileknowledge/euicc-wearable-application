package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.string.BerNumericString;
import org.openmuc.jasn1.ber.types.string.BerPrintableString;











public class PostalCode
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  private BerNumericString numericCode = null;
  private BerPrintableString printableCode = null;
  
  public PostalCode() {}
  
  public PostalCode(byte[] code)
  {
    this.code = code;
  }
  
  public void setNumericCode(BerNumericString numericCode) {
    this.numericCode = numericCode;
  }
  
  public BerNumericString getNumericCode() {
    return numericCode;
  }
  
  public void setPrintableCode(BerPrintableString printableCode) {
    this.printableCode = printableCode;
  }
  
  public BerPrintableString getPrintableCode() {
    return printableCode;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (printableCode != null) {
      codeLength += printableCode.encode(os, true);
      return codeLength;
    }
    
    if (numericCode != null) {
      codeLength += numericCode.encode(os, true);
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, null);
  }
  
  public int decode(InputStream is, BerTag berTag) throws IOException
  {
    int codeLength = 0;
    BerTag passedTag = berTag;
    
    if (berTag == null) {
      berTag = new BerTag();
      codeLength += berTag.decode(is);
    }
    
    if (berTag.equals(BerNumericString.tag)) {
      numericCode = new BerNumericString();
      codeLength += numericCode.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(BerPrintableString.tag)) {
      printableCode = new BerPrintableString();
      codeLength += printableCode.decode(is, false);
      return codeLength;
    }
    
    if (passedTag != null) {
      return 0;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (numericCode != null) {
      sb.append("numericCode: ").append(numericCode);
      return;
    }
    
    if (printableCode != null) {
      sb.append("printableCode: ").append(printableCode);
      return;
    }
    
    sb.append("<none>");
  }
}
