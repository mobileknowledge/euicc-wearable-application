//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerAny;
import org.openmuc.jasn1.ber.types.BerObjectIdentifier;

public class AlgorithmIdentifier implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  public byte[] code = null;
  private BerObjectIdentifier algorithm = null;
  private BerAny parameters = null;

  public AlgorithmIdentifier() {
  }

  public AlgorithmIdentifier(byte[] code) {
    this.code = code;
  }

  public void setAlgorithm(BerObjectIdentifier algorithm) {
    this.algorithm = algorithm;
  }

  public BerObjectIdentifier getAlgorithm() {
    return this.algorithm;
  }

  public void setParameters(BerAny parameters) {
    this.parameters = parameters;
  }

  public BerAny getParameters() {
    return this.parameters;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int codeLength;
    if (this.code == null) {
      codeLength = 0;
      if (this.parameters != null) {
        codeLength += this.parameters.encode(os);
      }

      codeLength += this.algorithm.encode(os, true);
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }

      return codeLength;
    } else {
      for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
        os.write(this.code[codeLength]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;
    codeLength += totalLength;
    subCodeLength = subCodeLength + berTag.decode(is);
    if (berTag.equals(BerObjectIdentifier.tag)) {
      this.algorithm = new BerObjectIdentifier();
      subCodeLength += this.algorithm.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      } else {
        subCodeLength += berTag.decode(is);
        this.parameters = new BerAny();
        int choiceDecodeLength = this.parameters.decode(is, berTag);
        subCodeLength += choiceDecodeLength;
        if (subCodeLength == totalLength) {
          return codeLength;
        } else {
          throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
        }
      }
    } else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    sb.append("\n");

    int i;
    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.algorithm != null) {
      sb.append("algorithm: ").append(this.algorithm);
    } else {
      sb.append("algorithm: <empty-required-field>");
    }

    if (this.parameters != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("parameters: ").append(this.parameters);
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }
}
