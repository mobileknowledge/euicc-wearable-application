package com.truphone.lpa.dto.asn1.rspdefinitions;

import org.openmuc.jasn1.ber.types.BerOctetString;




















public class Octet1
  extends BerOctetString
{
  private static final long serialVersionUID = 1L;
  
  public Octet1() {}
  
  public Octet1(byte[] value)
  {
    super(value);
  }
}
