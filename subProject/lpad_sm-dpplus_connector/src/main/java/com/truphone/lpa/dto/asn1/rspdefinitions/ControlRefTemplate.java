package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
















public class ControlRefTemplate
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private Octet1 keyType = null;
  private Octet1 keyLen = null;
  private OctetTo16 hostId = null;
  
  public ControlRefTemplate() {}
  
  public ControlRefTemplate(byte[] code)
  {
    this.code = code;
  }
  
  public void setKeyType(Octet1 keyType) {
    this.keyType = keyType;
  }
  
  public Octet1 getKeyType() {
    return keyType;
  }
  
  public void setKeyLen(Octet1 keyLen) {
    this.keyLen = keyLen;
  }
  
  public Octet1 getKeyLen() {
    return keyLen;
  }
  
  public void setHostId(OctetTo16 hostId) {
    this.hostId = hostId;
  }
  
  public OctetTo16 getHostId() {
    return hostId;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += hostId.encode(os, false);
    
    os.write(132);
    codeLength++;
    
    codeLength += keyLen.encode(os, false);
    
    os.write(129);
    codeLength++;
    
    codeLength += keyType.encode(os, false);
    
    os.write(128);
    codeLength++;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 0)) {
      keyType = new Octet1();
      subCodeLength += keyType.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 1)) {
      keyLen = new Octet1();
      subCodeLength += keyLen.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 4)) {
      hostId = new OctetTo16();
      subCodeLength += hostId.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (keyType != null) {
      sb.append("keyType: ").append(keyType);
    }
    else {
      sb.append("keyType: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (keyLen != null) {
      sb.append("keyLen: ").append(keyLen);
    }
    else {
      sb.append("keyLen: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (hostId != null) {
      sb.append("hostId: ").append(hostId);
    }
    else {
      sb.append("hostId: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
