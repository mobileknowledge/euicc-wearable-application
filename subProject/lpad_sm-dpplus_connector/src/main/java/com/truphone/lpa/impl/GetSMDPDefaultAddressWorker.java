package com.truphone.lpa.impl;

import android.org.apache.commons.codec.DecoderException;
import android.org.apache.commons.codec.binary.Hex;

import com.truphone.lpa.ApduChannel;
import com.truphone.lpa.apdu.ApduUtils;
import com.truphone.lpa.dto.asn1.rspdefinitions.EuiccConfiguredAddressesResponse;
import com.truphone.util.LogStub;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GetSMDPDefaultAddressWorker {

    private static final Logger LOG = Logger.getLogger(GetSMDPDefaultAddressWorker.class.getName());

    //private final Progress progress;
    private final ApduChannel apduChannel;



    GetSMDPDefaultAddressWorker(ApduChannel apduChannel) {

        //this.progress = progress;
        this.apduChannel = apduChannel;

    }

    String run() {

        //progress.setTotalSteps(3);
        // progress.stepExecuted(ProgressStep.SET_NICKNAME_RETRIEVING, "setNickname retrieving...");

        //if (LogStub.getInstance().isDebugEnabled()) {
        LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Setting SMDPDefaultAddress");
        //}


        String SetSMDPAddressApdu = ApduUtils.getEuiccConfiguredAddressesApdu();

        //if (LogStub.getInstance().isDebugEnabled()) {
        LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - SetSMDPDefaultAddress APDU: " + SetSMDPAddressApdu);
        //}

        String SetSMDPAddressApduResponseStr = apduChannel.transmitAPDU(SetSMDPAddressApdu);

        //if (LogStub.getInstance().isDebugEnabled()) {
        LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Response: " + SetSMDPAddressApduResponseStr);
        //}

        return setSMDPAddressApduRspData(SetSMDPAddressApduResponseStr);
    }

    private String setSMDPAddressApduRspData(String SetSMDPAddressApduResponseStr) {

        //progress.stepExecuted(ProgressStep.SET_NICKNAME_CONVERTING, "setNickname converting...");

        // setDPAddressResponse = new SetDefaultDpAddressResponse();
        EuiccConfiguredAddressesResponse setDPAddressResponse = new EuiccConfiguredAddressesResponse();
        String DefaultSMDPAddress = null;

        try {
            //  if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Decoding response: " + SetSMDPAddressApduResponseStr);
            //}

            InputStream is = new ByteArrayInputStream(Hex.decodeHex(SetSMDPAddressApduResponseStr.toCharArray()));

            //if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Decoding with SetDefaultDpAddressResponse");
            //}

            setDPAddressResponse.decode(is, true);
            if (setDPAddressResponse.getDefaultDpAddress() != null){
                DefaultSMDPAddress = setDPAddressResponse.getDefaultDpAddress().toString();
                LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Default SM-DP+ address is: " + setDPAddressResponse.getDefaultDpAddress().toString());
            }

            //if (LogStub.getInstance().isDebugEnabled()) {
           // LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - EID is: " + setDPAddressResponse.getDefaultDpAddress().toString());
            //}

            //progress.stepExecuted(ProgressStep.GET_EID_CONVERTED, "getEID converted...");

            return DefaultSMDPAddress;
        } catch (DecoderException e) {
            LOG.log(Level.SEVERE, LogStub.getInstance().getTag() + " - " + e.getMessage(), e);
            LOG.log(Level.SEVERE, LogStub.getInstance().getTag() + " -  Unable to SetSMDPDefaultAddress. Exception in Decoder:" + e.getMessage());

            throw new RuntimeException("Unable to SetSMDPDefaultAddress");
        } catch (IOException ioe) {
            LOG.log(Level.SEVERE, LogStub.getInstance().getTag() + " - " + ioe.getMessage(), ioe);

            throw new RuntimeException("Invalid SetSMDPDefaultAddress response");
        }
    }

}
