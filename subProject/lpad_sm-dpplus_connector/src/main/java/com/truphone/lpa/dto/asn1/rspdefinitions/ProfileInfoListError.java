package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class ProfileInfoListError
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public ProfileInfoListError() {}
  
  public ProfileInfoListError(byte[] code)
  {
    super(code);
  }
  
  public ProfileInfoListError(BigInteger value) {
    super(value);
  }
  
  public ProfileInfoListError(long value) {
    super(value);
  }
}
