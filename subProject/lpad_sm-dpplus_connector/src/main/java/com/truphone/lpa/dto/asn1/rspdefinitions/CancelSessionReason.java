package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class CancelSessionReason
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public CancelSessionReason() {}
  
  public CancelSessionReason(byte[] code)
  {
    super(code);
  }
  
  public CancelSessionReason(BigInteger value) {
    super(value);
  }
  
  public CancelSessionReason(long value) {
    super(value);
  }
}
