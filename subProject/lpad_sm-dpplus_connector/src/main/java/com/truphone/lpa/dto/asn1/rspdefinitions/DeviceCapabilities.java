package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
















public class DeviceCapabilities
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private VersionType gsmSupportedRelease = null;
  private VersionType utranSupportedRelease = null;
  private VersionType cdma2000onexSupportedRelease = null;
  private VersionType cdma2000hrpdSupportedRelease = null;
  private VersionType cdma2000ehrpdSupportedRelease = null;
  private VersionType eutranSupportedRelease = null;
  private VersionType contactlessSupportedRelease = null;
  private VersionType rspCrlSupportedVersion = null;
  private VersionType rspRpmSupportedVersion = null;
  
  public DeviceCapabilities() {}
  
  public DeviceCapabilities(byte[] code)
  {
    this.code = code;
  }
  
  public void setGsmSupportedRelease(VersionType gsmSupportedRelease) {
    this.gsmSupportedRelease = gsmSupportedRelease;
  }
  
  public VersionType getGsmSupportedRelease() {
    return gsmSupportedRelease;
  }
  
  public void setUtranSupportedRelease(VersionType utranSupportedRelease) {
    this.utranSupportedRelease = utranSupportedRelease;
  }
  
  public VersionType getUtranSupportedRelease() {
    return utranSupportedRelease;
  }
  
  public void setCdma2000onexSupportedRelease(VersionType cdma2000onexSupportedRelease) {
    this.cdma2000onexSupportedRelease = cdma2000onexSupportedRelease;
  }
  
  public VersionType getCdma2000onexSupportedRelease() {
    return cdma2000onexSupportedRelease;
  }
  
  public void setCdma2000hrpdSupportedRelease(VersionType cdma2000hrpdSupportedRelease) {
    this.cdma2000hrpdSupportedRelease = cdma2000hrpdSupportedRelease;
  }
  
  public VersionType getCdma2000hrpdSupportedRelease() {
    return cdma2000hrpdSupportedRelease;
  }
  
  public void setCdma2000ehrpdSupportedRelease(VersionType cdma2000ehrpdSupportedRelease) {
    this.cdma2000ehrpdSupportedRelease = cdma2000ehrpdSupportedRelease;
  }
  
  public VersionType getCdma2000ehrpdSupportedRelease() {
    return cdma2000ehrpdSupportedRelease;
  }
  
  public void setEutranSupportedRelease(VersionType eutranSupportedRelease) {
    this.eutranSupportedRelease = eutranSupportedRelease;
  }
  
  public VersionType getEutranSupportedRelease() {
    return eutranSupportedRelease;
  }
  
  public void setContactlessSupportedRelease(VersionType contactlessSupportedRelease) {
    this.contactlessSupportedRelease = contactlessSupportedRelease;
  }
  
  public VersionType getContactlessSupportedRelease() {
    return contactlessSupportedRelease;
  }
  
  public void setRspCrlSupportedVersion(VersionType rspCrlSupportedVersion) {
    this.rspCrlSupportedVersion = rspCrlSupportedVersion;
  }
  
  public VersionType getRspCrlSupportedVersion() {
    return rspCrlSupportedVersion;
  }
  
  public void setRspRpmSupportedVersion(VersionType rspRpmSupportedVersion) {
    this.rspRpmSupportedVersion = rspRpmSupportedVersion;
  }
  
  public VersionType getRspRpmSupportedVersion() {
    return rspRpmSupportedVersion;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (rspRpmSupportedVersion != null) {
      codeLength += rspRpmSupportedVersion.encode(os, false);
      
      os.write(136);
      codeLength++;
    }
    
    if (rspCrlSupportedVersion != null) {
      codeLength += rspCrlSupportedVersion.encode(os, false);
      
      os.write(135);
      codeLength++;
    }
    
    if (contactlessSupportedRelease != null) {
      codeLength += contactlessSupportedRelease.encode(os, false);
      
      os.write(134);
      codeLength++;
    }
    
    if (eutranSupportedRelease != null) {
      codeLength += eutranSupportedRelease.encode(os, false);
      
      os.write(133);
      codeLength++;
    }
    
    if (cdma2000ehrpdSupportedRelease != null) {
      codeLength += cdma2000ehrpdSupportedRelease.encode(os, false);
      
      os.write(132);
      codeLength++;
    }
    
    if (cdma2000hrpdSupportedRelease != null) {
      codeLength += cdma2000hrpdSupportedRelease.encode(os, false);
      
      os.write(131);
      codeLength++;
    }
    
    if (cdma2000onexSupportedRelease != null) {
      codeLength += cdma2000onexSupportedRelease.encode(os, false);
      
      os.write(130);
      codeLength++;
    }
    
    if (utranSupportedRelease != null) {
      codeLength += utranSupportedRelease.encode(os, false);
      
      os.write(129);
      codeLength++;
    }
    
    if (gsmSupportedRelease != null) {
      codeLength += gsmSupportedRelease.encode(os, false);
      
      os.write(128);
      codeLength++;
    }
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    if (totalLength == 0) {
      return codeLength;
    }
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 0)) {
      gsmSupportedRelease = new VersionType();
      subCodeLength += gsmSupportedRelease.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 1)) {
      utranSupportedRelease = new VersionType();
      subCodeLength += utranSupportedRelease.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 2)) {
      cdma2000onexSupportedRelease = new VersionType();
      subCodeLength += cdma2000onexSupportedRelease.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 3)) {
      cdma2000hrpdSupportedRelease = new VersionType();
      subCodeLength += cdma2000hrpdSupportedRelease.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 4)) {
      cdma2000ehrpdSupportedRelease = new VersionType();
      subCodeLength += cdma2000ehrpdSupportedRelease.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 5)) {
      eutranSupportedRelease = new VersionType();
      subCodeLength += eutranSupportedRelease.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 6)) {
      contactlessSupportedRelease = new VersionType();
      subCodeLength += contactlessSupportedRelease.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 7)) {
      rspCrlSupportedVersion = new VersionType();
      subCodeLength += rspCrlSupportedVersion.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 8)) {
      rspRpmSupportedVersion = new VersionType();
      subCodeLength += rspRpmSupportedVersion.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    boolean firstSelectedElement = true;
    if (gsmSupportedRelease != null) {
      sb.append("\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("gsmSupportedRelease: ").append(gsmSupportedRelease);
      firstSelectedElement = false;
    }
    
    if (utranSupportedRelease != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("utranSupportedRelease: ").append(utranSupportedRelease);
      firstSelectedElement = false;
    }
    
    if (cdma2000onexSupportedRelease != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("cdma2000onexSupportedRelease: ").append(cdma2000onexSupportedRelease);
      firstSelectedElement = false;
    }
    
    if (cdma2000hrpdSupportedRelease != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("cdma2000hrpdSupportedRelease: ").append(cdma2000hrpdSupportedRelease);
      firstSelectedElement = false;
    }
    
    if (cdma2000ehrpdSupportedRelease != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("cdma2000ehrpdSupportedRelease: ").append(cdma2000ehrpdSupportedRelease);
      firstSelectedElement = false;
    }
    
    if (eutranSupportedRelease != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("eutranSupportedRelease: ").append(eutranSupportedRelease);
      firstSelectedElement = false;
    }
    
    if (contactlessSupportedRelease != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("contactlessSupportedRelease: ").append(contactlessSupportedRelease);
      firstSelectedElement = false;
    }
    
    if (rspCrlSupportedVersion != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("rspCrlSupportedVersion: ").append(rspCrlSupportedVersion);
      firstSelectedElement = false;
    }
    
    if (rspRpmSupportedVersion != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("rspRpmSupportedVersion: ").append(rspRpmSupportedVersion);
      firstSelectedElement = false;
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
