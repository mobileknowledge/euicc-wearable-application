package com.truphone.lpa.dto.asn1.pkix1explicit88;

import org.openmuc.jasn1.ber.types.string.BerIA5String;
















public class DomainComponent
  extends BerIA5String
{
  private static final long serialVersionUID = 1L;
  
  public DomainComponent() {}
  
  public DomainComponent(byte[] value)
  {
    super(value);
  }
}
