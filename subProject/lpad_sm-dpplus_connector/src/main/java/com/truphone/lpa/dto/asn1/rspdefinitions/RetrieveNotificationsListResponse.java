package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerInteger;












public class RetrieveNotificationsListResponse
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  public static final BerTag tag = new BerTag(128, 32, 43);
  public RetrieveNotificationsListResponse() {}
  
  public static class NotificationList implements Serializable
  {
    private static final long serialVersionUID = 1L;
    public static final BerTag tag = new BerTag(0, 32, 16);
    public byte[] code = null;
    private List<PendingNotification> seqOf = null;
    
    public NotificationList() {
      seqOf = new ArrayList();
    }
    
    public NotificationList(byte[] code) {
      this.code = code;
    }
    
    public List<PendingNotification> getPendingNotification() {
      if (seqOf == null) {
        seqOf = new ArrayList();
      }
      return seqOf;
    }
    
    public int encode(BerByteArrayOutputStream os) throws IOException {
      return encode(os, true);
    }
    
    public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
    {
      if (code != null) {
        for (int i = code.length - 1; i >= 0; i--) {
          os.write(code[i]);
        }
        if (withTag) {
          return tag.encode(os) + code.length;
        }
        return code.length;
      }
      
      int codeLength = 0;
      for (int i = seqOf.size() - 1; i >= 0; i--) {
        codeLength += seqOf.get(i).encode(os);
      }
      
      codeLength += BerLength.encodeLength(os, codeLength);
      
      if (withTag) {
        codeLength += tag.encode(os);
      }
      
      return codeLength;
    }
    
    public int decode(InputStream is) throws IOException {
      return decode(is, true);
    }
    
    public int decode(InputStream is, boolean withTag) throws IOException {
      int codeLength = 0;
      int subCodeLength = 0;
      if (withTag) {
        codeLength += tag.decodeAndCheck(is);
      }
      
      BerLength length = new BerLength();
      codeLength += length.decode(is);
      int totalLength = length.val;
      
      while (subCodeLength < totalLength) {
        PendingNotification element = new PendingNotification();
        subCodeLength += element.decode(is, null);
        seqOf.add(element);
      }
      if (subCodeLength != totalLength) {
        throw new IOException("Decoded SequenceOf or SetOf has wrong length. Expected " + totalLength + " but has " + subCodeLength);
      }
      
      codeLength += subCodeLength;
      
      return codeLength;
    }
    
    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      encode(os, false);
      code = os.getArray();
    }
    
    public String toString() {
      StringBuilder sb = new StringBuilder();
      appendAsString(sb, 0);
      return sb.toString();
    }
    
    public void appendAsString(StringBuilder sb, int indentLevel)
    {
      sb.append("{\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      if (seqOf == null) {
        sb.append("null");
      }
      else {
        Iterator<PendingNotification> it = seqOf.iterator();
        if (it.hasNext()) {
          it.next().appendAsString(sb, indentLevel + 1);
          while (it.hasNext()) {
            sb.append(",\n");
            for (int i = 0; i < indentLevel + 1; i++) {
              sb.append("\t");
            }
            it.next().appendAsString(sb, indentLevel + 1);
          }
        }
      }
      
      sb.append("\n");
      for (int i = 0; i < indentLevel; i++) {
        sb.append("\t");
      }
      sb.append("}");
    }
  }
  

  private NotificationList notificationList = null;
  private BerInteger notificationsListResultError = null;
  


  public RetrieveNotificationsListResponse(byte[] code)
  {
    this.code = code;
  }
  
  public void setNotificationList(NotificationList notificationList) {
    this.notificationList = notificationList;
  }
  
  public NotificationList getNotificationList() {
    return notificationList;
  }
  
  public void setNotificationsListResultError(BerInteger notificationsListResultError) {
    this.notificationsListResultError = notificationsListResultError;
  }
  
  public BerInteger getNotificationsListResultError() {
    return notificationsListResultError;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (notificationsListResultError != null) {
      codeLength += notificationsListResultError.encode(os, false);
      
      os.write(129);
      codeLength++;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (notificationList != null) {
      codeLength += notificationList.encode(os, false);
      
      os.write(160);
      codeLength++;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    BerLength length = new BerLength();
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    codeLength += length.decode(is);
    codeLength += berTag.decode(is);
    
    if (berTag.equals(128, 32, 0)) {
      notificationList = new NotificationList();
      codeLength += notificationList.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 0, 1)) {
      notificationsListResultError = new BerInteger();
      codeLength += notificationsListResultError.decode(is, false);
      return codeLength;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (notificationList != null) {
      sb.append("notificationList: ");
      notificationList.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (notificationsListResultError != null) {
      sb.append("notificationsListResultError: ").append(notificationsListResultError);
      return;
    }
    
    sb.append("<none>");
  }
}
