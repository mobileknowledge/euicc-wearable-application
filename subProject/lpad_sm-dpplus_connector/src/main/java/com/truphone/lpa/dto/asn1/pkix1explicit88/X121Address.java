package com.truphone.lpa.dto.asn1.pkix1explicit88;

import org.openmuc.jasn1.ber.types.string.BerNumericString;
















public class X121Address
  extends BerNumericString
{
  private static final long serialVersionUID = 1L;
  
  public X121Address() {}
  
  public X121Address(byte[] value)
  {
    super(value);
  }
}
