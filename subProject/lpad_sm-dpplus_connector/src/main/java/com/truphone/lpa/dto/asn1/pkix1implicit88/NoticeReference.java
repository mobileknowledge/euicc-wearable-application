package com.truphone.lpa.dto.asn1.pkix1implicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerInteger;












public class NoticeReference
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public NoticeReference() {}
  
  public static class NoticeNumbers
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    public static final BerTag tag = new BerTag(0, 32, 16);
    public byte[] code = null;
    private List<BerInteger> seqOf = null;
    
    public NoticeNumbers() {
      seqOf = new ArrayList();
    }
    
    public NoticeNumbers(byte[] code) {
      this.code = code;
    }
    
    public List<BerInteger> getBerInteger() {
      if (seqOf == null) {
        seqOf = new ArrayList();
      }
      return seqOf;
    }
    
    public int encode(BerByteArrayOutputStream os) throws IOException {
      return encode(os, true);
    }
    
    public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
    {
      if (code != null) {
        for (int i = code.length - 1; i >= 0; i--) {
          os.write(code[i]);
        }
        if (withTag) {
          return tag.encode(os) + code.length;
        }
        return code.length;
      }
      
      int codeLength = 0;
      for (int i = seqOf.size() - 1; i >= 0; i--) {
        codeLength += seqOf.get(i).encode(os, true);
      }
      
      codeLength += BerLength.encodeLength(os, codeLength);
      
      if (withTag) {
        codeLength += tag.encode(os);
      }
      
      return codeLength;
    }
    
    public int decode(InputStream is) throws IOException {
      return decode(is, true);
    }
    
    public int decode(InputStream is, boolean withTag) throws IOException {
      int codeLength = 0;
      int subCodeLength = 0;
      if (withTag) {
        codeLength += tag.decodeAndCheck(is);
      }
      
      BerLength length = new BerLength();
      codeLength += length.decode(is);
      int totalLength = length.val;
      
      while (subCodeLength < totalLength) {
        BerInteger element = new BerInteger();
        subCodeLength += element.decode(is, true);
        seqOf.add(element);
      }
      if (subCodeLength != totalLength) {
        throw new IOException("Decoded SequenceOf or SetOf has wrong length. Expected " + totalLength + " but has " + subCodeLength);
      }
      
      codeLength += subCodeLength;
      
      return codeLength;
    }
    
    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      encode(os, false);
      code = os.getArray();
    }
    
    public String toString() {
      StringBuilder sb = new StringBuilder();
      appendAsString(sb, 0);
      return sb.toString();
    }
    
    public void appendAsString(StringBuilder sb, int indentLevel)
    {
      sb.append("{\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      if (seqOf == null) {
        sb.append("null");
      }
      else {
        Iterator<BerInteger> it = seqOf.iterator();
        if (it.hasNext()) {
          sb.append(it.next());
          while (it.hasNext()) {
            sb.append(",\n");
            for (int i = 0; i < indentLevel + 1; i++) {
              sb.append("\t");
            }
            sb.append(it.next());
          }
        }
      }
      
      sb.append("\n");
      for (int i = 0; i < indentLevel; i++) {
        sb.append("\t");
      }
      sb.append("}");
    }
  }
  

  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private DisplayText organization = null;
  private NoticeNumbers noticeNumbers = null;
  


  public NoticeReference(byte[] code)
  {
    this.code = code;
  }
  
  public void setOrganization(DisplayText organization) {
    this.organization = organization;
  }
  
  public DisplayText getOrganization() {
    return organization;
  }
  
  public void setNoticeNumbers(NoticeNumbers noticeNumbers) {
    this.noticeNumbers = noticeNumbers;
  }
  
  public NoticeNumbers getNoticeNumbers() {
    return noticeNumbers;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += noticeNumbers.encode(os, true);
    
    codeLength += organization.encode(os);
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    organization = new DisplayText();
    subCodeLength += organization.decode(is, berTag);
    subCodeLength += berTag.decode(is);
    
    if (berTag.equals(NoticeNumbers.tag)) {
      noticeNumbers = new NoticeNumbers();
      subCodeLength += noticeNumbers.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (organization != null) {
      sb.append("organization: ");
      organization.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("organization: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (noticeNumbers != null) {
      sb.append("noticeNumbers: ");
      noticeNumbers.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("noticeNumbers: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
