package com.truphone.lpa.dto.asn1.pkix1explicit88;

import org.openmuc.jasn1.ber.types.string.BerNumericString;
















public class NumericUserIdentifier
  extends BerNumericString
{
  private static final long serialVersionUID = 1L;
  
  public NumericUserIdentifier() {}
  
  public NumericUserIdentifier(byte[] value)
  {
    super(value);
  }
}
