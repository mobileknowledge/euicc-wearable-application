package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class PartialCrlNumber
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public PartialCrlNumber() {}
  
  public PartialCrlNumber(byte[] code)
  {
    super(code);
  }
  
  public PartialCrlNumber(BigInteger value) {
    super(value);
  }
  
  public PartialCrlNumber(long value) {
    super(value);
  }
}
