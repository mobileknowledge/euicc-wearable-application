package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class IconType
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public IconType() {}
  
  public IconType(byte[] code)
  {
    super(code);
  }
  
  public IconType(BigInteger value) {
    super(value);
  }
  
  public IconType(long value) {
    super(value);
  }
}
