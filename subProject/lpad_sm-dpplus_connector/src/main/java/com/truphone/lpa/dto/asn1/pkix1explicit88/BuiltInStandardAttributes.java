//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;

public class BuiltInStandardAttributes implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  public byte[] code = null;
  private CountryName countryName = null;
  private AdministrationDomainName administrationDomainName = null;
  private NetworkAddress networkAddress = null;
  private TerminalIdentifier terminalIdentifier = null;
  private PrivateDomainName privateDomainName = null;
  private OrganizationName organizationName = null;
  private NumericUserIdentifier numericUserIdentifier = null;
  private PersonalName personalName = null;
  private OrganizationalUnitNames organizationalUnitNames = null;

  public BuiltInStandardAttributes() {
  }

  public BuiltInStandardAttributes(byte[] code) {
    this.code = code;
  }

  public void setCountryName(CountryName countryName) {
    this.countryName = countryName;
  }

  public CountryName getCountryName() {
    return this.countryName;
  }

  public void setAdministrationDomainName(AdministrationDomainName administrationDomainName) {
    this.administrationDomainName = administrationDomainName;
  }

  public AdministrationDomainName getAdministrationDomainName() {
    return this.administrationDomainName;
  }

  public void setNetworkAddress(NetworkAddress networkAddress) {
    this.networkAddress = networkAddress;
  }

  public NetworkAddress getNetworkAddress() {
    return this.networkAddress;
  }

  public void setTerminalIdentifier(TerminalIdentifier terminalIdentifier) {
    this.terminalIdentifier = terminalIdentifier;
  }

  public TerminalIdentifier getTerminalIdentifier() {
    return this.terminalIdentifier;
  }

  public void setPrivateDomainName(PrivateDomainName privateDomainName) {
    this.privateDomainName = privateDomainName;
  }

  public PrivateDomainName getPrivateDomainName() {
    return this.privateDomainName;
  }

  public void setOrganizationName(OrganizationName organizationName) {
    this.organizationName = organizationName;
  }

  public OrganizationName getOrganizationName() {
    return this.organizationName;
  }

  public void setNumericUserIdentifier(NumericUserIdentifier numericUserIdentifier) {
    this.numericUserIdentifier = numericUserIdentifier;
  }

  public NumericUserIdentifier getNumericUserIdentifier() {
    return this.numericUserIdentifier;
  }

  public void setPersonalName(PersonalName personalName) {
    this.personalName = personalName;
  }

  public PersonalName getPersonalName() {
    return this.personalName;
  }

  public void setOrganizationalUnitNames(OrganizationalUnitNames organizationalUnitNames) {
    this.organizationalUnitNames = organizationalUnitNames;
  }

  public OrganizationalUnitNames getOrganizationalUnitNames() {
    return this.organizationalUnitNames;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int codeLength;
    if (this.code == null) {
      codeLength = 0;
      if (this.organizationalUnitNames != null) {
        codeLength += this.organizationalUnitNames.encode(os, false);
        os.write(166);
        ++codeLength;
      }

      if (this.personalName != null) {
        codeLength += this.personalName.encode(os, false);
        os.write(165);
        ++codeLength;
      }

      if (this.numericUserIdentifier != null) {
        codeLength += this.numericUserIdentifier.encode(os, false);
        os.write(132);
        ++codeLength;
      }

      if (this.organizationName != null) {
        codeLength += this.organizationName.encode(os, false);
        os.write(131);
        ++codeLength;
      }

      if (this.privateDomainName != null) {
        int sublength = this.privateDomainName.encode(os);
        codeLength += sublength;
        codeLength += BerLength.encodeLength(os, sublength);
        os.write(162);
        ++codeLength;
      }

      if (this.terminalIdentifier != null) {
        codeLength += this.terminalIdentifier.encode(os, false);
        os.write(129);
        ++codeLength;
      }

      if (this.networkAddress != null) {
        codeLength += this.networkAddress.encode(os, false);
        os.write(128);
        ++codeLength;
      }

      if (this.administrationDomainName != null) {
        codeLength += this.administrationDomainName.encode(os, true);
      }

      if (this.countryName != null) {
        codeLength += this.countryName.encode(os, true);
      }

      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }

      return codeLength;
    } else {
      for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
        os.write(this.code[codeLength]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;
    codeLength += totalLength;
    if (totalLength == 0) {
      return codeLength;
    } else {
      subCodeLength = subCodeLength + berTag.decode(is);
      if (berTag.equals(CountryName.tag)) {
        this.countryName = new CountryName();
        subCodeLength += this.countryName.decode(is, false);
        if (subCodeLength == totalLength) {
          return codeLength;
        }

        subCodeLength += berTag.decode(is);
      }

      if (berTag.equals(AdministrationDomainName.tag)) {
        this.administrationDomainName = new AdministrationDomainName();
        subCodeLength += this.administrationDomainName.decode(is, false);
        if (subCodeLength == totalLength) {
          return codeLength;
        }

        subCodeLength += berTag.decode(is);
      }

      if (berTag.equals(128, 0, 0)) {
        this.networkAddress = new NetworkAddress();
        subCodeLength += this.networkAddress.decode(is, false);
        if (subCodeLength == totalLength) {
          return codeLength;
        }

        subCodeLength += berTag.decode(is);
      }

      if (berTag.equals(128, 0, 1)) {
        this.terminalIdentifier = new TerminalIdentifier();
        subCodeLength += this.terminalIdentifier.decode(is, false);
        if (subCodeLength == totalLength) {
          return codeLength;
        }

        subCodeLength += berTag.decode(is);
      }

      if (berTag.equals(128, 32, 2)) {
        subCodeLength += length.decode(is);
        this.privateDomainName = new PrivateDomainName();
        subCodeLength += this.privateDomainName.decode(is, null);
        if (subCodeLength == totalLength) {
          return codeLength;
        }

        subCodeLength += berTag.decode(is);
      }

      if (berTag.equals(128, 0, 3)) {
        this.organizationName = new OrganizationName();
        subCodeLength += this.organizationName.decode(is, false);
        if (subCodeLength == totalLength) {
          return codeLength;
        }

        subCodeLength += berTag.decode(is);
      }

      if (berTag.equals(128, 0, 4)) {
        this.numericUserIdentifier = new NumericUserIdentifier();
        subCodeLength += this.numericUserIdentifier.decode(is, false);
        if (subCodeLength == totalLength) {
          return codeLength;
        }

        subCodeLength += berTag.decode(is);
      }

      if (berTag.equals(128, 32, 5)) {
        this.personalName = new PersonalName();
        subCodeLength += this.personalName.decode(is, false);
        if (subCodeLength == totalLength) {
          return codeLength;
        }

        subCodeLength += berTag.decode(is);
      }

      if (berTag.equals(128, 32, 6)) {
        this.organizationalUnitNames = new OrganizationalUnitNames();
        subCodeLength += this.organizationalUnitNames.decode(is, false);
        if (subCodeLength == totalLength) {
          return codeLength;
        }
      }

      throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    boolean firstSelectedElement = true;
    int i;
    if (this.countryName != null) {
      sb.append("\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("countryName: ");
      this.countryName.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }

    if (this.administrationDomainName != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("administrationDomainName: ");
      this.administrationDomainName.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }

    if (this.networkAddress != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("networkAddress: ").append(this.networkAddress);
      firstSelectedElement = false;
    }

    if (this.terminalIdentifier != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("terminalIdentifier: ").append(this.terminalIdentifier);
      firstSelectedElement = false;
    }

    if (this.privateDomainName != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("privateDomainName: ");
      this.privateDomainName.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }

    if (this.organizationName != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("organizationName: ").append(this.organizationName);
      firstSelectedElement = false;
    }

    if (this.numericUserIdentifier != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("numericUserIdentifier: ").append(this.numericUserIdentifier);
      firstSelectedElement = false;
    }

    if (this.personalName != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("personalName: ");
      this.personalName.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }

    if (this.organizationalUnitNames != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("organizationalUnitNames: ");
      this.organizationalUnitNames.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }
}
