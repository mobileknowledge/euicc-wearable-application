package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class DownloadErrorCode
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public DownloadErrorCode() {}
  
  public DownloadErrorCode(byte[] code)
  {
    super(code);
  }
  
  public DownloadErrorCode(BigInteger value) {
    super(value);
  }
  
  public DownloadErrorCode(long value) {
    super(value);
  }
}
