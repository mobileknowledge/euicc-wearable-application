package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class TotalPartialCrlNumber
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public TotalPartialCrlNumber() {}
  
  public TotalPartialCrlNumber(byte[] code)
  {
    super(code);
  }
  
  public TotalPartialCrlNumber(BigInteger value) {
    super(value);
  }
  
  public TotalPartialCrlNumber(long value) {
    super(value);
  }
}
