package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.string.BerNumericString;
import org.openmuc.jasn1.ber.types.string.BerPrintableString;











public class PrivateDomainName
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  private BerNumericString numeric = null;
  private BerPrintableString printable = null;
  
  public PrivateDomainName() {}
  
  public PrivateDomainName(byte[] code)
  {
    this.code = code;
  }
  
  public void setNumeric(BerNumericString numeric) {
    this.numeric = numeric;
  }
  
  public BerNumericString getNumeric() {
    return numeric;
  }
  
  public void setPrintable(BerPrintableString printable) {
    this.printable = printable;
  }
  
  public BerPrintableString getPrintable() {
    return printable;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (printable != null) {
      codeLength += printable.encode(os, true);
      return codeLength;
    }
    
    if (numeric != null) {
      codeLength += numeric.encode(os, true);
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, null);
  }
  
  public int decode(InputStream is, BerTag berTag) throws IOException
  {
    int codeLength = 0;
    BerTag passedTag = berTag;
    
    if (berTag == null) {
      berTag = new BerTag();
      codeLength += berTag.decode(is);
    }
    
    if (berTag.equals(BerNumericString.tag)) {
      numeric = new BerNumericString();
      codeLength += numeric.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(BerPrintableString.tag)) {
      printable = new BerPrintableString();
      codeLength += printable.decode(is, false);
      return codeLength;
    }
    
    if (passedTag != null) {
      return 0;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (numeric != null) {
      sb.append("numeric: ").append(numeric);
      return;
    }
    
    if (printable != null) {
      sb.append("printable: ").append(printable);
      return;
    }
    
    sb.append("<none>");
  }
}
