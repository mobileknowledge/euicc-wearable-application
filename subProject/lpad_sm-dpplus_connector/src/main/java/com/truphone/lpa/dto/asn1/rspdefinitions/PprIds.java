package com.truphone.lpa.dto.asn1.rspdefinitions;

import org.openmuc.jasn1.ber.types.BerBitString;




















public class PprIds
  extends BerBitString
{
  private static final long serialVersionUID = 1L;
  
  public PprIds() {}
  
  public PprIds(byte[] code)
  {
    super(code);
  }
  
  public PprIds(byte[] value, int numBits) {
    super(value, numBits);
  }
}
