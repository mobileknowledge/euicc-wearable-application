//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;

public class Validity implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  public byte[] code = null;
  private Time notBefore = null;
  private Time notAfter = null;

  public Validity() {
  }

  public Validity(byte[] code) {
    this.code = code;
  }

  public void setNotBefore(Time notBefore) {
    this.notBefore = notBefore;
  }

  public Time getNotBefore() {
    return this.notBefore;
  }

  public void setNotAfter(Time notAfter) {
    this.notAfter = notAfter;
  }

  public Time getNotAfter() {
    return this.notAfter;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int i;
    if (this.code == null) {
      int codeLength = 0;
      i = codeLength + this.notAfter.encode(os);
      i += this.notBefore.encode(os);
      i += BerLength.encodeLength(os, i);
      if (withTag) {
        i += tag.encode(os);
      }

      return i;
    } else {
      for(i = this.code.length - 1; i >= 0; --i) {
        os.write(this.code[i]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;
    codeLength += totalLength;
    subCodeLength = subCodeLength + berTag.decode(is);
    this.notBefore = new Time();
    subCodeLength += this.notBefore.decode(is, berTag);
    subCodeLength += berTag.decode(is);
    this.notAfter = new Time();
    subCodeLength += this.notAfter.decode(is, berTag);
    if (subCodeLength == totalLength) {
      return codeLength;
    } else {
      throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    sb.append("\n");

    int i;
    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.notBefore != null) {
      sb.append("notBefore: ");
      this.notBefore.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("notBefore: <empty-required-field>");
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.notAfter != null) {
      sb.append("notAfter: ");
      this.notAfter.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("notAfter: <empty-required-field>");
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }
}
