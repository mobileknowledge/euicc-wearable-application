package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
















public class RemoteProfileProvisioningResponse
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  public static final BerTag tag = new BerTag(128, 32, 2);
  
  private InitiateAuthenticationResponse initiateAuthenticationResponse = null;
  private AuthenticateClientResponseEs9 authenticateClientResponseEs9 = null;
  private GetBoundProfilePackageResponse getBoundProfilePackageResponse = null;
  private CancelSessionResponseEs9 cancelSessionResponseEs9 = null;
  private AuthenticateClientResponseEs11 authenticateClientResponseEs11 = null;
  
  public RemoteProfileProvisioningResponse() {}
  
  public RemoteProfileProvisioningResponse(byte[] code)
  {
    this.code = code;
  }
  
  public void setInitiateAuthenticationResponse(InitiateAuthenticationResponse initiateAuthenticationResponse) {
    this.initiateAuthenticationResponse = initiateAuthenticationResponse;
  }
  
  public InitiateAuthenticationResponse getInitiateAuthenticationResponse() {
    return initiateAuthenticationResponse;
  }
  
  public void setAuthenticateClientResponseEs9(AuthenticateClientResponseEs9 authenticateClientResponseEs9) {
    this.authenticateClientResponseEs9 = authenticateClientResponseEs9;
  }
  
  public AuthenticateClientResponseEs9 getAuthenticateClientResponseEs9() {
    return authenticateClientResponseEs9;
  }
  
  public void setGetBoundProfilePackageResponse(GetBoundProfilePackageResponse getBoundProfilePackageResponse) {
    this.getBoundProfilePackageResponse = getBoundProfilePackageResponse;
  }
  
  public GetBoundProfilePackageResponse getGetBoundProfilePackageResponse() {
    return getBoundProfilePackageResponse;
  }
  
  public void setCancelSessionResponseEs9(CancelSessionResponseEs9 cancelSessionResponseEs9) {
    this.cancelSessionResponseEs9 = cancelSessionResponseEs9;
  }
  
  public CancelSessionResponseEs9 getCancelSessionResponseEs9() {
    return cancelSessionResponseEs9;
  }
  
  public void setAuthenticateClientResponseEs11(AuthenticateClientResponseEs11 authenticateClientResponseEs11) {
    this.authenticateClientResponseEs11 = authenticateClientResponseEs11;
  }
  
  public AuthenticateClientResponseEs11 getAuthenticateClientResponseEs11() {
    return authenticateClientResponseEs11;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (authenticateClientResponseEs11 != null) {
      codeLength += authenticateClientResponseEs11.encode(os, false);
      
      os.write(64);
      os.write(191);
      codeLength += 2;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (cancelSessionResponseEs9 != null) {
      codeLength += cancelSessionResponseEs9.encode(os, false);
      
      os.write(65);
      os.write(191);
      codeLength += 2;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (getBoundProfilePackageResponse != null) {
      codeLength += getBoundProfilePackageResponse.encode(os, false);
      
      os.write(58);
      os.write(191);
      codeLength += 2;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (authenticateClientResponseEs9 != null) {
      codeLength += authenticateClientResponseEs9.encode(os, false);
      
      os.write(59);
      os.write(191);
      codeLength += 2;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (initiateAuthenticationResponse != null) {
      codeLength += initiateAuthenticationResponse.encode(os, false);
      
      os.write(57);
      os.write(191);
      codeLength += 2;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    BerLength length = new BerLength();
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    codeLength += length.decode(is);
    codeLength += berTag.decode(is);
    
    if (berTag.equals(128, 32, 57)) {
      initiateAuthenticationResponse = new InitiateAuthenticationResponse();
      codeLength += initiateAuthenticationResponse.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 59)) {
      authenticateClientResponseEs9 = new AuthenticateClientResponseEs9();
      codeLength += authenticateClientResponseEs9.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 58)) {
      getBoundProfilePackageResponse = new GetBoundProfilePackageResponse();
      codeLength += getBoundProfilePackageResponse.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 65)) {
      cancelSessionResponseEs9 = new CancelSessionResponseEs9();
      codeLength += cancelSessionResponseEs9.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 64)) {
      authenticateClientResponseEs11 = new AuthenticateClientResponseEs11();
      codeLength += authenticateClientResponseEs11.decode(is, false);
      return codeLength;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (initiateAuthenticationResponse != null) {
      sb.append("initiateAuthenticationResponse: ");
      initiateAuthenticationResponse.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (authenticateClientResponseEs9 != null) {
      sb.append("authenticateClientResponseEs9: ");
      authenticateClientResponseEs9.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (getBoundProfilePackageResponse != null) {
      sb.append("getBoundProfilePackageResponse: ");
      getBoundProfilePackageResponse.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (cancelSessionResponseEs9 != null) {
      sb.append("cancelSessionResponseEs9: ");
      cancelSessionResponseEs9.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (authenticateClientResponseEs11 != null) {
      sb.append("authenticateClientResponseEs11: ");
      authenticateClientResponseEs11.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    sb.append("<none>");
  }
}
