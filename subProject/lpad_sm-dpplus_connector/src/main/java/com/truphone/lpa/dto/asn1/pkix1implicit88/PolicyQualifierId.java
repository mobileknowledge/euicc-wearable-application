package com.truphone.lpa.dto.asn1.pkix1implicit88;

import org.openmuc.jasn1.ber.types.BerObjectIdentifier;






















public class PolicyQualifierId
  extends BerObjectIdentifier
{
  private static final long serialVersionUID = 1L;
  
  public PolicyQualifierId() {}
  
  public PolicyQualifierId(byte[] code)
  {
    super(code);
  }
  
  public PolicyQualifierId(int[] value) {
    super(value);
  }
}
