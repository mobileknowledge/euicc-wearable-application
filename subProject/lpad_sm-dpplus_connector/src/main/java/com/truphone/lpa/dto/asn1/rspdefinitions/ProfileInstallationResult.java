package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
















public class ProfileInstallationResult
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(128, 32, 55);
  
  public byte[] code = null;
  private ProfileInstallationResultData profileInstallationResultData = null;
  private EuiccSignPIR euiccSignPIR = null;
  
  public ProfileInstallationResult() {}
  
  public ProfileInstallationResult(byte[] code)
  {
    this.code = code;
  }
  
  public void setProfileInstallationResultData(ProfileInstallationResultData profileInstallationResultData) {
    this.profileInstallationResultData = profileInstallationResultData;
  }
  
  public ProfileInstallationResultData getProfileInstallationResultData() {
    return profileInstallationResultData;
  }
  
  public void setEuiccSignPIR(EuiccSignPIR euiccSignPIR) {
    this.euiccSignPIR = euiccSignPIR;
  }
  
  public EuiccSignPIR getEuiccSignPIR() {
    return euiccSignPIR;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += euiccSignPIR.encode(os, true);
    
    codeLength += profileInstallationResultData.encode(os, false);
    
    os.write(39);
    os.write(191);
    codeLength += 2;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 32, 39)) {
      profileInstallationResultData = new ProfileInstallationResultData();
      subCodeLength += profileInstallationResultData.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(EuiccSignPIR.tag)) {
      euiccSignPIR = new EuiccSignPIR();
      subCodeLength += euiccSignPIR.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (profileInstallationResultData != null) {
      sb.append("profileInstallationResultData: ");
      profileInstallationResultData.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("profileInstallationResultData: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccSignPIR != null) {
      sb.append("euiccSignPIR: ").append(euiccSignPIR);
    }
    else {
      sb.append("euiccSignPIR: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
