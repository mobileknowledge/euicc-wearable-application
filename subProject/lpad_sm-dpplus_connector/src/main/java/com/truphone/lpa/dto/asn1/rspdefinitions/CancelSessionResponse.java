package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerInteger;















public class CancelSessionResponse
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  public static final BerTag tag = new BerTag(128, 32, 65);
  
  private CancelSessionResponseOk cancelSessionResponseOk = null;
  private BerInteger cancelSessionResponseError = null;
  
  public CancelSessionResponse() {}
  
  public CancelSessionResponse(byte[] code)
  {
    this.code = code;
  }
  
  public void setCancelSessionResponseOk(CancelSessionResponseOk cancelSessionResponseOk) {
    this.cancelSessionResponseOk = cancelSessionResponseOk;
  }
  
  public CancelSessionResponseOk getCancelSessionResponseOk() {
    return cancelSessionResponseOk;
  }
  
  public void setCancelSessionResponseError(BerInteger cancelSessionResponseError) {
    this.cancelSessionResponseError = cancelSessionResponseError;
  }
  
  public BerInteger getCancelSessionResponseError() {
    return cancelSessionResponseError;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (cancelSessionResponseError != null) {
      codeLength += cancelSessionResponseError.encode(os, false);
      
      os.write(129);
      codeLength++;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (cancelSessionResponseOk != null) {
      codeLength += cancelSessionResponseOk.encode(os, false);
      
      os.write(160);
      codeLength++;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    BerLength length = new BerLength();
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    codeLength += length.decode(is);
    codeLength += berTag.decode(is);
    
    if (berTag.equals(128, 32, 0)) {
      cancelSessionResponseOk = new CancelSessionResponseOk();
      codeLength += cancelSessionResponseOk.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 0, 1)) {
      cancelSessionResponseError = new BerInteger();
      codeLength += cancelSessionResponseError.decode(is, false);
      return codeLength;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (cancelSessionResponseOk != null) {
      sb.append("cancelSessionResponseOk: ");
      cancelSessionResponseOk.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (cancelSessionResponseError != null) {
      sb.append("cancelSessionResponseError: ").append(cancelSessionResponseError);
      return;
    }
    
    sb.append("<none>");
  }
}
