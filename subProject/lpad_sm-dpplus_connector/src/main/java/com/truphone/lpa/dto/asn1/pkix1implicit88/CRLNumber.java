package com.truphone.lpa.dto.asn1.pkix1implicit88;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;





















public class CRLNumber
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public CRLNumber() {}
  
  public CRLNumber(byte[] code)
  {
    super(code);
  }
  
  public CRLNumber(BigInteger value) {
    super(value);
  }
  
  public CRLNumber(long value) {
    super(value);
  }
}
