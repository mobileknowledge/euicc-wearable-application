package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.string.BerUTF8String;















public class InitiateAuthenticationRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(128, 32, 57);
  
  public byte[] code = null;
  private Octet16 euiccChallenge = null;
  private BerUTF8String smdpAddress = null;
  private EUICCInfo1 euiccInfo1 = null;
  
  public InitiateAuthenticationRequest() {}
  
  public InitiateAuthenticationRequest(byte[] code)
  {
    this.code = code;
  }
  
  public void setEuiccChallenge(Octet16 euiccChallenge) {
    this.euiccChallenge = euiccChallenge;
  }
  
  public Octet16 getEuiccChallenge() {
    return euiccChallenge;
  }
  
  public void setSmdpAddress(BerUTF8String smdpAddress) {
    this.smdpAddress = smdpAddress;
  }
  
  public BerUTF8String getSmdpAddress() {
    return smdpAddress;
  }
  
  public void setEuiccInfo1(EUICCInfo1 euiccInfo1) {
    this.euiccInfo1 = euiccInfo1;
  }
  
  public EUICCInfo1 getEuiccInfo1() {
    return euiccInfo1;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += euiccInfo1.encode(os, true);
    
    codeLength += smdpAddress.encode(os, false);
    
    os.write(131);
    codeLength++;
    
    codeLength += euiccChallenge.encode(os, false);
    
    os.write(129);
    codeLength++;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 1)) {
      euiccChallenge = new Octet16();
      subCodeLength += euiccChallenge.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 3)) {
      smdpAddress = new BerUTF8String();
      subCodeLength += smdpAddress.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(EUICCInfo1.tag)) {
      euiccInfo1 = new EUICCInfo1();
      subCodeLength += euiccInfo1.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccChallenge != null) {
      sb.append("euiccChallenge: ").append(euiccChallenge);
    }
    else {
      sb.append("euiccChallenge: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (smdpAddress != null) {
      sb.append("smdpAddress: ").append(smdpAddress);
    }
    else {
      sb.append("smdpAddress: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccInfo1 != null) {
      sb.append("euiccInfo1: ");
      euiccInfo1.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("euiccInfo1: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
