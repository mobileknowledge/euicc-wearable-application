package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;
import org.openmuc.jasn1.ber.types.string.BerUTF8String;









public class ProfileInfo
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public ProfileInfo() {}
  
  public static class NotificationConfigurationInfo
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    public static final BerTag tag = new BerTag(0, 32, 16);
    public byte[] code = null;
    private List<NotificationConfigurationInformation> seqOf = null;
    
    public NotificationConfigurationInfo() {
      seqOf = new ArrayList();
    }
    
    public NotificationConfigurationInfo(byte[] code) {
      this.code = code;
    }
    
    public List<NotificationConfigurationInformation> getNotificationConfigurationInformation() {
      if (seqOf == null) {
        seqOf = new ArrayList();
      }
      return seqOf;
    }
    
    public int encode(BerByteArrayOutputStream os) throws IOException {
      return encode(os, true);
    }
    
    public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
    {
      if (code != null) {
        for (int i = code.length - 1; i >= 0; i--) {
          os.write(code[i]);
        }
        if (withTag) {
          return tag.encode(os) + code.length;
        }
        return code.length;
      }
      
      int codeLength = 0;
      for (int i = seqOf.size() - 1; i >= 0; i--) {
        codeLength += seqOf.get(i).encode(os, true);
      }
      
      codeLength += BerLength.encodeLength(os, codeLength);
      
      if (withTag) {
        codeLength += tag.encode(os);
      }
      
      return codeLength;
    }
    
    public int decode(InputStream is) throws IOException {
      return decode(is, true);
    }
    
    public int decode(InputStream is, boolean withTag) throws IOException {
      int codeLength = 0;
      int subCodeLength = 0;
      if (withTag) {
        codeLength += tag.decodeAndCheck(is);
      }
      
      BerLength length = new BerLength();
      codeLength += length.decode(is);
      int totalLength = length.val;
      
      while (subCodeLength < totalLength) {
        NotificationConfigurationInformation element = new NotificationConfigurationInformation();
        subCodeLength += element.decode(is, true);
        seqOf.add(element);
      }
      if (subCodeLength != totalLength) {
        throw new IOException("Decoded SequenceOf or SetOf has wrong length. Expected " + totalLength + " but has " + subCodeLength);
      }
      
      codeLength += subCodeLength;
      
      return codeLength;
    }
    
    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      encode(os, false);
      code = os.getArray();
    }
    
    public String toString() {
      StringBuilder sb = new StringBuilder();
      appendAsString(sb, 0);
      return sb.toString();
    }
    
    public void appendAsString(StringBuilder sb, int indentLevel)
    {
      sb.append("{\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      if (seqOf == null) {
        sb.append("null");
      }
      else {
        Iterator<NotificationConfigurationInformation> it = seqOf.iterator();
        if (it.hasNext()) {
          it.next().appendAsString(sb, indentLevel + 1);
          while (it.hasNext()) {
            sb.append(",\n");
            for (int i = 0; i < indentLevel + 1; i++) {
              sb.append("\t");
            }
            it.next().appendAsString(sb, indentLevel + 1);
          }
        }
      }
      
      sb.append("\n");
      for (int i = 0; i < indentLevel; i++) {
        sb.append("\t");
      }
      sb.append("}");
    }
  }
  

  public static final BerTag tag = new BerTag(192, 32, 3);
  
  public byte[] code = null;
  private Iccid iccid = null;
  private OctetTo16 isdpAid = null;
  private ProfileState profileState = null;
  private BerUTF8String profileNickname = null;
  private BerUTF8String serviceProviderName = null;
  private BerUTF8String profileName = null;
  private IconType iconType = null;
  private BerOctetString icon = null;
  private ProfileClass profileClass = null;
  private NotificationConfigurationInfo notificationConfigurationInfo = null;
  private OperatorID profileOwner = null;
  private DpProprietaryData dpProprietaryData = null;
  private PprIds profilePolicyRules = null;
  


  public ProfileInfo(byte[] code)
  {
    this.code = code;
  }
  
  public void setIccid(Iccid iccid) {
    this.iccid = iccid;
  }
  
  public Iccid getIccid() {
    return iccid;
  }
  
  public void setIsdpAid(OctetTo16 isdpAid) {
    this.isdpAid = isdpAid;
  }
  
  public OctetTo16 getIsdpAid() {
    return isdpAid;
  }
  
  public void setProfileState(ProfileState profileState) {
    this.profileState = profileState;
  }
  
  public ProfileState getProfileState() {
    return profileState;
  }
  
  public void setProfileNickname(BerUTF8String profileNickname) {
    this.profileNickname = profileNickname;
  }
  
  public BerUTF8String getProfileNickname() {
    return profileNickname;
  }
  
  public void setServiceProviderName(BerUTF8String serviceProviderName) {
    this.serviceProviderName = serviceProviderName;
  }
  
  public BerUTF8String getServiceProviderName() {
    return serviceProviderName;
  }
  
  public void setProfileName(BerUTF8String profileName) {
    this.profileName = profileName;
  }
  
  public BerUTF8String getProfileName() {
    return profileName;
  }
  
  public void setIconType(IconType iconType) {
    this.iconType = iconType;
  }
  
  public IconType getIconType() {
    return iconType;
  }
  
  public void setIcon(BerOctetString icon) {
    this.icon = icon;
  }
  
  public BerOctetString getIcon() {
    return icon;
  }
  
  public void setProfileClass(ProfileClass profileClass) {
    this.profileClass = profileClass;
  }
  
  public ProfileClass getProfileClass() {
    return profileClass;
  }
  
  public void setNotificationConfigurationInfo(NotificationConfigurationInfo notificationConfigurationInfo) {
    this.notificationConfigurationInfo = notificationConfigurationInfo;
  }
  
  public NotificationConfigurationInfo getNotificationConfigurationInfo() {
    return notificationConfigurationInfo;
  }
  
  public void setProfileOwner(OperatorID profileOwner) {
    this.profileOwner = profileOwner;
  }
  
  public OperatorID getProfileOwner() {
    return profileOwner;
  }
  
  public void setDpProprietaryData(DpProprietaryData dpProprietaryData) {
    this.dpProprietaryData = dpProprietaryData;
  }
  
  public DpProprietaryData getDpProprietaryData() {
    return dpProprietaryData;
  }
  
  public void setProfilePolicyRules(PprIds profilePolicyRules) {
    this.profilePolicyRules = profilePolicyRules;
  }
  
  public PprIds getProfilePolicyRules() {
    return profilePolicyRules;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (profilePolicyRules != null) {
      codeLength += profilePolicyRules.encode(os, false);
      
      os.write(153);
      codeLength++;
    }
    
    if (dpProprietaryData != null) {
      codeLength += dpProprietaryData.encode(os, false);
      
      os.write(184);
      codeLength++;
    }
    
    if (profileOwner != null) {
      codeLength += profileOwner.encode(os, false);
      
      os.write(183);
      codeLength++;
    }
    
    if (notificationConfigurationInfo != null) {
      codeLength += notificationConfigurationInfo.encode(os, false);
      
      os.write(182);
      codeLength++;
    }
    
    if (profileClass != null) {
      codeLength += profileClass.encode(os, false);
      
      os.write(149);
      codeLength++;
    }
    
    if (icon != null) {
      codeLength += icon.encode(os, false);
      
      os.write(148);
      codeLength++;
    }
    
    if (iconType != null) {
      codeLength += iconType.encode(os, false);
      
      os.write(147);
      codeLength++;
    }
    
    if (profileName != null) {
      codeLength += profileName.encode(os, false);
      
      os.write(146);
      codeLength++;
    }
    
    if (serviceProviderName != null) {
      codeLength += serviceProviderName.encode(os, false);
      
      os.write(145);
      codeLength++;
    }
    
    if (profileNickname != null) {
      codeLength += profileNickname.encode(os, false);
      
      os.write(144);
      codeLength++;
    }
    
    if (profileState != null) {
      codeLength += profileState.encode(os, false);
      
      os.write(112);
      os.write(159);
      codeLength += 2;
    }
    
    if (isdpAid != null) {
      codeLength += isdpAid.encode(os, false);
      
      os.write(79);
      codeLength++;
    }
    
    if (iccid != null) {
      codeLength += iccid.encode(os, true);
    }
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    if (totalLength == 0) {
      return codeLength;
    }
    subCodeLength += berTag.decode(is);
    if (berTag.equals(Iccid.tag)) {
      iccid = new Iccid();
      subCodeLength += iccid.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(64, 0, 15)) {
      isdpAid = new OctetTo16();
      subCodeLength += isdpAid.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 112)) {
      profileState = new ProfileState();
      subCodeLength += profileState.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 16)) {
      profileNickname = new BerUTF8String();
      subCodeLength += profileNickname.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 17)) {
      serviceProviderName = new BerUTF8String();
      subCodeLength += serviceProviderName.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 18)) {
      profileName = new BerUTF8String();
      subCodeLength += profileName.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 19)) {
      iconType = new IconType();
      subCodeLength += iconType.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 20)) {
      icon = new BerOctetString();
      subCodeLength += icon.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 21)) {
      profileClass = new ProfileClass();
      subCodeLength += profileClass.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 32, 22)) {
      notificationConfigurationInfo = new NotificationConfigurationInfo();
      subCodeLength += notificationConfigurationInfo.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 32, 23)) {
      profileOwner = new OperatorID();
      subCodeLength += profileOwner.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 32, 24)) {
      dpProprietaryData = new DpProprietaryData();
      subCodeLength += dpProprietaryData.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 25)) {
      profilePolicyRules = new PprIds();
      subCodeLength += profilePolicyRules.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    boolean firstSelectedElement = true;
    if (iccid != null) {
      sb.append("\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("iccid: ").append(iccid);
      firstSelectedElement = false;
    }
    
    if (isdpAid != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("isdpAid: ").append(isdpAid);
      firstSelectedElement = false;
    }
    
    if (profileState != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("profileState: ").append(profileState);
      firstSelectedElement = false;
    }
    
    if (profileNickname != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("profileNickname: ").append(profileNickname);
      firstSelectedElement = false;
    }
    
    if (serviceProviderName != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("serviceProviderName: ").append(serviceProviderName);
      firstSelectedElement = false;
    }
    
    if (profileName != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("profileName: ").append(profileName);
      firstSelectedElement = false;
    }
    
    if (iconType != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("iconType: ").append(iconType);
      firstSelectedElement = false;
    }
    
    if (icon != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("icon: ").append(icon);
      firstSelectedElement = false;
    }
    
    if (profileClass != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("profileClass: ").append(profileClass);
      firstSelectedElement = false;
    }
    
    if (notificationConfigurationInfo != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("notificationConfigurationInfo: ");
      notificationConfigurationInfo.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }
    
    if (profileOwner != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("profileOwner: ");
      profileOwner.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }
    
    if (dpProprietaryData != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("dpProprietaryData: ");
      dpProprietaryData.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }
    
    if (profilePolicyRules != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("profilePolicyRules: ").append(profilePolicyRules);
      firstSelectedElement = false;
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
