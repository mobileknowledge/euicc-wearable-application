package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerInteger;
import org.openmuc.jasn1.ber.types.string.BerUTF8String;














public class NotificationMetadata
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(128, 32, 47);
  
  public byte[] code = null;
  private BerInteger seqNumber = null;
  private NotificationEvent profileManagementOperation = null;
  private BerUTF8String notificationAddress = null;
  private Iccid iccid = null;
  
  public NotificationMetadata() {}
  
  public NotificationMetadata(byte[] code)
  {
    this.code = code;
  }
  
  public void setSeqNumber(BerInteger seqNumber) {
    this.seqNumber = seqNumber;
  }
  
  public BerInteger getSeqNumber() {
    return seqNumber;
  }
  
  public void setProfileManagementOperation(NotificationEvent profileManagementOperation) {
    this.profileManagementOperation = profileManagementOperation;
  }
  
  public NotificationEvent getProfileManagementOperation() {
    return profileManagementOperation;
  }
  
  public void setNotificationAddress(BerUTF8String notificationAddress) {
    this.notificationAddress = notificationAddress;
  }
  
  public BerUTF8String getNotificationAddress() {
    return notificationAddress;
  }
  
  public void setIccid(Iccid iccid) {
    this.iccid = iccid;
  }
  
  public Iccid getIccid() {
    return iccid;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (iccid != null) {
      codeLength += iccid.encode(os, true);
    }
    
    codeLength += notificationAddress.encode(os, true);
    
    codeLength += profileManagementOperation.encode(os, false);
    
    os.write(129);
    codeLength++;
    
    codeLength += seqNumber.encode(os, false);
    
    os.write(128);
    codeLength++;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 0)) {
      seqNumber = new BerInteger();
      subCodeLength += seqNumber.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 1)) {
      profileManagementOperation = new NotificationEvent();
      subCodeLength += profileManagementOperation.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(BerUTF8String.tag)) {
      notificationAddress = new BerUTF8String();
      subCodeLength += notificationAddress.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(Iccid.tag)) {
      iccid = new Iccid();
      subCodeLength += iccid.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (seqNumber != null) {
      sb.append("seqNumber: ").append(seqNumber);
    }
    else {
      sb.append("seqNumber: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (profileManagementOperation != null) {
      sb.append("profileManagementOperation: ").append(profileManagementOperation);
    }
    else {
      sb.append("profileManagementOperation: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (notificationAddress != null) {
      sb.append("notificationAddress: ").append(notificationAddress);
    }
    else {
      sb.append("notificationAddress: <empty-required-field>");
    }
    
    if (iccid != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("iccid: ").append(iccid);
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
