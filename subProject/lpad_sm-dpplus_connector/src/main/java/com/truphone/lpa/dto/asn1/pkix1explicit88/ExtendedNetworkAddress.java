//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.string.BerNumericString;

public class ExtendedNetworkAddress implements Serializable {
    private static final long serialVersionUID = 1L;
    public byte[] code = null;
    private ExtendedNetworkAddress.E1634Address e1634Address = null;
    private PresentationAddress psapAddress = null;

    public ExtendedNetworkAddress() {
    }

    public ExtendedNetworkAddress(byte[] code) {
        this.code = code;
    }

    public void setE1634Address(ExtendedNetworkAddress.E1634Address e1634Address) {
        this.e1634Address = e1634Address;
    }

    public ExtendedNetworkAddress.E1634Address getE1634Address() {
        return this.e1634Address;
    }

    public void setPsapAddress(PresentationAddress psapAddress) {
        this.psapAddress = psapAddress;
    }

    public PresentationAddress getPsapAddress() {
        return this.psapAddress;
    }

    public int encode(BerByteArrayOutputStream os) throws IOException {
        int codeLength;
        if (this.code == null) {
            codeLength = 0;
            if (this.psapAddress != null) {
                codeLength = codeLength + this.psapAddress.encode(os, false);
                os.write(160);
                ++codeLength;
                return codeLength;
            } else if (this.e1634Address != null) {
                codeLength = codeLength + this.e1634Address.encode(os, true);
                return codeLength;
            } else {
                throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
            }
        } else {
            for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
                os.write(this.code[codeLength]);
            }

            return this.code.length;
        }
    }

    public int decode(InputStream is) throws IOException {
        return this.decode(is, null);
    }

    public int decode(InputStream is, BerTag berTag) throws IOException {
        int codeLength = 0;
        BerTag passedTag = berTag;
        if (berTag == null) {
            berTag = new BerTag();
            codeLength += berTag.decode(is);
        }

        if (berTag.equals(ExtendedNetworkAddress.E1634Address.tag)) {
            this.e1634Address = new ExtendedNetworkAddress.E1634Address();
            codeLength += this.e1634Address.decode(is, false);
            return codeLength;
        } else if (berTag.equals(128, 32, 0)) {
            this.psapAddress = new PresentationAddress();
            codeLength += this.psapAddress.decode(is, false);
            return codeLength;
        } else if (passedTag != null) {
            return 0;
        } else {
            throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
        }
    }

    public void encodeAndSave(int encodingSizeGuess) throws IOException {
        BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
        this.encode(os);
        this.code = os.getArray();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        this.appendAsString(sb, 0);
        return sb.toString();
    }

    public void appendAsString(StringBuilder sb, int indentLevel) {
        if (this.e1634Address != null) {
            sb.append("e1634Address: ");
            this.e1634Address.appendAsString(sb, indentLevel + 1);
        } else if (this.psapAddress != null) {
            sb.append("psapAddress: ");
            this.psapAddress.appendAsString(sb, indentLevel + 1);
        } else {
            sb.append("<none>");
        }
    }

    public static class E1634Address implements Serializable {
        private static final long serialVersionUID = 1L;
        public static final BerTag tag = new BerTag(0, 32, 16);
        public byte[] code = null;
        private BerNumericString number = null;
        private BerNumericString subAddress = null;

        public E1634Address() {
        }

        public E1634Address(byte[] code) {
            this.code = code;
        }

        public void setNumber(BerNumericString number) {
            this.number = number;
        }

        public BerNumericString getNumber() {
            return this.number;
        }

        public void setSubAddress(BerNumericString subAddress) {
            this.subAddress = subAddress;
        }

        public BerNumericString getSubAddress() {
            return this.subAddress;
        }

        public int encode(BerByteArrayOutputStream os) throws IOException {
            return this.encode(os, true);
        }

        public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
            int codeLength;
            if (this.code == null) {
                codeLength = 0;
                if (this.subAddress != null) {
                    codeLength += this.subAddress.encode(os, false);
                    os.write(129);
                    ++codeLength;
                }

                codeLength += this.number.encode(os, false);
                os.write(128);
                ++codeLength;
                codeLength += BerLength.encodeLength(os, codeLength);
                if (withTag) {
                    codeLength += tag.encode(os);
                }

                return codeLength;
            } else {
                for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
                    os.write(this.code[codeLength]);
                }

                return withTag ? tag.encode(os) + this.code.length : this.code.length;
            }
        }

        public int decode(InputStream is) throws IOException {
            return this.decode(is, true);
        }

        public int decode(InputStream is, boolean withTag) throws IOException {
            int codeLength = 0;
            int subCodeLength = 0;
            BerTag berTag = new BerTag();
            if (withTag) {
                codeLength += tag.decodeAndCheck(is);
            }

            BerLength length = new BerLength();
            codeLength += length.decode(is);
            int totalLength = length.val;
            codeLength += totalLength;
            subCodeLength = subCodeLength + berTag.decode(is);
            if (berTag.equals(128, 0, 0)) {
                this.number = new BerNumericString();
                subCodeLength += this.number.decode(is, false);
                if (subCodeLength == totalLength) {
                    return codeLength;
                } else {
                    subCodeLength += berTag.decode(is);
                    if (berTag.equals(128, 0, 1)) {
                        this.subAddress = new BerNumericString();
                        subCodeLength += this.subAddress.decode(is, false);
                        if (subCodeLength == totalLength) {
                            return codeLength;
                        }
                    }

                    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
                }
            } else {
                throw new IOException("Tag does not match the mandatory sequence element tag.");
            }
        }

        public void encodeAndSave(int encodingSizeGuess) throws IOException {
            BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
            this.encode(os, false);
            this.code = os.getArray();
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            this.appendAsString(sb, 0);
            return sb.toString();
        }

        public void appendAsString(StringBuilder sb, int indentLevel) {
            sb.append("{");
            sb.append("\n");

            int i;
            for(i = 0; i < indentLevel + 1; ++i) {
                sb.append("\t");
            }

            if (this.number != null) {
                sb.append("number: ").append(this.number);
            } else {
                sb.append("number: <empty-required-field>");
            }

            if (this.subAddress != null) {
                sb.append(",\n");

                for(i = 0; i < indentLevel + 1; ++i) {
                    sb.append("\t");
                }

                sb.append("subAddress: ").append(this.subAddress);
            }

            sb.append("\n");

            for(i = 0; i < indentLevel; ++i) {
                sb.append("\t");
            }

            sb.append("}");
        }
    }
}
