package com.truphone.lpa.dto.asn1.pkix1implicit88;

import com.truphone.lpa.dto.asn1.pkix1explicit88.Name;
import com.truphone.lpa.dto.asn1.pkix1explicit88.ORAddress;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerObjectIdentifier;
import org.openmuc.jasn1.ber.types.BerOctetString;
import org.openmuc.jasn1.ber.types.string.BerIA5String;













public class GeneralName
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  private AnotherName otherName = null;
  private BerIA5String rfc822Name = null;
  private BerIA5String dNSName = null;
  private ORAddress x400Address = null;
  private Name directoryName = null;
  private EDIPartyName ediPartyName = null;
  private BerIA5String uniformResourceIdentifier = null;
  private BerOctetString iPAddress = null;
  private BerObjectIdentifier registeredID = null;
  
  public GeneralName() {}
  
  public GeneralName(byte[] code)
  {
    this.code = code;
  }
  
  public void setOtherName(AnotherName otherName) {
    this.otherName = otherName;
  }
  
  public AnotherName getOtherName() {
    return otherName;
  }
  
  public void setRfc822Name(BerIA5String rfc822Name) {
    this.rfc822Name = rfc822Name;
  }
  
  public BerIA5String getRfc822Name() {
    return rfc822Name;
  }
  
  public void setDNSName(BerIA5String dNSName) {
    this.dNSName = dNSName;
  }
  
  public BerIA5String getDNSName() {
    return dNSName;
  }
  
  public void setX400Address(ORAddress x400Address) {
    this.x400Address = x400Address;
  }
  
  public ORAddress getX400Address() {
    return x400Address;
  }
  
  public void setDirectoryName(Name directoryName) {
    this.directoryName = directoryName;
  }
  
  public Name getDirectoryName() {
    return directoryName;
  }
  
  public void setEdiPartyName(EDIPartyName ediPartyName) {
    this.ediPartyName = ediPartyName;
  }
  
  public EDIPartyName getEdiPartyName() {
    return ediPartyName;
  }
  
  public void setUniformResourceIdentifier(BerIA5String uniformResourceIdentifier) {
    this.uniformResourceIdentifier = uniformResourceIdentifier;
  }
  
  public BerIA5String getUniformResourceIdentifier() {
    return uniformResourceIdentifier;
  }
  
  public void setIPAddress(BerOctetString iPAddress) {
    this.iPAddress = iPAddress;
  }
  
  public BerOctetString getIPAddress() {
    return iPAddress;
  }
  
  public void setRegisteredID(BerObjectIdentifier registeredID) {
    this.registeredID = registeredID;
  }
  
  public BerObjectIdentifier getRegisteredID() {
    return registeredID;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      return code.length;
    }
    
    int codeLength = 0;
    

    if (registeredID != null) {
      codeLength += registeredID.encode(os, false);
      
      os.write(136);
      codeLength++;
      return codeLength;
    }
    
    if (iPAddress != null) {
      codeLength += iPAddress.encode(os, false);
      
      os.write(135);
      codeLength++;
      return codeLength;
    }
    
    if (uniformResourceIdentifier != null) {
      codeLength += uniformResourceIdentifier.encode(os, false);
      
      os.write(134);
      codeLength++;
      return codeLength;
    }
    
    if (ediPartyName != null) {
      codeLength += ediPartyName.encode(os, false);
      
      os.write(165);
      codeLength++;
      return codeLength;
    }
    
    if (directoryName != null) {
      int sublength = directoryName.encode(os);
      codeLength += sublength;
      codeLength += BerLength.encodeLength(os, sublength);
      
      os.write(164);
      codeLength++;
      return codeLength;
    }
    
    if (x400Address != null) {
      codeLength += x400Address.encode(os, false);
      
      os.write(163);
      codeLength++;
      return codeLength;
    }
    
    if (dNSName != null) {
      codeLength += dNSName.encode(os, false);
      
      os.write(130);
      codeLength++;
      return codeLength;
    }
    
    if (rfc822Name != null) {
      codeLength += rfc822Name.encode(os, false);
      
      os.write(129);
      codeLength++;
      return codeLength;
    }
    
    if (otherName != null) {
      codeLength += otherName.encode(os, false);
      
      os.write(160);
      codeLength++;
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, null);
  }
  
  public int decode(InputStream is, BerTag berTag) throws IOException
  {
    int codeLength = 0;
    BerTag passedTag = berTag;
    
    if (berTag == null) {
      berTag = new BerTag();
      codeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 32, 0)) {
      otherName = new AnotherName();
      codeLength += otherName.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 0, 1)) {
      rfc822Name = new BerIA5String();
      codeLength += rfc822Name.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 0, 2)) {
      dNSName = new BerIA5String();
      codeLength += dNSName.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 3)) {
      x400Address = new ORAddress();
      codeLength += x400Address.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 4)) {
      codeLength += BerLength.skip(is);
      directoryName = new Name();
      codeLength += directoryName.decode(is, null);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 5)) {
      ediPartyName = new EDIPartyName();
      codeLength += ediPartyName.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 0, 6)) {
      uniformResourceIdentifier = new BerIA5String();
      codeLength += uniformResourceIdentifier.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 0, 7)) {
      iPAddress = new BerOctetString();
      codeLength += iPAddress.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 0, 8)) {
      registeredID = new BerObjectIdentifier();
      codeLength += registeredID.decode(is, false);
      return codeLength;
    }
    
    if (passedTag != null) {
      return 0;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (otherName != null) {
      sb.append("otherName: ");
      otherName.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (rfc822Name != null) {
      sb.append("rfc822Name: ").append(rfc822Name);
      return;
    }
    
    if (dNSName != null) {
      sb.append("dNSName: ").append(dNSName);
      return;
    }
    
    if (x400Address != null) {
      sb.append("x400Address: ");
      x400Address.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (directoryName != null) {
      sb.append("directoryName: ");
      directoryName.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (ediPartyName != null) {
      sb.append("ediPartyName: ");
      ediPartyName.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (uniformResourceIdentifier != null) {
      sb.append("uniformResourceIdentifier: ").append(uniformResourceIdentifier);
      return;
    }
    
    if (iPAddress != null) {
      sb.append("iPAddress: ").append(iPAddress);
      return;
    }
    
    if (registeredID != null) {
      sb.append("registeredID: ").append(registeredID);
      return;
    }
    
    sb.append("<none>");
  }
}
