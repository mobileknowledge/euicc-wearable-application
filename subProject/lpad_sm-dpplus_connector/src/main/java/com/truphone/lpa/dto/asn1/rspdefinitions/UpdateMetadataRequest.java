package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;
import org.openmuc.jasn1.ber.types.string.BerUTF8String;














public class UpdateMetadataRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(128, 32, 42);
  
  public byte[] code = null;
  private BerUTF8String serviceProviderName = null;
  private BerUTF8String profileName = null;
  private IconType iconType = null;
  private BerOctetString icon = null;
  private PprIds profilePolicyRules = null;
  
  public UpdateMetadataRequest() {}
  
  public UpdateMetadataRequest(byte[] code)
  {
    this.code = code;
  }
  
  public void setServiceProviderName(BerUTF8String serviceProviderName) {
    this.serviceProviderName = serviceProviderName;
  }
  
  public BerUTF8String getServiceProviderName() {
    return serviceProviderName;
  }
  
  public void setProfileName(BerUTF8String profileName) {
    this.profileName = profileName;
  }
  
  public BerUTF8String getProfileName() {
    return profileName;
  }
  
  public void setIconType(IconType iconType) {
    this.iconType = iconType;
  }
  
  public IconType getIconType() {
    return iconType;
  }
  
  public void setIcon(BerOctetString icon) {
    this.icon = icon;
  }
  
  public BerOctetString getIcon() {
    return icon;
  }
  
  public void setProfilePolicyRules(PprIds profilePolicyRules) {
    this.profilePolicyRules = profilePolicyRules;
  }
  
  public PprIds getProfilePolicyRules() {
    return profilePolicyRules;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (profilePolicyRules != null) {
      codeLength += profilePolicyRules.encode(os, false);
      
      os.write(153);
      codeLength++;
    }
    
    if (icon != null) {
      codeLength += icon.encode(os, false);
      
      os.write(148);
      codeLength++;
    }
    
    if (iconType != null) {
      codeLength += iconType.encode(os, false);
      
      os.write(147);
      codeLength++;
    }
    
    if (profileName != null) {
      codeLength += profileName.encode(os, false);
      
      os.write(146);
      codeLength++;
    }
    
    if (serviceProviderName != null) {
      codeLength += serviceProviderName.encode(os, false);
      
      os.write(145);
      codeLength++;
    }
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    if (totalLength == 0) {
      return codeLength;
    }
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 17)) {
      serviceProviderName = new BerUTF8String();
      subCodeLength += serviceProviderName.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 18)) {
      profileName = new BerUTF8String();
      subCodeLength += profileName.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 19)) {
      iconType = new IconType();
      subCodeLength += iconType.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 20)) {
      icon = new BerOctetString();
      subCodeLength += icon.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 25)) {
      profilePolicyRules = new PprIds();
      subCodeLength += profilePolicyRules.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    boolean firstSelectedElement = true;
    if (serviceProviderName != null) {
      sb.append("\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("serviceProviderName: ").append(serviceProviderName);
      firstSelectedElement = false;
    }
    
    if (profileName != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("profileName: ").append(profileName);
      firstSelectedElement = false;
    }
    
    if (iconType != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("iconType: ").append(iconType);
      firstSelectedElement = false;
    }
    
    if (icon != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("icon: ").append(icon);
      firstSelectedElement = false;
    }
    
    if (profilePolicyRules != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("profilePolicyRules: ").append(profilePolicyRules);
      firstSelectedElement = false;
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
