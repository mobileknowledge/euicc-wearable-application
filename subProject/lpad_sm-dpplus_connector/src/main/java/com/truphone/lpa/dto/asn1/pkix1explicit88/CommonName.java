package com.truphone.lpa.dto.asn1.pkix1explicit88;

import org.openmuc.jasn1.ber.types.string.BerPrintableString;
















public class CommonName
  extends BerPrintableString
{
  private static final long serialVersionUID = 1L;
  
  public CommonName() {}
  
  public CommonName(byte[] value)
  {
    super(value);
  }
}
