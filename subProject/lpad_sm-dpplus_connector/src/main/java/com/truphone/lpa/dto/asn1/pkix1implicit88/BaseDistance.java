package com.truphone.lpa.dto.asn1.pkix1implicit88;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;





















public class BaseDistance
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public BaseDistance() {}
  
  public BaseDistance(byte[] code)
  {
    super(code);
  }
  
  public BaseDistance(BigInteger value) {
    super(value);
  }
  
  public BaseDistance(long value) {
    super(value);
  }
}
