//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;

public class PresentationAddress implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  public byte[] code = null;
  private BerOctetString pSelector = null;
  private BerOctetString sSelector = null;
  private BerOctetString tSelector = null;
  private PresentationAddress.NAddresses nAddresses = null;

  public PresentationAddress() {
  }

  public PresentationAddress(byte[] code) {
    this.code = code;
  }

  public void setPSelector(BerOctetString pSelector) {
    this.pSelector = pSelector;
  }

  public BerOctetString getPSelector() {
    return this.pSelector;
  }

  public void setSSelector(BerOctetString sSelector) {
    this.sSelector = sSelector;
  }

  public BerOctetString getSSelector() {
    return this.sSelector;
  }

  public void setTSelector(BerOctetString tSelector) {
    this.tSelector = tSelector;
  }

  public BerOctetString getTSelector() {
    return this.tSelector;
  }

  public void setNAddresses(PresentationAddress.NAddresses nAddresses) {
    this.nAddresses = nAddresses;
  }

  public PresentationAddress.NAddresses getNAddresses() {
    return this.nAddresses;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int codeLength;
    if (this.code == null) {
      codeLength = 0;
      int sublength = this.nAddresses.encode(os, true);
      codeLength = codeLength + sublength;
      codeLength += BerLength.encodeLength(os, sublength);
      os.write(163);
      ++codeLength;
      if (this.tSelector != null) {
        sublength = this.tSelector.encode(os, true);
        codeLength += sublength;
        codeLength += BerLength.encodeLength(os, sublength);
        os.write(162);
        ++codeLength;
      }

      if (this.sSelector != null) {
        sublength = this.sSelector.encode(os, true);
        codeLength += sublength;
        codeLength += BerLength.encodeLength(os, sublength);
        os.write(161);
        ++codeLength;
      }

      if (this.pSelector != null) {
        sublength = this.pSelector.encode(os, true);
        codeLength += sublength;
        codeLength += BerLength.encodeLength(os, sublength);
        os.write(160);
        ++codeLength;
      }

      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }

      return codeLength;
    } else {
      for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
        os.write(this.code[codeLength]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;
    codeLength += totalLength;
    subCodeLength = subCodeLength + berTag.decode(is);
    if (berTag.equals(128, 32, 0)) {
      subCodeLength += length.decode(is);
      this.pSelector = new BerOctetString();
      subCodeLength += this.pSelector.decode(is, true);
      subCodeLength += berTag.decode(is);
    }

    if (berTag.equals(128, 32, 1)) {
      subCodeLength += length.decode(is);
      this.sSelector = new BerOctetString();
      subCodeLength += this.sSelector.decode(is, true);
      subCodeLength += berTag.decode(is);
    }

    if (berTag.equals(128, 32, 2)) {
      subCodeLength += length.decode(is);
      this.tSelector = new BerOctetString();
      subCodeLength += this.tSelector.decode(is, true);
      subCodeLength += berTag.decode(is);
    }

    if (berTag.equals(128, 32, 3)) {
      subCodeLength += length.decode(is);
      this.nAddresses = new PresentationAddress.NAddresses();
      subCodeLength += this.nAddresses.decode(is, true);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }

    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    boolean firstSelectedElement = true;
    int i;
    if (this.pSelector != null) {
      sb.append("\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("pSelector: ").append(this.pSelector);
      firstSelectedElement = false;
    }

    if (this.sSelector != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("sSelector: ").append(this.sSelector);
      firstSelectedElement = false;
    }

    if (this.tSelector != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("tSelector: ").append(this.tSelector);
      firstSelectedElement = false;
    }

    if (!firstSelectedElement) {
      sb.append(",\n");
    }

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.nAddresses != null) {
      sb.append("nAddresses: ");
      this.nAddresses.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("nAddresses: <empty-required-field>");
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }

  public static class NAddresses implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final BerTag tag = new BerTag(0, 32, 17);
    public byte[] code = null;
    private List<BerOctetString> seqOf = null;

    public NAddresses() {
      this.seqOf = new ArrayList();
    }

    public NAddresses(byte[] code) {
      this.code = code;
    }

    public List<BerOctetString> getBerOctetString() {
      if (this.seqOf == null) {
        this.seqOf = new ArrayList();
      }

      return this.seqOf;
    }

    public int encode(BerByteArrayOutputStream os) throws IOException {
      return this.encode(os, true);
    }

    public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
      int codeLength;
      if (this.code != null) {
        for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
          os.write(this.code[codeLength]);
        }

        return withTag ? tag.encode(os) + this.code.length : this.code.length;
      } else {
        codeLength = 0;

        for(int i = this.seqOf.size() - 1; i >= 0; --i) {
          codeLength += this.seqOf.get(i).encode(os, true);
        }

        codeLength += BerLength.encodeLength(os, codeLength);
        if (withTag) {
          codeLength += tag.encode(os);
        }

        return codeLength;
      }
    }

    public int decode(InputStream is) throws IOException {
      return this.decode(is, true);
    }

    public int decode(InputStream is, boolean withTag) throws IOException {
      int codeLength = 0;
      int subCodeLength = 0;
      if (withTag) {
        codeLength += tag.decodeAndCheck(is);
      }

      BerLength length = new BerLength();
      codeLength += length.decode(is);
      int totalLength = length.val;

      while(subCodeLength < totalLength) {
        BerOctetString element = new BerOctetString();
        subCodeLength += element.decode(is, true);
        this.seqOf.add(element);
      }

      if (subCodeLength != totalLength) {
        throw new IOException("Decoded SequenceOf or SetOf has wrong length. Expected " + totalLength + " but has " + subCodeLength);
      } else {
        codeLength += subCodeLength;
        return codeLength;
      }
    }

    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      this.encode(os, false);
      this.code = os.getArray();
    }

    public String toString() {
      StringBuilder sb = new StringBuilder();
      this.appendAsString(sb, 0);
      return sb.toString();
    }

    public void appendAsString(StringBuilder sb, int indentLevel) {
      sb.append("{\n");

      int i;
      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      if (this.seqOf == null) {
        sb.append("null");
      } else {
        Iterator<BerOctetString> it = this.seqOf.iterator();
        if (it.hasNext()) {
          sb.append(it.next());

          while(it.hasNext()) {
            sb.append(",\n");

            for( i = 0; i < indentLevel + 1; ++i) {
              sb.append("\t");
            }

            sb.append(it.next());
          }
        }
      }

      sb.append("\n");

      for(i = 0; i < indentLevel; ++i) {
        sb.append("\t");
      }

      sb.append("}");
    }
  }
}
