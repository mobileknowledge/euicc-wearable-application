package com.truphone.lpa.dto.asn1.pkix1implicit88;

import com.truphone.lpa.dto.asn1.pkix1explicit88.CertificateSerialNumber;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;

















public class AuthorityKeyIdentifier
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private KeyIdentifier keyIdentifier = null;
  private GeneralNames authorityCertIssuer = null;
  private CertificateSerialNumber authorityCertSerialNumber = null;
  
  public AuthorityKeyIdentifier() {}
  
  public AuthorityKeyIdentifier(byte[] code)
  {
    this.code = code;
  }
  
  public void setKeyIdentifier(KeyIdentifier keyIdentifier) {
    this.keyIdentifier = keyIdentifier;
  }
  
  public KeyIdentifier getKeyIdentifier() {
    return keyIdentifier;
  }
  
  public void setAuthorityCertIssuer(GeneralNames authorityCertIssuer) {
    this.authorityCertIssuer = authorityCertIssuer;
  }
  
  public GeneralNames getAuthorityCertIssuer() {
    return authorityCertIssuer;
  }
  
  public void setAuthorityCertSerialNumber(CertificateSerialNumber authorityCertSerialNumber) {
    this.authorityCertSerialNumber = authorityCertSerialNumber;
  }
  
  public CertificateSerialNumber getAuthorityCertSerialNumber() {
    return authorityCertSerialNumber;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (authorityCertSerialNumber != null) {
      codeLength += authorityCertSerialNumber.encode(os, false);
      
      os.write(130);
      codeLength++;
    }
    
    if (authorityCertIssuer != null) {
      codeLength += authorityCertIssuer.encode(os, false);
      
      os.write(161);
      codeLength++;
    }
    
    if (keyIdentifier != null) {
      codeLength += keyIdentifier.encode(os, false);
      
      os.write(128);
      codeLength++;
    }
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    if (totalLength == 0) {
      return codeLength;
    }
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 0)) {
      keyIdentifier = new KeyIdentifier();
      subCodeLength += keyIdentifier.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 32, 1)) {
      authorityCertIssuer = new GeneralNames();
      subCodeLength += authorityCertIssuer.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 2)) {
      authorityCertSerialNumber = new CertificateSerialNumber();
      subCodeLength += authorityCertSerialNumber.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    boolean firstSelectedElement = true;
    if (keyIdentifier != null) {
      sb.append("\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("keyIdentifier: ").append(keyIdentifier);
      firstSelectedElement = false;
    }
    
    if (authorityCertIssuer != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("authorityCertIssuer: ");
      authorityCertIssuer.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }
    
    if (authorityCertSerialNumber != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("authorityCertSerialNumber: ").append(authorityCertSerialNumber);
      firstSelectedElement = false;
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
