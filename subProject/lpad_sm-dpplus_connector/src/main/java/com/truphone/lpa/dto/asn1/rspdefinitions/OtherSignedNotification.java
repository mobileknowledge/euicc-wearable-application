package com.truphone.lpa.dto.asn1.rspdefinitions;

import com.truphone.lpa.dto.asn1.pkix1explicit88.Certificate;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;














public class OtherSignedNotification
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private NotificationMetadata tbsOtherNotification = null;
  private BerOctetString euiccNotificationSignature = null;
  private Certificate euiccCertificate = null;
  private Certificate eumCertificate = null;
  
  public OtherSignedNotification() {}
  
  public OtherSignedNotification(byte[] code)
  {
    this.code = code;
  }
  
  public void setTbsOtherNotification(NotificationMetadata tbsOtherNotification) {
    this.tbsOtherNotification = tbsOtherNotification;
  }
  
  public NotificationMetadata getTbsOtherNotification() {
    return tbsOtherNotification;
  }
  
  public void setEuiccNotificationSignature(BerOctetString euiccNotificationSignature) {
    this.euiccNotificationSignature = euiccNotificationSignature;
  }
  
  public BerOctetString getEuiccNotificationSignature() {
    return euiccNotificationSignature;
  }
  
  public void setEuiccCertificate(Certificate euiccCertificate) {
    this.euiccCertificate = euiccCertificate;
  }
  
  public Certificate getEuiccCertificate() {
    return euiccCertificate;
  }
  
  public void setEumCertificate(Certificate eumCertificate) {
    this.eumCertificate = eumCertificate;
  }
  
  public Certificate getEumCertificate() {
    return eumCertificate;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += eumCertificate.encode(os, true);
    
    codeLength += euiccCertificate.encode(os, true);
    
    codeLength += euiccNotificationSignature.encode(os, false);
    
    os.write(55);
    os.write(95);
    codeLength += 2;
    
    codeLength += tbsOtherNotification.encode(os, true);
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(NotificationMetadata.tag)) {
      tbsOtherNotification = new NotificationMetadata();
      subCodeLength += tbsOtherNotification.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(64, 0, 55)) {
      euiccNotificationSignature = new BerOctetString();
      subCodeLength += euiccNotificationSignature.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(Certificate.tag)) {
      euiccCertificate = new Certificate();
      subCodeLength += euiccCertificate.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(Certificate.tag)) {
      eumCertificate = new Certificate();
      subCodeLength += eumCertificate.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (tbsOtherNotification != null) {
      sb.append("tbsOtherNotification: ");
      tbsOtherNotification.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("tbsOtherNotification: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccNotificationSignature != null) {
      sb.append("euiccNotificationSignature: ").append(euiccNotificationSignature);
    }
    else {
      sb.append("euiccNotificationSignature: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccCertificate != null) {
      sb.append("euiccCertificate: ");
      euiccCertificate.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("euiccCertificate: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (eumCertificate != null) {
      sb.append("eumCertificate: ");
      eumCertificate.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("eumCertificate: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
