package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;















public class TerminalType
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public TerminalType() {}
  
  public TerminalType(byte[] code)
  {
    super(code);
  }
  
  public TerminalType(BigInteger value) {
    super(value);
  }
  
  public TerminalType(long value) {
    super(value);
  }
}
