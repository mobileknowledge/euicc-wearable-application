package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerBoolean;













public class EnableProfileRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public EnableProfileRequest() {}
  
  public static class ProfileIdentifier
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    public byte[] code = null;
    private OctetTo16 isdpAid = null;
    private Iccid iccid = null;
    
    public ProfileIdentifier() {}
    
    public ProfileIdentifier(byte[] code)
    {
      this.code = code;
    }
    
    public void setIsdpAid(OctetTo16 isdpAid) {
      this.isdpAid = isdpAid;
    }
    
    public OctetTo16 getIsdpAid() {
      return isdpAid;
    }
    
    public void setIccid(Iccid iccid) {
      this.iccid = iccid;
    }
    
    public Iccid getIccid() {
      return iccid;
    }
    
    public int encode(BerByteArrayOutputStream os) throws IOException
    {
      if (code != null) {
        for (int i = code.length - 1; i >= 0; i--) {
          os.write(code[i]);
        }
        return code.length;
      }
      
      int codeLength = 0;
      if (iccid != null) {
        codeLength += iccid.encode(os, true);
        return codeLength;
      }
      
      if (isdpAid != null) {
        codeLength += isdpAid.encode(os, false);
        
        os.write(79);
        codeLength++;
        return codeLength;
      }
      
      throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
    }
    
    public int decode(InputStream is) throws IOException {
      return decode(is, null);
    }
    
    public int decode(InputStream is, BerTag berTag) throws IOException
    {
      int codeLength = 0;
      BerTag passedTag = berTag;
      
      if (berTag == null) {
        berTag = new BerTag();
        codeLength += berTag.decode(is);
      }
      
      if (berTag.equals(64, 0, 15)) {
        isdpAid = new OctetTo16();
        codeLength += isdpAid.decode(is, false);
        return codeLength;
      }
      
      if (berTag.equals(Iccid.tag)) {
        iccid = new Iccid();
        codeLength += iccid.decode(is, false);
        return codeLength;
      }
      
      if (passedTag != null) {
        return 0;
      }
      
      throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
    }
    
    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      encode(os);
      code = os.getArray();
    }
    
    public String toString() {
      StringBuilder sb = new StringBuilder();
      appendAsString(sb, 0);
      return sb.toString();
    }
    
    public void appendAsString(StringBuilder sb, int indentLevel)
    {
      if (isdpAid != null) {
        sb.append("isdpAid: ").append(isdpAid);
        return;
      }
      
      if (iccid != null) {
        sb.append("iccid: ").append(iccid);
        return;
      }
      
      sb.append("<none>");
    }
  }
  

  public static final BerTag tag = new BerTag(128, 32, 49);
  
  public byte[] code = null;
  private ProfileIdentifier profileIdentifier = null;
  private BerBoolean refreshFlag = null;
  


  public EnableProfileRequest(byte[] code)
  {
    this.code = code;
  }
  
  public void setProfileIdentifier(ProfileIdentifier profileIdentifier) {
    this.profileIdentifier = profileIdentifier;
  }
  
  public ProfileIdentifier getProfileIdentifier() {
    return profileIdentifier;
  }
  
  public void setRefreshFlag(BerBoolean refreshFlag) {
    this.refreshFlag = refreshFlag;
  }
  
  public BerBoolean getRefreshFlag() {
    return refreshFlag;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    

    codeLength += refreshFlag.encode(os, false);
    
    os.write(129);
    codeLength++;
    
    int sublength = profileIdentifier.encode(os);
    codeLength += sublength;
    codeLength += BerLength.encodeLength(os, sublength);
    
    os.write(160);
    codeLength++;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 32, 0)) {
      subCodeLength += length.decode(is);
      profileIdentifier = new ProfileIdentifier();
      subCodeLength += profileIdentifier.decode(is, null);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 1)) {
      refreshFlag = new BerBoolean();
      subCodeLength += refreshFlag.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (profileIdentifier != null) {
      sb.append("profileIdentifier: ");
      profileIdentifier.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("profileIdentifier: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (refreshFlag != null) {
      sb.append("refreshFlag: ").append(refreshFlag);
    }
    else {
      sb.append("refreshFlag: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
