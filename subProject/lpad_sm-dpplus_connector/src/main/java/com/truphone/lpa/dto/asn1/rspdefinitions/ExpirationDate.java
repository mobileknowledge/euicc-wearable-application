package com.truphone.lpa.dto.asn1.rspdefinitions;

import com.truphone.lpa.dto.asn1.pkix1explicit88.Time;




















public class ExpirationDate
  extends Time
{
  private static final long serialVersionUID = 1L;
  
  public ExpirationDate() {}
  
  public ExpirationDate(byte[] code)
  {
    super(code);
  }
}
