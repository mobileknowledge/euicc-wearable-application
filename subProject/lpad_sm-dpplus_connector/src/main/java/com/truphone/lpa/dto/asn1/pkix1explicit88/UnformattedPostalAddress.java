//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.string.BerPrintableString;
import org.openmuc.jasn1.ber.types.string.BerTeletexString;

public class UnformattedPostalAddress implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 17);
  public byte[] code = null;
  private UnformattedPostalAddress.PrintableAddress printableAddress = null;
  private BerTeletexString teletexString = null;

  public UnformattedPostalAddress() {
  }

  public UnformattedPostalAddress(byte[] code) {
    this.code = code;
  }

  public void setPrintableAddress(UnformattedPostalAddress.PrintableAddress printableAddress) {
    this.printableAddress = printableAddress;
  }

  public UnformattedPostalAddress.PrintableAddress getPrintableAddress() {
    return this.printableAddress;
  }

  public void setTeletexString(BerTeletexString teletexString) {
    this.teletexString = teletexString;
  }

  public BerTeletexString getTeletexString() {
    return this.teletexString;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int codeLength;
    if (this.code == null) {
      codeLength = 0;
      if (this.teletexString != null) {
        codeLength += this.teletexString.encode(os, true);
      }

      if (this.printableAddress != null) {
        codeLength += this.printableAddress.encode(os, true);
      }

      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }

      return codeLength;
    } else {
      for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
        os.write(this.code[codeLength]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;

    while(subCodeLength < totalLength) {
      subCodeLength += berTag.decode(is);
      if (berTag.equals(UnformattedPostalAddress.PrintableAddress.tag)) {
        this.printableAddress = new UnformattedPostalAddress.PrintableAddress();
        subCodeLength += this.printableAddress.decode(is, false);
      } else if (berTag.equals(BerTeletexString.tag)) {
        this.teletexString = new BerTeletexString();
        subCodeLength += this.teletexString.decode(is, false);
      }
    }

    if (subCodeLength != totalLength) {
      throw new IOException("Length of set does not match length tag, length tag: " + totalLength + ", actual set length: " + subCodeLength);
    } else {
      codeLength += subCodeLength;
      return codeLength;
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    boolean firstSelectedElement = true;
    int i;
    if (this.printableAddress != null) {
      sb.append("\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("printableAddress: ");
      this.printableAddress.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }

    if (this.teletexString != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("teletexString: ").append(this.teletexString);
      firstSelectedElement = false;
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }

  public static class PrintableAddress implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final BerTag tag = new BerTag(0, 32, 16);
    public byte[] code = null;
    private List<BerPrintableString> seqOf = null;

    public PrintableAddress() {
      this.seqOf = new ArrayList();
    }

    public PrintableAddress(byte[] code) {
      this.code = code;
    }

    public List<BerPrintableString> getBerPrintableString() {
      if (this.seqOf == null) {
        this.seqOf = new ArrayList();
      }

      return this.seqOf;
    }

    public int encode(BerByteArrayOutputStream os) throws IOException {
      return this.encode(os, true);
    }

    public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
      int codeLength;
      if (this.code != null) {
        for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
          os.write(this.code[codeLength]);
        }

        return withTag ? tag.encode(os) + this.code.length : this.code.length;
      } else {
        codeLength = 0;

        for(int i = this.seqOf.size() - 1; i >= 0; --i) {
          codeLength += this.seqOf.get(i).encode(os, true);
        }

        codeLength += BerLength.encodeLength(os, codeLength);
        if (withTag) {
          codeLength += tag.encode(os);
        }

        return codeLength;
      }
    }

    public int decode(InputStream is) throws IOException {
      return this.decode(is, true);
    }

    public int decode(InputStream is, boolean withTag) throws IOException {
      int codeLength = 0;
      int subCodeLength = 0;
      if (withTag) {
        codeLength += tag.decodeAndCheck(is);
      }

      BerLength length = new BerLength();
      codeLength += length.decode(is);
      int totalLength = length.val;

      while(subCodeLength < totalLength) {
        BerPrintableString element = new BerPrintableString();
        subCodeLength += element.decode(is, true);
        this.seqOf.add(element);
      }

      if (subCodeLength != totalLength) {
        throw new IOException("Decoded SequenceOf or SetOf has wrong length. Expected " + totalLength + " but has " + subCodeLength);
      } else {
        codeLength += subCodeLength;
        return codeLength;
      }
    }

    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      this.encode(os, false);
      this.code = os.getArray();
    }

    public String toString() {
      StringBuilder sb = new StringBuilder();
      this.appendAsString(sb, 0);
      return sb.toString();
    }

    public void appendAsString(StringBuilder sb, int indentLevel) {
      sb.append("{\n");

      int i;
      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      if (this.seqOf == null) {
        sb.append("null");
      } else {
        Iterator<BerPrintableString> it = this.seqOf.iterator();
        if (it.hasNext()) {
          sb.append(it.next());

          while(it.hasNext()) {
            sb.append(",\n");

            for( i = 0; i < indentLevel + 1; ++i) {
              sb.append("\t");
            }

            sb.append(it.next());
          }
        }
      }

      sb.append("\n");

      for(i = 0; i < indentLevel; ++i) {
        sb.append("\t");
      }

      sb.append("}");
    }
  }
}
