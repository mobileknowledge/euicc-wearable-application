package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class BppCommandId
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public BppCommandId() {}
  
  public BppCommandId(byte[] code)
  {
    super(code);
  }
  
  public BppCommandId(BigInteger value) {
    super(value);
  }
  
  public BppCommandId(long value) {
    super(value);
  }
}
