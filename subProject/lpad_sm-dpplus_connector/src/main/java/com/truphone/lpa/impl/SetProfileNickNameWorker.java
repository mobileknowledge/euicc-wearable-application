package com.truphone.lpa.impl;

import com.truphone.lpa.ApduChannel;
import com.truphone.lpa.apdu.ApduUtils;
import com.truphone.lpa.dto.asn1.rspdefinitions.SetNicknameResponse;
import com.truphone.lpa.progress.Progress;
import com.truphone.lpa.progress.ProgressStep;
import com.truphone.util.LogStub;

import android.org.apache.commons.codec.DecoderException;
import android.org.apache.commons.codec.binary.Hex;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SetProfileNickNameWorker {

    private static final Logger LOG = Logger.getLogger(SetProfileNickNameWorker.class.getName());

    //private final Progress progress;
    private final ApduChannel apduChannel;
    private final String iccid;
    private final String profileNickname;

    SetProfileNickNameWorker(String iccid, String profileNickname, ApduChannel apduChannel) {

        //this.progress = progress;
        this.apduChannel = apduChannel;
        this.iccid = iccid;
        this.profileNickname = profileNickname;
    }

    String run() {

        //progress.setTotalSteps(3);
       // progress.stepExecuted(ProgressStep.SET_NICKNAME_RETRIEVING, "setNickname retrieving...");

        //if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Setting NickName");
        //}

        String setNicknameApdu = ApduUtils.setNicknameApdu(iccid, profileNickname);

        //if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - setNickname APDU: " + setNicknameApdu);
        //}

        String setNicknameApduResponseStr = apduChannel.transmitAPDU(setNicknameApdu);

        //if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Response: " + setNicknameApduResponseStr);
        //}

        return convertSetNickNameRspData(setNicknameApduResponseStr);
    }

    private String convertSetNickNameRspData(String eidapduResponseStr) {

        //progress.stepExecuted(ProgressStep.SET_NICKNAME_CONVERTING, "setNickname converting...");

        SetNicknameResponse setNicknameResponse = new SetNicknameResponse();

        try {
          //  if (LogStub.getInstance().isDebugEnabled()) {
                LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Decoding response: " + eidapduResponseStr);
            //}

            InputStream is = new ByteArrayInputStream(Hex.decodeHex(eidapduResponseStr.toCharArray()));

            //if (LogStub.getInstance().isDebugEnabled()) {
                LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Decoding with GetEuiccDataResponse");
            //}

            setNicknameResponse.decode(is, true);

            //if (LogStub.getInstance().isDebugEnabled()) {
                LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - EID is: " + setNicknameResponse.getSetNicknameResult().toString());
            //}

            //progress.stepExecuted(ProgressStep.GET_EID_CONVERTED, "getEID converted...");

            return setNicknameResponse.getSetNicknameResult().toString();
        } catch (DecoderException e) {
            LOG.log(Level.SEVERE, LogStub.getInstance().getTag() + " - " + e.getMessage(), e);
            LOG.log(Level.SEVERE, LogStub.getInstance().getTag() + " -  Unable to retrieve EID. Exception in Decoder:" + e.getMessage());

            throw new RuntimeException("Unable to retrieve EID");
        } catch (IOException ioe) {
            LOG.log(Level.SEVERE, LogStub.getInstance().getTag() + " - " + ioe.getMessage(), ioe);

            throw new RuntimeException("Invalid EID response, unable to retrieve EID");
        }
    }
}
