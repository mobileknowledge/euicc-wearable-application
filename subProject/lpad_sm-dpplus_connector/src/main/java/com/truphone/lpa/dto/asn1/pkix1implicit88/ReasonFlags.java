package com.truphone.lpa.dto.asn1.pkix1implicit88;

import org.openmuc.jasn1.ber.types.BerBitString;






















public class ReasonFlags
  extends BerBitString
{
  private static final long serialVersionUID = 1L;
  
  public ReasonFlags() {}
  
  public ReasonFlags(byte[] code)
  {
    super(code);
  }
  
  public ReasonFlags(byte[] value, int numBits) {
    super(value, numBits);
  }
}
