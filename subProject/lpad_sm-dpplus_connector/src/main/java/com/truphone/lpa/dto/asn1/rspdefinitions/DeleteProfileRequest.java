package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
















public class DeleteProfileRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  public static final BerTag tag = new BerTag(128, 32, 51);
  
  private OctetTo16 isdpAid = null;
  private Iccid iccid = null;
  
  public DeleteProfileRequest() {}
  
  public DeleteProfileRequest(byte[] code)
  {
    this.code = code;
  }
  
  public void setIsdpAid(OctetTo16 isdpAid) {
    this.isdpAid = isdpAid;
  }
  
  public OctetTo16 getIsdpAid() {
    return isdpAid;
  }
  
  public void setIccid(Iccid iccid) {
    this.iccid = iccid;
  }
  
  public Iccid getIccid() {
    return iccid;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (iccid != null) {
      codeLength += iccid.encode(os, true);
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (isdpAid != null) {
      codeLength += isdpAid.encode(os, false);
      
      os.write(79);
      codeLength++;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    BerLength length = new BerLength();
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    codeLength += length.decode(is);
    codeLength += berTag.decode(is);
    
    if (berTag.equals(64, 0, 15)) {
      isdpAid = new OctetTo16();
      codeLength += isdpAid.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(Iccid.tag)) {
      iccid = new Iccid();
      codeLength += iccid.decode(is, false);
      return codeLength;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (isdpAid != null) {
      sb.append("isdpAid: ").append(isdpAid);
      return;
    }
    
    if (iccid != null) {
      sb.append("iccid: ").append(iccid);
      return;
    }
    
    sb.append("<none>");
  }
}
