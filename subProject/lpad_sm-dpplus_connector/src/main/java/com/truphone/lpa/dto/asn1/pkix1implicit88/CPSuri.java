package com.truphone.lpa.dto.asn1.pkix1implicit88;

import org.openmuc.jasn1.ber.types.string.BerIA5String;






















public class CPSuri
  extends BerIA5String
{
  private static final long serialVersionUID = 1L;
  
  public CPSuri() {}
  
  public CPSuri(byte[] value)
  {
    super(value);
  }
}
