package com.truphone.lpa.dto.asn1.pkix1explicit88;

import org.openmuc.jasn1.ber.types.string.BerPrintableString;
















public class X520SerialNumber
  extends BerPrintableString
{
  private static final long serialVersionUID = 1L;
  
  public X520SerialNumber() {}
  
  public X520SerialNumber(byte[] value)
  {
    super(value);
  }
}
