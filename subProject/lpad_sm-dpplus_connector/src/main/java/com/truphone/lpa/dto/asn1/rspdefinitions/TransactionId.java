package com.truphone.lpa.dto.asn1.rspdefinitions;

import org.openmuc.jasn1.ber.types.BerOctetString;




















public class TransactionId
  extends BerOctetString
{
  private static final long serialVersionUID = 1L;
  
  public TransactionId() {}
  
  public TransactionId(byte[] value)
  {
    super(value);
  }
}
