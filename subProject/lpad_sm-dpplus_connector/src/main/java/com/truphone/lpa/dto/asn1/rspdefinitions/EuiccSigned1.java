package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.string.BerUTF8String;















public class EuiccSigned1
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private TransactionId transactionId = null;
  private BerUTF8String serverAddress = null;
  private Octet16 serverChallenge = null;
  private EUICCInfo2 euiccInfo2 = null;
  private CtxParams1 ctxParams1 = null;
  
  public EuiccSigned1() {}
  
  public EuiccSigned1(byte[] code)
  {
    this.code = code;
  }
  
  public void setTransactionId(TransactionId transactionId) {
    this.transactionId = transactionId;
  }
  
  public TransactionId getTransactionId() {
    return transactionId;
  }
  
  public void setServerAddress(BerUTF8String serverAddress) {
    this.serverAddress = serverAddress;
  }
  
  public BerUTF8String getServerAddress() {
    return serverAddress;
  }
  
  public void setServerChallenge(Octet16 serverChallenge) {
    this.serverChallenge = serverChallenge;
  }
  
  public Octet16 getServerChallenge() {
    return serverChallenge;
  }
  
  public void setEuiccInfo2(EUICCInfo2 euiccInfo2) {
    this.euiccInfo2 = euiccInfo2;
  }
  
  public EUICCInfo2 getEuiccInfo2() {
    return euiccInfo2;
  }
  
  public void setCtxParams1(CtxParams1 ctxParams1) {
    this.ctxParams1 = ctxParams1;
  }
  
  public CtxParams1 getCtxParams1() {
    return ctxParams1;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += ctxParams1.encode(os);
    
    codeLength += euiccInfo2.encode(os, false);
    
    os.write(34);
    os.write(191);
    codeLength += 2;
    
    codeLength += serverChallenge.encode(os, false);
    
    os.write(132);
    codeLength++;
    
    codeLength += serverAddress.encode(os, false);
    
    os.write(131);
    codeLength++;
    
    codeLength += transactionId.encode(os, false);
    
    os.write(128);
    codeLength++;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 0)) {
      transactionId = new TransactionId();
      subCodeLength += transactionId.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 3)) {
      serverAddress = new BerUTF8String();
      subCodeLength += serverAddress.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 4)) {
      serverChallenge = new Octet16();
      subCodeLength += serverChallenge.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 32, 34)) {
      euiccInfo2 = new EUICCInfo2();
      subCodeLength += euiccInfo2.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    ctxParams1 = new CtxParams1();
    subCodeLength += ctxParams1.decode(is, berTag);
    if (subCodeLength == totalLength) {
      return codeLength;
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (transactionId != null) {
      sb.append("transactionId: ").append(transactionId);
    }
    else {
      sb.append("transactionId: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (serverAddress != null) {
      sb.append("serverAddress: ").append(serverAddress);
    }
    else {
      sb.append("serverAddress: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (serverChallenge != null) {
      sb.append("serverChallenge: ").append(serverChallenge);
    }
    else {
      sb.append("serverChallenge: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccInfo2 != null) {
      sb.append("euiccInfo2: ");
      euiccInfo2.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("euiccInfo2: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (ctxParams1 != null) {
      sb.append("ctxParams1: ");
      ctxParams1.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("ctxParams1: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
