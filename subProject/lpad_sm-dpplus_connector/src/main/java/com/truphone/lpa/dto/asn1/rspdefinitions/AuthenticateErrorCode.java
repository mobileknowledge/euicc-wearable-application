package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class AuthenticateErrorCode
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public AuthenticateErrorCode() {}
  
  public AuthenticateErrorCode(byte[] code)
  {
    super(code);
  }
  
  public AuthenticateErrorCode(BigInteger value) {
    super(value);
  }
  
  public AuthenticateErrorCode(long value) {
    super(value);
  }
}
