package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;

















public class Iccid
  extends BerOctetString
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(64, 0, 26);
  
  public Iccid() {}
  
  public Iccid(byte[] value)
  {
    super(value);
  }
  

  public int encode(BerByteArrayOutputStream os, boolean withTag)
    throws IOException
  {
    int codeLength = super.encode(os, false);
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException
  {
    int codeLength = 0;
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    codeLength += super.decode(is, false);
    
    return codeLength;
  }
}
