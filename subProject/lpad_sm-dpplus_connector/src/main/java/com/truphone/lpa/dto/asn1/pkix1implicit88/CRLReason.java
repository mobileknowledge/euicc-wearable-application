package com.truphone.lpa.dto.asn1.pkix1implicit88;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerEnum;





















public class CRLReason
  extends BerEnum
{
  private static final long serialVersionUID = 1L;
  
  public CRLReason() {}
  
  public CRLReason(byte[] code)
  {
    super(code);
  }
  
  public CRLReason(BigInteger value) {
    super(value);
  }
  
  public CRLReason(long value) {
    super(value);
  }
}
