package com.truphone.lpa.dto.asn1.rspdefinitions;

import com.truphone.lpa.dto.asn1.pkix1explicit88.Certificate;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;














public class AuthenticateResponseOk
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private EuiccSigned1 euiccSigned1 = null;
  private BerOctetString euiccSignature1 = null;
  private Certificate euiccCertificate = null;
  private Certificate eumCertificate = null;
  
  public AuthenticateResponseOk() {}
  
  public AuthenticateResponseOk(byte[] code)
  {
    this.code = code;
  }
  
  public void setEuiccSigned1(EuiccSigned1 euiccSigned1) {
    this.euiccSigned1 = euiccSigned1;
  }
  
  public EuiccSigned1 getEuiccSigned1() {
    return euiccSigned1;
  }
  
  public void setEuiccSignature1(BerOctetString euiccSignature1) {
    this.euiccSignature1 = euiccSignature1;
  }
  
  public BerOctetString getEuiccSignature1() {
    return euiccSignature1;
  }
  
  public void setEuiccCertificate(Certificate euiccCertificate) {
    this.euiccCertificate = euiccCertificate;
  }
  
  public Certificate getEuiccCertificate() {
    return euiccCertificate;
  }
  
  public void setEumCertificate(Certificate eumCertificate) {
    this.eumCertificate = eumCertificate;
  }
  
  public Certificate getEumCertificate() {
    return eumCertificate;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += eumCertificate.encode(os, true);
    
    codeLength += euiccCertificate.encode(os, true);
    
    codeLength += euiccSignature1.encode(os, false);
    
    os.write(55);
    os.write(95);
    codeLength += 2;
    
    codeLength += euiccSigned1.encode(os, true);
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(EuiccSigned1.tag)) {
      euiccSigned1 = new EuiccSigned1();
      subCodeLength += euiccSigned1.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(64, 0, 55)) {
      euiccSignature1 = new BerOctetString();
      subCodeLength += euiccSignature1.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(Certificate.tag)) {
      euiccCertificate = new Certificate();
      subCodeLength += euiccCertificate.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(Certificate.tag)) {
      eumCertificate = new Certificate();
      subCodeLength += eumCertificate.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccSigned1 != null) {
      sb.append("euiccSigned1: ");
      euiccSigned1.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("euiccSigned1: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccSignature1 != null) {
      sb.append("euiccSignature1: ").append(euiccSignature1);
    }
    else {
      sb.append("euiccSignature1: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccCertificate != null) {
      sb.append("euiccCertificate: ");
      euiccCertificate.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("euiccCertificate: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (eumCertificate != null) {
      sb.append("eumCertificate: ");
      eumCertificate.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("eumCertificate: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
