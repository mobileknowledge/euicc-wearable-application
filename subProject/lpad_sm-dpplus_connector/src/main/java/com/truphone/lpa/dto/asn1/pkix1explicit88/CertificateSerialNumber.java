package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;















public class CertificateSerialNumber
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public CertificateSerialNumber() {}
  
  public CertificateSerialNumber(byte[] code)
  {
    super(code);
  }
  
  public CertificateSerialNumber(BigInteger value) {
    super(value);
  }
  
  public CertificateSerialNumber(long value) {
    super(value);
  }
}
