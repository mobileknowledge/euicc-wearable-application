package com.truphone.lpa.dto.asn1.pkix1explicit88;

import org.openmuc.jasn1.ber.types.BerBitString;
















public class UniqueIdentifier
  extends BerBitString
{
  private static final long serialVersionUID = 1L;
  
  public UniqueIdentifier() {}
  
  public UniqueIdentifier(byte[] code)
  {
    super(code);
  }
  
  public UniqueIdentifier(byte[] value, int numBits) {
    super(value, numBits);
  }
}
