package com.truphone.lpa.dto.asn1.pkix1implicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;


















public class NameConstraints
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private GeneralSubtrees permittedSubtrees = null;
  private GeneralSubtrees excludedSubtrees = null;
  
  public NameConstraints() {}
  
  public NameConstraints(byte[] code)
  {
    this.code = code;
  }
  
  public void setPermittedSubtrees(GeneralSubtrees permittedSubtrees) {
    this.permittedSubtrees = permittedSubtrees;
  }
  
  public GeneralSubtrees getPermittedSubtrees() {
    return permittedSubtrees;
  }
  
  public void setExcludedSubtrees(GeneralSubtrees excludedSubtrees) {
    this.excludedSubtrees = excludedSubtrees;
  }
  
  public GeneralSubtrees getExcludedSubtrees() {
    return excludedSubtrees;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (excludedSubtrees != null) {
      codeLength += excludedSubtrees.encode(os, false);
      
      os.write(161);
      codeLength++;
    }
    
    if (permittedSubtrees != null) {
      codeLength += permittedSubtrees.encode(os, false);
      
      os.write(160);
      codeLength++;
    }
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    if (totalLength == 0) {
      return codeLength;
    }
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 32, 0)) {
      permittedSubtrees = new GeneralSubtrees();
      subCodeLength += permittedSubtrees.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 32, 1)) {
      excludedSubtrees = new GeneralSubtrees();
      subCodeLength += excludedSubtrees.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    boolean firstSelectedElement = true;
    if (permittedSubtrees != null) {
      sb.append("\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("permittedSubtrees: ");
      permittedSubtrees.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }
    
    if (excludedSubtrees != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("excludedSubtrees: ");
      excludedSubtrees.appendAsString(sb, indentLevel + 1);
      firstSelectedElement = false;
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
