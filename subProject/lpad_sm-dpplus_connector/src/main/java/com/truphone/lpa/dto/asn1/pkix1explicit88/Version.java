package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;















public class Version
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public Version() {}
  
  public Version(byte[] code)
  {
    super(code);
  }
  
  public Version(BigInteger value) {
    super(value);
  }
  
  public Version(long value) {
    super(value);
  }
}
