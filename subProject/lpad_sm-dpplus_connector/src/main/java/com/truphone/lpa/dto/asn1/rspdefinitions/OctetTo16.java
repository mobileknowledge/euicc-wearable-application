package com.truphone.lpa.dto.asn1.rspdefinitions;

import org.openmuc.jasn1.ber.types.BerOctetString;




















public class OctetTo16
  extends BerOctetString
{
  private static final long serialVersionUID = 1L;
  
  public OctetTo16() {}
  
  public OctetTo16(byte[] value)
  {
    super(value);
  }
}
