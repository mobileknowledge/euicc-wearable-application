package com.truphone.lpa.dto.asn1.pkix1implicit88;

import org.openmuc.jasn1.ber.types.BerBitString;






















public class KeyUsage
  extends BerBitString
{
  private static final long serialVersionUID = 1L;
  
  public KeyUsage() {}
  
  public KeyUsage(byte[] code)
  {
    super(code);
  }
  
  public KeyUsage(byte[] value, int numBits) {
    super(value, numBits);
  }
}
