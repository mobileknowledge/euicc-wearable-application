package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerInteger;
















public class RemoteOpId
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(128, 0, 2);
  
  public RemoteOpId() {}
  
  public RemoteOpId(byte[] code)
  {
    super(code);
  }
  
  public RemoteOpId(BigInteger value) {
    super(value);
  }
  
  public RemoteOpId(long value) {
    super(value);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    


    int codeLength = super.encode(os, false);
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException
  {
    int codeLength = 0;
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    codeLength += super.decode(is, false);
    
    return codeLength;
  }
}
