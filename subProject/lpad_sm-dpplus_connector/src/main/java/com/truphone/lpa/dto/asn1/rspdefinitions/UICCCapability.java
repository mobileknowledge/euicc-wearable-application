package com.truphone.lpa.dto.asn1.rspdefinitions;

import org.openmuc.jasn1.ber.types.BerBitString;




















public class UICCCapability
  extends BerBitString
{
  private static final long serialVersionUID = 1L;
  
  public UICCCapability() {}
  
  public UICCCapability(byte[] code)
  {
    super(code);
  }
  
  public UICCCapability(byte[] value, int numBits) {
    super(value, numBits);
  }
}
