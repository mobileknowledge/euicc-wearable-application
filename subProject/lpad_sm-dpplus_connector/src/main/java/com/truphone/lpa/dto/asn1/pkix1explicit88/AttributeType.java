package com.truphone.lpa.dto.asn1.pkix1explicit88;

import org.openmuc.jasn1.ber.types.BerObjectIdentifier;
















public class AttributeType
  extends BerObjectIdentifier
{
  private static final long serialVersionUID = 1L;
  
  public AttributeType() {}
  
  public AttributeType(byte[] code)
  {
    super(code);
  }
  
  public AttributeType(int[] value) {
    super(value);
  }
}
