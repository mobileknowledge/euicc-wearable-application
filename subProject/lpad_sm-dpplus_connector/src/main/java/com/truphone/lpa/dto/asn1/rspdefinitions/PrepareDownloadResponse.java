package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
















public class PrepareDownloadResponse
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  public static final BerTag tag = new BerTag(128, 32, 33);
  
  private PrepareDownloadResponseOk downloadResponseOk = null;
  private PrepareDownloadResponseError downloadResponseError = null;
  
  public PrepareDownloadResponse() {}
  
  public PrepareDownloadResponse(byte[] code)
  {
    this.code = code;
  }
  
  public void setDownloadResponseOk(PrepareDownloadResponseOk downloadResponseOk) {
    this.downloadResponseOk = downloadResponseOk;
  }
  
  public PrepareDownloadResponseOk getDownloadResponseOk() {
    return downloadResponseOk;
  }
  
  public void setDownloadResponseError(PrepareDownloadResponseError downloadResponseError) {
    this.downloadResponseError = downloadResponseError;
  }
  
  public PrepareDownloadResponseError getDownloadResponseError() {
    return downloadResponseError;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (downloadResponseError != null) {
      codeLength += downloadResponseError.encode(os, false);
      
      os.write(161);
      codeLength++;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (downloadResponseOk != null) {
      codeLength += downloadResponseOk.encode(os, false);
      
      os.write(160);
      codeLength++;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    BerLength length = new BerLength();
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    codeLength += length.decode(is);
    codeLength += berTag.decode(is);
    
    if (berTag.equals(128, 32, 0)) {
      downloadResponseOk = new PrepareDownloadResponseOk();
      codeLength += downloadResponseOk.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 1)) {
      downloadResponseError = new PrepareDownloadResponseError();
      codeLength += downloadResponseError.decode(is, false);
      return codeLength;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (downloadResponseOk != null) {
      sb.append("downloadResponseOk: ");
      downloadResponseOk.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (downloadResponseError != null) {
      sb.append("downloadResponseError: ");
      downloadResponseError.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    sb.append("<none>");
  }
}
