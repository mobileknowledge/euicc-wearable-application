package com.truphone.lpa.impl;

import android.org.apache.commons.codec.DecoderException;
import android.org.apache.commons.codec.binary.Hex;

import com.truphone.lpa.ApduChannel;
import com.truphone.lpa.apdu.ApduUtils;
import com.truphone.lpa.dto.asn1.rspdefinitions.SetDefaultDpAddressResponse;
import com.truphone.lpa.dto.asn1.rspdefinitions.SetNicknameResponse;
import com.truphone.util.LogStub;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SetSMDPDefaultAddressWorker {

    private static final Logger LOG = Logger.getLogger(SetSMDPDefaultAddressWorker.class.getName());

    //private final Progress progress;
    private final ApduChannel apduChannel;
    private final String address;


    SetSMDPDefaultAddressWorker(String address, ApduChannel apduChannel) {

        //this.progress = progress;
        this.apduChannel = apduChannel;
        this.address = address;

    }

    String run() {

        //progress.setTotalSteps(3);
        // progress.stepExecuted(ProgressStep.SET_NICKNAME_RETRIEVING, "setNickname retrieving...");

        //if (LogStub.getInstance().isDebugEnabled()) {
        LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Setting NickName");
        //}

        String SetSMDPAddressApdu = ApduUtils.setDefaultDpAddressApdu(address);

        //if (LogStub.getInstance().isDebugEnabled()) {
        LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - SetSMDPDefaultAddress APDU: " + SetSMDPAddressApdu);
        //}

        String SetSMDPAddressApduResponseStr = apduChannel.transmitAPDU(SetSMDPAddressApdu);

        //if (LogStub.getInstance().isDebugEnabled()) {
        LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Response: " + SetSMDPAddressApduResponseStr);
        //}

        return setSMDPAddressApduRspData(SetSMDPAddressApduResponseStr);
    }

    private String setSMDPAddressApduRspData(String SetSMDPAddressApduResponseStr) {

        //progress.stepExecuted(ProgressStep.SET_NICKNAME_CONVERTING, "setNickname converting...");

        SetDefaultDpAddressResponse setDPAddressResponse = new SetDefaultDpAddressResponse();

        try {
            //  if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Decoding response: " + SetSMDPAddressApduResponseStr);
            //}

            InputStream is = new ByteArrayInputStream(Hex.decodeHex(SetSMDPAddressApduResponseStr.toCharArray()));

            //if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Decoding with SetDefaultDpAddressResponse");
            //}

            setDPAddressResponse.decode(is, true);

            //if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - EID is: " + setDPAddressResponse.getSetDefaultDpAddressResult().toString());
            //}

            //progress.stepExecuted(ProgressStep.GET_EID_CONVERTED, "getEID converted...");

            return setDPAddressResponse.getSetDefaultDpAddressResult().toString();
        } catch (DecoderException e) {
            LOG.log(Level.SEVERE, LogStub.getInstance().getTag() + " - " + e.getMessage(), e);
            LOG.log(Level.SEVERE, LogStub.getInstance().getTag() + " -  Unable to SetSMDPDefaultAddress. Exception in Decoder:" + e.getMessage());

            throw new RuntimeException("Unable SetSMDPDefaultAddress");
        } catch (IOException ioe) {
            LOG.log(Level.SEVERE, LogStub.getInstance().getTag() + " - " + ioe.getMessage(), ioe);

            throw new RuntimeException("Invalid SetSMDPDefaultAddress response");
        }
    }

}
