package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;















public class ReplaceSessionKeysRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(128, 32, 38);
  
  public byte[] code = null;
  private BerOctetString initialMacChainingValue = null;
  private BerOctetString ppkEnc = null;
  private BerOctetString ppkCmac = null;
  
  public ReplaceSessionKeysRequest() {}
  
  public ReplaceSessionKeysRequest(byte[] code)
  {
    this.code = code;
  }
  
  public void setInitialMacChainingValue(BerOctetString initialMacChainingValue) {
    this.initialMacChainingValue = initialMacChainingValue;
  }
  
  public BerOctetString getInitialMacChainingValue() {
    return initialMacChainingValue;
  }
  
  public void setPpkEnc(BerOctetString ppkEnc) {
    this.ppkEnc = ppkEnc;
  }
  
  public BerOctetString getPpkEnc() {
    return ppkEnc;
  }
  
  public void setPpkCmac(BerOctetString ppkCmac) {
    this.ppkCmac = ppkCmac;
  }
  
  public BerOctetString getPpkCmac() {
    return ppkCmac;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += ppkCmac.encode(os, false);
    
    os.write(130);
    codeLength++;
    
    codeLength += ppkEnc.encode(os, false);
    
    os.write(129);
    codeLength++;
    
    codeLength += initialMacChainingValue.encode(os, false);
    
    os.write(128);
    codeLength++;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 0)) {
      initialMacChainingValue = new BerOctetString();
      subCodeLength += initialMacChainingValue.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 1)) {
      ppkEnc = new BerOctetString();
      subCodeLength += ppkEnc.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 2)) {
      ppkCmac = new BerOctetString();
      subCodeLength += ppkCmac.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (initialMacChainingValue != null) {
      sb.append("initialMacChainingValue: ").append(initialMacChainingValue);
    }
    else {
      sb.append("initialMacChainingValue: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (ppkEnc != null) {
      sb.append("ppkEnc: ").append(ppkEnc);
    }
    else {
      sb.append("ppkEnc: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (ppkCmac != null) {
      sb.append("ppkCmac: ").append(ppkCmac);
    }
    else {
      sb.append("ppkCmac: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
