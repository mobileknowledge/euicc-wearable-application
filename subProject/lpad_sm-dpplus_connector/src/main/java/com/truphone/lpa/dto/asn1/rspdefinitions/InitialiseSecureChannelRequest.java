package com.truphone.lpa.dto.asn1.rspdefinitions;

import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;















public class InitialiseSecureChannelRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(128, 32, 35);
  
  public byte[] code = null;
  private RemoteOpId remoteOpId = null;
  private TransactionId transactionId = null;
  private ControlRefTemplate controlRefTemplate = null;
  private BerOctetString smdpOtpk = null;
  private BerOctetString smdpSign = null;
  
  public InitialiseSecureChannelRequest() {}
  
  public InitialiseSecureChannelRequest(byte[] code)
  {
    this.code = code;
  }
  
  public void setRemoteOpId(RemoteOpId remoteOpId) {
    this.remoteOpId = remoteOpId;
  }
  
  public RemoteOpId getRemoteOpId() {
    return remoteOpId;
  }
  
  public void setTransactionId(TransactionId transactionId) {
    this.transactionId = transactionId;
  }
  
  public TransactionId getTransactionId() {
    return transactionId;
  }
  
  public void setControlRefTemplate(ControlRefTemplate controlRefTemplate) {
    this.controlRefTemplate = controlRefTemplate;
  }
  
  public ControlRefTemplate getControlRefTemplate() {
    return controlRefTemplate;
  }
  
  public void setSmdpOtpk(BerOctetString smdpOtpk) {
    this.smdpOtpk = smdpOtpk;
  }
  
  public BerOctetString getSmdpOtpk() {
    return smdpOtpk;
  }
  
  public void setSmdpSign(BerOctetString smdpSign) {
    this.smdpSign = smdpSign;
  }
  
  public BerOctetString getSmdpSign() {
    return smdpSign;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    codeLength += smdpSign.encode(os, false);
    
    os.write(55);
    os.write(95);
    codeLength += 2;
    
    codeLength += smdpOtpk.encode(os, false);
    
    os.write(73);
    os.write(95);
    codeLength += 2;
    
    codeLength += controlRefTemplate.encode(os, false);
    
    os.write(166);
    codeLength++;
    
    codeLength += transactionId.encode(os, false);
    
    os.write(128);
    codeLength++;
    
    codeLength += remoteOpId.encode(os, true);
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(RemoteOpId.tag)) {
      remoteOpId = new RemoteOpId();
      subCodeLength += remoteOpId.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 0)) {
      transactionId = new TransactionId();
      subCodeLength += transactionId.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 32, 6)) {
      controlRefTemplate = new ControlRefTemplate();
      subCodeLength += controlRefTemplate.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(64, 0, 73)) {
      smdpOtpk = new BerOctetString();
      subCodeLength += smdpOtpk.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(64, 0, 55)) {
      smdpSign = new BerOctetString();
      subCodeLength += smdpSign.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (remoteOpId != null) {
      sb.append("remoteOpId: ").append(remoteOpId);
    }
    else {
      sb.append("remoteOpId: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (transactionId != null) {
      sb.append("transactionId: ").append(transactionId);
    }
    else {
      sb.append("transactionId: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (controlRefTemplate != null) {
      sb.append("controlRefTemplate: ");
      controlRefTemplate.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("controlRefTemplate: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (smdpOtpk != null) {
      sb.append("smdpOtpk: ").append(smdpOtpk);
    }
    else {
      sb.append("smdpOtpk: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (smdpSign != null) {
      sb.append("smdpSign: ").append(smdpSign);
    }
    else {
      sb.append("smdpSign: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
