package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerTag;













public class Name
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  private RDNSequence rdnSequence = null;
  
  public Name() {}
  
  public Name(byte[] code)
  {
    this.code = code;
  }
  
  public void setRdnSequence(RDNSequence rdnSequence) {
    this.rdnSequence = rdnSequence;
  }
  
  public RDNSequence getRdnSequence() {
    return rdnSequence;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (rdnSequence != null) {
      codeLength += rdnSequence.encode(os, true);
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, null);
  }
  
  public int decode(InputStream is, BerTag berTag) throws IOException
  {
    int codeLength = 0;
    BerTag passedTag = berTag;
    
    if (berTag == null) {
      berTag = new BerTag();
      codeLength += berTag.decode(is);
    }
    
    if (berTag.equals(RDNSequence.tag)) {
      rdnSequence = new RDNSequence();
      codeLength += rdnSequence.decode(is, false);
      return codeLength;
    }
    
    if (passedTag != null) {
      return 0;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (rdnSequence != null) {
      sb.append("rdnSequence: ");
      rdnSequence.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    sb.append("<none>");
  }
}
