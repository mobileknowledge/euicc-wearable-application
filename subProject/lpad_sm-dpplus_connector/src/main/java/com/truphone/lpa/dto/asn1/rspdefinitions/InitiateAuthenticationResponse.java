package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerInteger;















public class InitiateAuthenticationResponse
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  public static final BerTag tag = new BerTag(128, 32, 57);
  
  private InitiateAuthenticationOkEs9 initiateAuthenticationOk = null;
  private BerInteger initiateAuthenticationError = null;
  
  public InitiateAuthenticationResponse() {}
  
  public InitiateAuthenticationResponse(byte[] code)
  {
    this.code = code;
  }
  
  public void setInitiateAuthenticationOk(InitiateAuthenticationOkEs9 initiateAuthenticationOk) {
    this.initiateAuthenticationOk = initiateAuthenticationOk;
  }
  
  public InitiateAuthenticationOkEs9 getInitiateAuthenticationOk() {
    return initiateAuthenticationOk;
  }
  
  public void setInitiateAuthenticationError(BerInteger initiateAuthenticationError) {
    this.initiateAuthenticationError = initiateAuthenticationError;
  }
  
  public BerInteger getInitiateAuthenticationError() {
    return initiateAuthenticationError;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (initiateAuthenticationError != null) {
      codeLength += initiateAuthenticationError.encode(os, false);
      
      os.write(129);
      codeLength++;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (initiateAuthenticationOk != null) {
      codeLength += initiateAuthenticationOk.encode(os, false);
      
      os.write(160);
      codeLength++;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    BerLength length = new BerLength();
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    codeLength += length.decode(is);
    codeLength += berTag.decode(is);
    
    if (berTag.equals(128, 32, 0)) {
      initiateAuthenticationOk = new InitiateAuthenticationOkEs9();
      codeLength += initiateAuthenticationOk.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 0, 1)) {
      initiateAuthenticationError = new BerInteger();
      codeLength += initiateAuthenticationError.decode(is, false);
      return codeLength;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (initiateAuthenticationOk != null) {
      sb.append("initiateAuthenticationOk: ");
      initiateAuthenticationOk.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (initiateAuthenticationError != null) {
      sb.append("initiateAuthenticationError: ").append(initiateAuthenticationError);
      return;
    }
    
    sb.append("<none>");
  }
}
