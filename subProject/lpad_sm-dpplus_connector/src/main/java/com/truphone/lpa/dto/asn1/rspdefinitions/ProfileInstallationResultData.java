package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerObjectIdentifier;













public class ProfileInstallationResultData
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public ProfileInstallationResultData() {}
  
  public static class FinalResult
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    public byte[] code = null;
    private SuccessResult successResult = null;
    private ErrorResult errorResult = null;
    
    public FinalResult() {}
    
    public FinalResult(byte[] code)
    {
      this.code = code;
    }
    
    public void setSuccessResult(SuccessResult successResult) {
      this.successResult = successResult;
    }
    
    public SuccessResult getSuccessResult() {
      return successResult;
    }
    
    public void setErrorResult(ErrorResult errorResult) {
      this.errorResult = errorResult;
    }
    
    public ErrorResult getErrorResult() {
      return errorResult;
    }
    
    public int encode(BerByteArrayOutputStream os) throws IOException
    {
      if (code != null) {
        for (int i = code.length - 1; i >= 0; i--) {
          os.write(code[i]);
        }
        return code.length;
      }
      
      int codeLength = 0;
      if (errorResult != null) {
        codeLength += errorResult.encode(os, false);
        
        os.write(161);
        codeLength++;
        return codeLength;
      }
      
      if (successResult != null) {
        codeLength += successResult.encode(os, false);
        
        os.write(160);
        codeLength++;
        return codeLength;
      }
      
      throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
    }
    
    public int decode(InputStream is) throws IOException {
      return decode(is, null);
    }
    
    public int decode(InputStream is, BerTag berTag) throws IOException
    {
      int codeLength = 0;
      BerTag passedTag = berTag;
      
      if (berTag == null) {
        berTag = new BerTag();
        codeLength += berTag.decode(is);
      }
      
      if (berTag.equals(128, 32, 0)) {
        successResult = new SuccessResult();
        codeLength += successResult.decode(is, false);
        return codeLength;
      }
      
      if (berTag.equals(128, 32, 1)) {
        errorResult = new ErrorResult();
        codeLength += errorResult.decode(is, false);
        return codeLength;
      }
      
      if (passedTag != null) {
        return 0;
      }
      
      throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
    }
    
    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      encode(os);
      code = os.getArray();
    }
    
    public String toString() {
      StringBuilder sb = new StringBuilder();
      appendAsString(sb, 0);
      return sb.toString();
    }
    
    public void appendAsString(StringBuilder sb, int indentLevel)
    {
      if (successResult != null) {
        sb.append("successResult: ");
        successResult.appendAsString(sb, indentLevel + 1);
        return;
      }
      
      if (errorResult != null) {
        sb.append("errorResult: ");
        errorResult.appendAsString(sb, indentLevel + 1);
        return;
      }
      
      sb.append("<none>");
    }
  }
  

  public static final BerTag tag = new BerTag(128, 32, 39);
  
  public byte[] code = null;
  private TransactionId transactionId = null;
  private NotificationMetadata notificationMetadata = null;
  private BerObjectIdentifier smdpOid = null;
  private FinalResult finalResult = null;
  


  public ProfileInstallationResultData(byte[] code)
  {
    this.code = code;
  }
  
  public void setTransactionId(TransactionId transactionId) {
    this.transactionId = transactionId;
  }
  
  public TransactionId getTransactionId() {
    return transactionId;
  }
  
  public void setNotificationMetadata(NotificationMetadata notificationMetadata) {
    this.notificationMetadata = notificationMetadata;
  }
  
  public NotificationMetadata getNotificationMetadata() {
    return notificationMetadata;
  }
  
  public void setSmdpOid(BerObjectIdentifier smdpOid) {
    this.smdpOid = smdpOid;
  }
  
  public BerObjectIdentifier getSmdpOid() {
    return smdpOid;
  }
  
  public void setFinalResult(FinalResult finalResult) {
    this.finalResult = finalResult;
  }
  
  public FinalResult getFinalResult() {
    return finalResult;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    

    int sublength = finalResult.encode(os);
    codeLength += sublength;
    codeLength += BerLength.encodeLength(os, sublength);
    
    os.write(162);
    codeLength++;
    
    if (smdpOid != null) {
      codeLength += smdpOid.encode(os, true);
    }
    
    codeLength += notificationMetadata.encode(os, false);
    
    os.write(47);
    os.write(191);
    codeLength += 2;
    
    codeLength += transactionId.encode(os, false);
    
    os.write(128);
    codeLength++;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 0)) {
      transactionId = new TransactionId();
      subCodeLength += transactionId.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 32, 47)) {
      notificationMetadata = new NotificationMetadata();
      subCodeLength += notificationMetadata.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(BerObjectIdentifier.tag)) {
      smdpOid = new BerObjectIdentifier();
      subCodeLength += smdpOid.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 32, 2)) {
      subCodeLength += length.decode(is);
      finalResult = new FinalResult();
      subCodeLength += finalResult.decode(is, null);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (transactionId != null) {
      sb.append("transactionId: ").append(transactionId);
    }
    else {
      sb.append("transactionId: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (notificationMetadata != null) {
      sb.append("notificationMetadata: ");
      notificationMetadata.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("notificationMetadata: <empty-required-field>");
    }
    
    if (smdpOid != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("smdpOid: ").append(smdpOid);
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (finalResult != null) {
      sb.append("finalResult: ");
      finalResult.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("finalResult: <empty-required-field>");
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
