package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class LoadCRLResponseError
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public LoadCRLResponseError() {}
  
  public LoadCRLResponseError(byte[] code)
  {
    super(code);
  }
  
  public LoadCRLResponseError(BigInteger value) {
    super(value);
  }
  
  public LoadCRLResponseError(long value) {
    super(value);
  }
}
