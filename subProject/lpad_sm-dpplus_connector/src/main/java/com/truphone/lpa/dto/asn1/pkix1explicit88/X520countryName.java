package com.truphone.lpa.dto.asn1.pkix1explicit88;

import org.openmuc.jasn1.ber.types.string.BerPrintableString;
















public class X520countryName
  extends BerPrintableString
{
  private static final long serialVersionUID = 1L;
  
  public X520countryName() {}
  
  public X520countryName(byte[] value)
  {
    super(value);
  }
}
