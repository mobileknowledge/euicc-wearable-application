package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
















public class RemoteProfileProvisioningRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  public static final BerTag tag = new BerTag(128, 32, 2);
  
  private InitiateAuthenticationRequest initiateAuthenticationRequest = null;
  private AuthenticateClientRequest authenticateClientRequest = null;
  private GetBoundProfilePackageRequest getBoundProfilePackageRequest = null;
  private CancelSessionRequestEs9 cancelSessionRequestEs9 = null;
  private HandleNotification handleNotification = null;
  
  public RemoteProfileProvisioningRequest() {}
  
  public RemoteProfileProvisioningRequest(byte[] code)
  {
    this.code = code;
  }
  
  public void setInitiateAuthenticationRequest(InitiateAuthenticationRequest initiateAuthenticationRequest) {
    this.initiateAuthenticationRequest = initiateAuthenticationRequest;
  }
  
  public InitiateAuthenticationRequest getInitiateAuthenticationRequest() {
    return initiateAuthenticationRequest;
  }
  
  public void setAuthenticateClientRequest(AuthenticateClientRequest authenticateClientRequest) {
    this.authenticateClientRequest = authenticateClientRequest;
  }
  
  public AuthenticateClientRequest getAuthenticateClientRequest() {
    return authenticateClientRequest;
  }
  
  public void setGetBoundProfilePackageRequest(GetBoundProfilePackageRequest getBoundProfilePackageRequest) {
    this.getBoundProfilePackageRequest = getBoundProfilePackageRequest;
  }
  
  public GetBoundProfilePackageRequest getGetBoundProfilePackageRequest() {
    return getBoundProfilePackageRequest;
  }
  
  public void setCancelSessionRequestEs9(CancelSessionRequestEs9 cancelSessionRequestEs9) {
    this.cancelSessionRequestEs9 = cancelSessionRequestEs9;
  }
  
  public CancelSessionRequestEs9 getCancelSessionRequestEs9() {
    return cancelSessionRequestEs9;
  }
  
  public void setHandleNotification(HandleNotification handleNotification) {
    this.handleNotification = handleNotification;
  }
  
  public HandleNotification getHandleNotification() {
    return handleNotification;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (handleNotification != null) {
      codeLength += handleNotification.encode(os, false);
      
      os.write(61);
      os.write(191);
      codeLength += 2;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (cancelSessionRequestEs9 != null) {
      codeLength += cancelSessionRequestEs9.encode(os, false);
      
      os.write(65);
      os.write(191);
      codeLength += 2;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (getBoundProfilePackageRequest != null) {
      codeLength += getBoundProfilePackageRequest.encode(os, false);
      
      os.write(58);
      os.write(191);
      codeLength += 2;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (authenticateClientRequest != null) {
      codeLength += authenticateClientRequest.encode(os, false);
      
      os.write(59);
      os.write(191);
      codeLength += 2;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (initiateAuthenticationRequest != null) {
      codeLength += initiateAuthenticationRequest.encode(os, false);
      
      os.write(57);
      os.write(191);
      codeLength += 2;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    BerLength length = new BerLength();
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    codeLength += length.decode(is);
    codeLength += berTag.decode(is);
    
    if (berTag.equals(128, 32, 57)) {
      initiateAuthenticationRequest = new InitiateAuthenticationRequest();
      codeLength += initiateAuthenticationRequest.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 59)) {
      authenticateClientRequest = new AuthenticateClientRequest();
      codeLength += authenticateClientRequest.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 58)) {
      getBoundProfilePackageRequest = new GetBoundProfilePackageRequest();
      codeLength += getBoundProfilePackageRequest.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 65)) {
      cancelSessionRequestEs9 = new CancelSessionRequestEs9();
      codeLength += cancelSessionRequestEs9.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 61)) {
      handleNotification = new HandleNotification();
      codeLength += handleNotification.decode(is, false);
      return codeLength;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (initiateAuthenticationRequest != null) {
      sb.append("initiateAuthenticationRequest: ");
      initiateAuthenticationRequest.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (authenticateClientRequest != null) {
      sb.append("authenticateClientRequest: ");
      authenticateClientRequest.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (getBoundProfilePackageRequest != null) {
      sb.append("getBoundProfilePackageRequest: ");
      getBoundProfilePackageRequest.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (cancelSessionRequestEs9 != null) {
      sb.append("cancelSessionRequestEs9: ");
      cancelSessionRequestEs9.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (handleNotification != null) {
      sb.append("handleNotification: ");
      handleNotification.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    sb.append("<none>");
  }
}
