package com.truphone.lpa.dto.asn1.rspdefinitions;

import org.openmuc.jasn1.ber.types.BerOctetString;




















public class Octet8
  extends BerOctetString
{
  private static final long serialVersionUID = 1L;
  
  public Octet8() {}
  
  public Octet8(byte[] value)
  {
    super(value);
  }
}
