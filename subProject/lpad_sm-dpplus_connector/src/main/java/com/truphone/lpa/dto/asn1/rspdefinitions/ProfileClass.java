package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class ProfileClass
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public ProfileClass() {}
  
  public ProfileClass(byte[] code)
  {
    super(code);
  }
  
  public ProfileClass(BigInteger value) {
    super(value);
  }
  
  public ProfileClass(long value) {
    super(value);
  }
}
