package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
















public class AuthenticateServerResponse
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  public static final BerTag tag = new BerTag(128, 32, 56);
  
  private AuthenticateResponseOk authenticateResponseOk = null;
  private AuthenticateResponseError authenticateResponseError = null;
  
  public AuthenticateServerResponse() {}
  
  public AuthenticateServerResponse(byte[] code)
  {
    this.code = code;
  }
  
  public void setAuthenticateResponseOk(AuthenticateResponseOk authenticateResponseOk) {
    this.authenticateResponseOk = authenticateResponseOk;
  }
  
  public AuthenticateResponseOk getAuthenticateResponseOk() {
    return authenticateResponseOk;
  }
  
  public void setAuthenticateResponseError(AuthenticateResponseError authenticateResponseError) {
    this.authenticateResponseError = authenticateResponseError;
  }
  
  public AuthenticateResponseError getAuthenticateResponseError() {
    return authenticateResponseError;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (authenticateResponseError != null) {
      codeLength += authenticateResponseError.encode(os, false);
      
      os.write(161);
      codeLength++;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    if (authenticateResponseOk != null) {
      codeLength += authenticateResponseOk.encode(os, false);
      
      os.write(160);
      codeLength++;
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    BerLength length = new BerLength();
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    codeLength += length.decode(is);
    codeLength += berTag.decode(is);
    
    if (berTag.equals(128, 32, 0)) {
      authenticateResponseOk = new AuthenticateResponseOk();
      codeLength += authenticateResponseOk.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(128, 32, 1)) {
      authenticateResponseError = new AuthenticateResponseError();
      codeLength += authenticateResponseError.decode(is, false);
      return codeLength;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (authenticateResponseOk != null) {
      sb.append("authenticateResponseOk: ");
      authenticateResponseOk.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    if (authenticateResponseError != null) {
      sb.append("authenticateResponseError: ");
      authenticateResponseError.appendAsString(sb, indentLevel + 1);
      return;
    }
    
    sb.append("<none>");
  }
}
