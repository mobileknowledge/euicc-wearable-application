//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;

public class TBSCertificate implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  public byte[] code = null;
  private Version version = null;
  private CertificateSerialNumber serialNumber = null;
  private AlgorithmIdentifier signature = null;
  private Name issuer = null;
  private Validity validity = null;
  private Name subject = null;
  private SubjectPublicKeyInfo subjectPublicKeyInfo = null;
  private UniqueIdentifier issuerUniqueID = null;
  private UniqueIdentifier subjectUniqueID = null;
  private Extensions extensions = null;

  public TBSCertificate() {
  }

  public TBSCertificate(byte[] code) {
    this.code = code;
  }

  public void setVersion(Version version) {
    this.version = version;
  }

  public Version getVersion() {
    return this.version;
  }

  public void setSerialNumber(CertificateSerialNumber serialNumber) {
    this.serialNumber = serialNumber;
  }

  public CertificateSerialNumber getSerialNumber() {
    return this.serialNumber;
  }

  public void setSignature(AlgorithmIdentifier signature) {
    this.signature = signature;
  }

  public AlgorithmIdentifier getSignature() {
    return this.signature;
  }

  public void setIssuer(Name issuer) {
    this.issuer = issuer;
  }

  public Name getIssuer() {
    return this.issuer;
  }

  public void setValidity(Validity validity) {
    this.validity = validity;
  }

  public Validity getValidity() {
    return this.validity;
  }

  public void setSubject(Name subject) {
    this.subject = subject;
  }

  public Name getSubject() {
    return this.subject;
  }

  public void setSubjectPublicKeyInfo(SubjectPublicKeyInfo subjectPublicKeyInfo) {
    this.subjectPublicKeyInfo = subjectPublicKeyInfo;
  }

  public SubjectPublicKeyInfo getSubjectPublicKeyInfo() {
    return this.subjectPublicKeyInfo;
  }

  public void setIssuerUniqueID(UniqueIdentifier issuerUniqueID) {
    this.issuerUniqueID = issuerUniqueID;
  }

  public UniqueIdentifier getIssuerUniqueID() {
    return this.issuerUniqueID;
  }

  public void setSubjectUniqueID(UniqueIdentifier subjectUniqueID) {
    this.subjectUniqueID = subjectUniqueID;
  }

  public UniqueIdentifier getSubjectUniqueID() {
    return this.subjectUniqueID;
  }

  public void setExtensions(Extensions extensions) {
    this.extensions = extensions;
  }

  public Extensions getExtensions() {
    return this.extensions;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int codeLength;
    if (this.code == null) {
      codeLength = 0;
      int sublength;
      if (this.extensions != null) {
        sublength = this.extensions.encode(os, true);
        codeLength += sublength;
        codeLength += BerLength.encodeLength(os, sublength);
        os.write(163);
        ++codeLength;
      }

      if (this.subjectUniqueID != null) {
        codeLength += this.subjectUniqueID.encode(os, false);
        os.write(130);
        ++codeLength;
      }

      if (this.issuerUniqueID != null) {
        codeLength += this.issuerUniqueID.encode(os, false);
        os.write(129);
        ++codeLength;
      }

      codeLength += this.subjectPublicKeyInfo.encode(os, true);
      codeLength += this.subject.encode(os);
      codeLength += this.validity.encode(os, true);
      codeLength += this.issuer.encode(os);
      codeLength += this.signature.encode(os, true);
      codeLength += this.serialNumber.encode(os, true);
      if (this.version != null) {
        sublength = this.version.encode(os, true);
        codeLength += sublength;
        codeLength += BerLength.encodeLength(os, sublength);
        os.write(160);
        ++codeLength;
      }

      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }

      return codeLength;
    } else {
      for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
        os.write(this.code[codeLength]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;
    codeLength += totalLength;
    subCodeLength = subCodeLength + berTag.decode(is);
    if (berTag.equals(128, 32, 0)) {
      subCodeLength += length.decode(is);
      this.version = new Version();
      subCodeLength += this.version.decode(is, true);
      subCodeLength += berTag.decode(is);
    }

    if (berTag.equals(CertificateSerialNumber.tag)) {
      this.serialNumber = new CertificateSerialNumber();
      subCodeLength += this.serialNumber.decode(is, false);
      subCodeLength += berTag.decode(is);
      if (berTag.equals(AlgorithmIdentifier.tag)) {
        this.signature = new AlgorithmIdentifier();
        subCodeLength += this.signature.decode(is, false);
        subCodeLength += berTag.decode(is);
        this.issuer = new Name();
        subCodeLength += this.issuer.decode(is, berTag);
        subCodeLength += berTag.decode(is);
        if (berTag.equals(Validity.tag)) {
          this.validity = new Validity();
          subCodeLength += this.validity.decode(is, false);
          subCodeLength += berTag.decode(is);
          this.subject = new Name();
          subCodeLength += this.subject.decode(is, berTag);
          subCodeLength += berTag.decode(is);
          if (berTag.equals(SubjectPublicKeyInfo.tag)) {
            this.subjectPublicKeyInfo = new SubjectPublicKeyInfo();
            subCodeLength += this.subjectPublicKeyInfo.decode(is, false);
            if (subCodeLength == totalLength) {
              return codeLength;
            } else {
              subCodeLength += berTag.decode(is);
              if (berTag.equals(128, 0, 1)) {
                this.issuerUniqueID = new UniqueIdentifier();
                subCodeLength += this.issuerUniqueID.decode(is, false);
                if (subCodeLength == totalLength) {
                  return codeLength;
                }

                subCodeLength += berTag.decode(is);
              }

              if (berTag.equals(128, 0, 2)) {
                this.subjectUniqueID = new UniqueIdentifier();
                subCodeLength += this.subjectUniqueID.decode(is, false);
                if (subCodeLength == totalLength) {
                  return codeLength;
                }

                subCodeLength += berTag.decode(is);
              }

              if (berTag.equals(128, 32, 3)) {
                subCodeLength += length.decode(is);
                this.extensions = new Extensions();
                subCodeLength += this.extensions.decode(is, true);
                if (subCodeLength == totalLength) {
                  return codeLength;
                }
              }

              throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
            }
          } else {
            throw new IOException("Tag does not match the mandatory sequence element tag.");
          }
        } else {
          throw new IOException("Tag does not match the mandatory sequence element tag.");
        }
      } else {
        throw new IOException("Tag does not match the mandatory sequence element tag.");
      }
    } else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    boolean firstSelectedElement = true;
    int i;
    if (this.version != null) {
      sb.append("\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("version: ").append(this.version);
      firstSelectedElement = false;
    }

    if (!firstSelectedElement) {
      sb.append(",\n");
    }

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.serialNumber != null) {
      sb.append("serialNumber: ").append(this.serialNumber);
    } else {
      sb.append("serialNumber: <empty-required-field>");
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.signature != null) {
      sb.append("signature: ");
      this.signature.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("signature: <empty-required-field>");
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.issuer != null) {
      sb.append("issuer: ");
      this.issuer.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("issuer: <empty-required-field>");
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.validity != null) {
      sb.append("validity: ");
      this.validity.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("validity: <empty-required-field>");
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.subject != null) {
      sb.append("subject: ");
      this.subject.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("subject: <empty-required-field>");
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.subjectPublicKeyInfo != null) {
      sb.append("subjectPublicKeyInfo: ");
      this.subjectPublicKeyInfo.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("subjectPublicKeyInfo: <empty-required-field>");
    }

    if (this.issuerUniqueID != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("issuerUniqueID: ").append(this.issuerUniqueID);
    }

    if (this.subjectUniqueID != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("subjectUniqueID: ").append(this.subjectUniqueID);
    }

    if (this.extensions != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("extensions: ");
      this.extensions.appendAsString(sb, indentLevel + 1);
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }
}
