package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;



















public class ErrorReason
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public ErrorReason() {}
  
  public ErrorReason(byte[] code)
  {
    super(code);
  }
  
  public ErrorReason(BigInteger value) {
    super(value);
  }
  
  public ErrorReason(long value) {
    super(value);
  }
}
