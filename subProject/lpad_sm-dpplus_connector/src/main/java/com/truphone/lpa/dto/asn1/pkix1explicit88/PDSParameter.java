//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.string.BerPrintableString;
import org.openmuc.jasn1.ber.types.string.BerTeletexString;

public class PDSParameter implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 17);
  public byte[] code = null;
  private BerPrintableString printableString = null;
  private BerTeletexString teletexString = null;

  public PDSParameter() {
  }

  public PDSParameter(byte[] code) {
    this.code = code;
  }

  public void setPrintableString(BerPrintableString printableString) {
    this.printableString = printableString;
  }

  public BerPrintableString getPrintableString() {
    return this.printableString;
  }

  public void setTeletexString(BerTeletexString teletexString) {
    this.teletexString = teletexString;
  }

  public BerTeletexString getTeletexString() {
    return this.teletexString;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int codeLength;
    if (this.code == null) {
      codeLength = 0;
      if (this.teletexString != null) {
        codeLength += this.teletexString.encode(os, true);
      }

      if (this.printableString != null) {
        codeLength += this.printableString.encode(os, true);
      }

      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }

      return codeLength;
    } else {
      for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
        os.write(this.code[codeLength]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;

    while(subCodeLength < totalLength) {
      subCodeLength += berTag.decode(is);
      if (berTag.equals(BerPrintableString.tag)) {
        this.printableString = new BerPrintableString();
        subCodeLength += this.printableString.decode(is, false);
      } else if (berTag.equals(BerTeletexString.tag)) {
        this.teletexString = new BerTeletexString();
        subCodeLength += this.teletexString.decode(is, false);
      }
    }

    if (subCodeLength != totalLength) {
      throw new IOException("Length of set does not match length tag, length tag: " + totalLength + ", actual set length: " + subCodeLength);
    } else {
      codeLength += subCodeLength;
      return codeLength;
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    boolean firstSelectedElement = true;
    int i;
    if (this.printableString != null) {
      sb.append("\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("printableString: ").append(this.printableString);
      firstSelectedElement = false;
    }

    if (this.teletexString != null) {
      if (!firstSelectedElement) {
        sb.append(",\n");
      }

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("teletexString: ").append(this.teletexString);
      firstSelectedElement = false;
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }
}
