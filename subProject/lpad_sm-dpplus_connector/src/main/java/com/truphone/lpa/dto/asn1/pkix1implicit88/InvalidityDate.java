package com.truphone.lpa.dto.asn1.pkix1implicit88;

import org.openmuc.jasn1.ber.types.BerGeneralizedTime;






















public class InvalidityDate
  extends BerGeneralizedTime
{
  private static final long serialVersionUID = 1L;
  
  public InvalidityDate() {}
  
  public InvalidityDate(byte[] value)
  {
    super(value);
  }
}
