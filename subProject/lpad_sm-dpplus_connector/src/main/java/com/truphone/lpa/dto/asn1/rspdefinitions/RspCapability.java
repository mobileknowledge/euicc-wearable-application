package com.truphone.lpa.dto.asn1.rspdefinitions;

import org.openmuc.jasn1.ber.types.BerBitString;




















public class RspCapability
  extends BerBitString
{
  private static final long serialVersionUID = 1L;
  
  public RspCapability() {}
  
  public RspCapability(byte[] code)
  {
    super(code);
  }
  
  public RspCapability(byte[] value, int numBits) {
    super(value, numBits);
  }
}
