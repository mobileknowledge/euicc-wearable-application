package com.truphone.lpa.dto.asn1.pkix1explicit88;

import org.openmuc.jasn1.ber.types.BerAny;
















public class AttributeValue
  extends BerAny
{
  private static final long serialVersionUID = 1L;
  
  public AttributeValue() {}
  
  public AttributeValue(byte[] value)
  {
    super(value);
  }
}
