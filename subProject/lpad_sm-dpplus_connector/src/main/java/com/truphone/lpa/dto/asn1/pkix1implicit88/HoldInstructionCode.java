package com.truphone.lpa.dto.asn1.pkix1implicit88;

import org.openmuc.jasn1.ber.types.BerObjectIdentifier;






















public class HoldInstructionCode
  extends BerObjectIdentifier
{
  private static final long serialVersionUID = 1L;
  
  public HoldInstructionCode() {}
  
  public HoldInstructionCode(byte[] code)
  {
    super(code);
  }
  
  public HoldInstructionCode(int[] value) {
    super(value);
  }
}
