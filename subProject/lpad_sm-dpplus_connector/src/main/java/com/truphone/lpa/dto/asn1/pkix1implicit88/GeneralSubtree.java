package com.truphone.lpa.dto.asn1.pkix1implicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;


















public class GeneralSubtree
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private GeneralName base = null;
  private BaseDistance minimum = null;
  private BaseDistance maximum = null;
  
  public GeneralSubtree() {}
  
  public GeneralSubtree(byte[] code)
  {
    this.code = code;
  }
  
  public void setBase(GeneralName base) {
    this.base = base;
  }
  
  public GeneralName getBase() {
    return base;
  }
  
  public void setMinimum(BaseDistance minimum) {
    this.minimum = minimum;
  }
  
  public BaseDistance getMinimum() {
    return minimum;
  }
  
  public void setMaximum(BaseDistance maximum) {
    this.maximum = maximum;
  }
  
  public BaseDistance getMaximum() {
    return maximum;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (maximum != null) {
      codeLength += maximum.encode(os, false);
      
      os.write(129);
      codeLength++;
    }
    
    if (minimum != null) {
      codeLength += minimum.encode(os, false);
      
      os.write(128);
      codeLength++;
    }
    
    codeLength += base.encode(os);
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    base = new GeneralName();
    subCodeLength += base.decode(is, berTag);
    if (subCodeLength == totalLength) {
      return codeLength;
    }
    subCodeLength += berTag.decode(is);
    
    if (berTag.equals(128, 0, 0)) {
      minimum = new BaseDistance();
      subCodeLength += minimum.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 1)) {
      maximum = new BaseDistance();
      subCodeLength += maximum.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (base != null) {
      sb.append("base: ");
      base.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("base: <empty-required-field>");
    }
    
    if (minimum != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("minimum: ").append(minimum);
    }
    
    if (maximum != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("maximum: ").append(maximum);
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
