//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.string.BerTeletexString;

public class TeletexDomainDefinedAttribute implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  public byte[] code = null;
  private BerTeletexString type = null;
  private BerTeletexString value = null;

  public TeletexDomainDefinedAttribute() {
  }

  public TeletexDomainDefinedAttribute(byte[] code) {
    this.code = code;
  }

  public void setType(BerTeletexString type) {
    this.type = type;
  }

  public BerTeletexString getType() {
    return this.type;
  }

  public void setValue(BerTeletexString value) {
    this.value = value;
  }

  public BerTeletexString getValue() {
    return this.value;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int i;
    if (this.code == null) {
      int codeLength = 0;
      i = codeLength + this.value.encode(os, true);
      i += this.type.encode(os, true);
      i += BerLength.encodeLength(os, i);
      if (withTag) {
        i += tag.encode(os);
      }

      return i;
    } else {
      for(i = this.code.length - 1; i >= 0; --i) {
        os.write(this.code[i]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;
    codeLength += totalLength;
    subCodeLength = subCodeLength + berTag.decode(is);
    if (berTag.equals(BerTeletexString.tag)) {
      this.type = new BerTeletexString();
      subCodeLength += this.type.decode(is, false);
      subCodeLength += berTag.decode(is);
      if (berTag.equals(BerTeletexString.tag)) {
        this.value = new BerTeletexString();
        subCodeLength += this.value.decode(is, false);
        if (subCodeLength == totalLength) {
          return codeLength;
        }
      }

      throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
    } else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    sb.append("\n");

    int i;
    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.type != null) {
      sb.append("type: ").append(this.type);
    } else {
      sb.append("type: <empty-required-field>");
    }

    sb.append(",\n");

    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.value != null) {
      sb.append("value: ").append(this.value);
    } else {
      sb.append("value: <empty-required-field>");
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }
}
