package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerInteger;









public class LoadCRLResponseOk
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public LoadCRLResponseOk() {}
  
  public static class MissingParts
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    
    public static class SEQUENCE
      implements Serializable
    {
      private static final long serialVersionUID = 1L;
      public static final BerTag tag = new BerTag(0, 32, 16);
      
      public byte[] code = null;
      private BerInteger number = null;
      
      public SEQUENCE() {}
      
      public SEQUENCE(byte[] code)
      {
        this.code = code;
      }
      
      public void setNumber(BerInteger number) {
        this.number = number;
      }
      
      public BerInteger getNumber() {
        return number;
      }
      
      public int encode(BerByteArrayOutputStream os) throws IOException {
        return encode(os, true);
      }
      
      public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
      {
        if (code != null) {
          for (int i = code.length - 1; i >= 0; i--) {
            os.write(code[i]);
          }
          if (withTag) {
            return tag.encode(os) + code.length;
          }
          return code.length;
        }
        
        int codeLength = 0;
        codeLength += number.encode(os, false);
        
        os.write(128);
        codeLength++;
        
        codeLength += BerLength.encodeLength(os, codeLength);
        
        if (withTag) {
          codeLength += tag.encode(os);
        }
        
        return codeLength;
      }
      
      public int decode(InputStream is) throws IOException
      {
        return decode(is, true);
      }
      
      public int decode(InputStream is, boolean withTag) throws IOException {
        int codeLength = 0;
        int subCodeLength = 0;
        BerTag berTag = new BerTag();
        
        if (withTag) {
          codeLength += tag.decodeAndCheck(is);
        }
        
        BerLength length = new BerLength();
        codeLength += length.decode(is);
        
        int totalLength = length.val;
        codeLength += totalLength;
        
        subCodeLength += berTag.decode(is);
        if (berTag.equals(128, 0, 0)) {
          number = new BerInteger();
          subCodeLength += number.decode(is, false);
          if (subCodeLength == totalLength) {
            return codeLength;
          }
        }
        throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
      }
      
      public void encodeAndSave(int encodingSizeGuess)
        throws IOException
      {
        BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
        encode(os, false);
        code = os.getArray();
      }
      
      public String toString() {
        StringBuilder sb = new StringBuilder();
        appendAsString(sb, 0);
        return sb.toString();
      }
      
      public void appendAsString(StringBuilder sb, int indentLevel)
      {
        sb.append("{");
        sb.append("\n");
        for (int i = 0; i < indentLevel + 1; i++) {
          sb.append("\t");
        }
        if (number != null) {
          sb.append("number: ").append(number);
        }
        else {
          sb.append("number: <empty-required-field>");
        }
        
        sb.append("\n");
        for (int i = 0; i < indentLevel; i++) {
          sb.append("\t");
        }
        sb.append("}");
      }
    }
    

    public static final BerTag tag = new BerTag(0, 32, 16);
    public byte[] code = null;
    private List<SEQUENCE> seqOf = null;
    
    public MissingParts() {
      seqOf = new ArrayList();
    }
    
    public MissingParts(byte[] code) {
      this.code = code;
    }
    
    public List<SEQUENCE> getSEQUENCE() {
      if (seqOf == null) {
        seqOf = new ArrayList();
      }
      return seqOf;
    }
    
    public int encode(BerByteArrayOutputStream os) throws IOException {
      return encode(os, true);
    }
    
    public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
    {
      if (code != null) {
        for (int i = code.length - 1; i >= 0; i--) {
          os.write(code[i]);
        }
        if (withTag) {
          return tag.encode(os) + code.length;
        }
        return code.length;
      }
      
      int codeLength = 0;
      for (int i = seqOf.size() - 1; i >= 0; i--) {
        codeLength += seqOf.get(i).encode(os, true);
      }
      
      codeLength += BerLength.encodeLength(os, codeLength);
      
      if (withTag) {
        codeLength += tag.encode(os);
      }
      
      return codeLength;
    }
    
    public int decode(InputStream is) throws IOException {
      return decode(is, true);
    }
    
    public int decode(InputStream is, boolean withTag) throws IOException {
      int codeLength = 0;
      int subCodeLength = 0;
      if (withTag) {
        codeLength += tag.decodeAndCheck(is);
      }
      
      BerLength length = new BerLength();
      codeLength += length.decode(is);
      int totalLength = length.val;
      
      while (subCodeLength < totalLength) {
        SEQUENCE element = new SEQUENCE();
        subCodeLength += element.decode(is, true);
        seqOf.add(element);
      }
      if (subCodeLength != totalLength) {
        throw new IOException("Decoded SequenceOf or SetOf has wrong length. Expected " + totalLength + " but has " + subCodeLength);
      }
      
      codeLength += subCodeLength;
      
      return codeLength;
    }
    
    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      encode(os, false);
      code = os.getArray();
    }
    
    public String toString() {
      StringBuilder sb = new StringBuilder();
      appendAsString(sb, 0);
      return sb.toString();
    }
    
    public void appendAsString(StringBuilder sb, int indentLevel)
    {
      sb.append("{\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      if (seqOf == null) {
        sb.append("null");
      }
      else {
        Iterator<SEQUENCE> it = seqOf.iterator();
        if (it.hasNext()) {
          it.next().appendAsString(sb, indentLevel + 1);
          while (it.hasNext()) {
            sb.append(",\n");
            for (int i = 0; i < indentLevel + 1; i++) {
              sb.append("\t");
            }
            it.next().appendAsString(sb, indentLevel + 1);
          }
        }
      }
      
      sb.append("\n");
      for (int i = 0; i < indentLevel; i++) {
        sb.append("\t");
      }
      sb.append("}");
    }
  }
  

  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private MissingParts missingParts = null;
  


  public LoadCRLResponseOk(byte[] code)
  {
    this.code = code;
  }
  
  public void setMissingParts(MissingParts missingParts) {
    this.missingParts = missingParts;
  }
  
  public MissingParts getMissingParts() {
    return missingParts;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (missingParts != null) {
      codeLength += missingParts.encode(os, false);
      
      os.write(160);
      codeLength++;
    }
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    if (totalLength == 0) {
      return codeLength;
    }
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 32, 0)) {
      missingParts = new MissingParts();
      subCodeLength += missingParts.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    if (missingParts != null) {
      sb.append("\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("missingParts: ");
      missingParts.appendAsString(sb, indentLevel + 1);
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
