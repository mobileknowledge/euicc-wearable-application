package com.truphone.lpa.dto.asn1.pkix1implicit88;

import org.openmuc.jasn1.ber.types.BerObjectIdentifier;






















public class KeyPurposeId
  extends BerObjectIdentifier
{
  private static final long serialVersionUID = 1L;
  
  public KeyPurposeId() {}
  
  public KeyPurposeId(byte[] code)
  {
    super(code);
  }
  
  public KeyPurposeId(int[] value) {
    super(value);
  }
}
