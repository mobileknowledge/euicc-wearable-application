package com.truphone.lpa.impl.download;


import android.org.apache.commons.codec.DecoderException;
import android.org.apache.commons.codec.binary.Base64;
import android.org.apache.commons.codec.binary.Hex;

import com.truphone.es9plus.Es9PlusImpl;
import com.truphone.es9plus.LpaUtils;
import com.truphone.es9plus.message.response.AuthenticateClientResp;
import com.truphone.es9plus.message.response.InitiateAuthenticationResp;
import com.truphone.lpa.apdu.ApduUtils;
import com.truphone.lpa.dto.asn1.rspdefinitions.GetEuiccChallengeResponse;
import com.truphone.lpa.impl.AuthenticateClientSmDp;
import com.truphone.lpa.impl.InitialAuthenticationKeys;
import com.truphone.lpa.progress.DownloadProgress;
import com.truphone.lpa.progress.DownloadProgressPhase;
import com.truphone.util.LogStub;
import com.truphone.util.Util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static android.org.apache.commons.codec.binary.Base64.decodeBase64;
import static com.truphone.lpad.progress.ProgressStep.*;

//import org.apache.commons.codec.binary.Base64;

public class AuthenticatingPhaseWorker {
    private static final Logger LOG = Logger.getLogger(AuthenticatingPhaseWorker.class.getName());

    private final DownloadProgress progress;
    private final ApduTransmitter apduTransmitter;
    private final Es9PlusImpl es9Module;
    private String confirmationCode;
    private String hashCc;

    public AuthenticatingPhaseWorker(DownloadProgress progress, ApduTransmitter apduTransmitter, Es9PlusImpl es9Module, String confirmationCode) {

        this.progress = progress;
        this.apduTransmitter = apduTransmitter;
        this.es9Module = es9Module;
        this.confirmationCode = confirmationCode;
    }

    public String getEuiccInfo() {


        progress.setCurrentPhase(DownloadProgressPhase.AUTHENTICATING);
        progress.stepExecuted(DOWNLOAD_PROFILE_GET_EUICC_INFO, "getEuiccInfo retrieving...");
        //String rsp = "BF20358203020100A9160414F54172BDF98A95D65CBEB88A38A1C11D800A85C3AA160414F54172BDF98A95D65CBEB88A38A1C11D800A85C39000";
        return convertEuiccInfo1(apduTransmitter.transmitApdu(ApduUtils.getEuiccInfo1Apdu()));
       // return convertEuiccInfo1(rsp);
    }

    private String convertEuiccInfo1(String euicInfo1APDUResponse) {
        String euiccInfo1;

        progress.stepExecuted(DOWNLOAD_PROFILE_CONVERTING_EUICC_INFO, "getEUICCInfo1 Success!");

        euiccInfo1 = euicInfo1APDUResponse.toUpperCase();

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - EUICC Info Object: " + euiccInfo1);
        }

        if (euiccInfo1.endsWith("9000"))
        {
            euiccInfo1 = euiccInfo1.substring(0,euiccInfo1.length() -4);
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - EUICC Info Object mod: " + euiccInfo1);


        }
        String encodedString = new String(Base64.encodeBase64(Util.hexStringToByteArray(euiccInfo1)));
        //euiccInfo1 = encodedString.replace('+','-').replace('/','_');
        euiccInfo1 = encodedString;

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - EUICC Info Object (Base 64): " + euiccInfo1);
        }

        progress.stepExecuted(DOWNLOAD_PROFILE_CONVERTED_EUICC_INFO, "getEUICCInfo1 Success!");

        return euiccInfo1;
    }

    public String getEuiccChallenge(String matchingId) {

        progress.stepExecuted(DOWNLOAD_PROFILE_GET_EUICC_CHALLENGE, "getEuiccChallenge retrieving...");
        //String rsp = "BF2E1280108F76C6EFBE239C01155B9D0B96FE3F159000";

        return convertEuiccChallenge(apduTransmitter.transmitApdu(ApduUtils.getEUICCChallengeApdu()), matchingId);
       // return convertEuiccChallenge(rsp, matchingId);
    }

    private String convertEuiccChallenge(String euiccChallengeApduResponse, String matchingId) {
        String euiccChallenge;

        progress.stepExecuted(DOWNLOAD_PROFILE_CONVERTING_EUICC_CHALLENGE, "convertEuiccChallenge converting...");

        try {
            //euiccChallenge = Base64.encodeBase64String(Util.hexStringToByteArray(decodeGetEuiccChallengeResponse(euiccChallengeApduResponse)));

            String encodedString = new String(Base64.encodeBase64(Util.hexStringToByteArray(decodeGetEuiccChallengeResponse(euiccChallengeApduResponse))));
            //euiccChallenge = encodedString.replace('+','-').replace('/','_');
            euiccChallenge = encodedString;

            if (LogStub.getInstance().isDebugEnabled()) {
                LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - eUICCChallenge is " + euiccChallenge);
            }
        } catch (DecoderException e) {
            LOG.log(Level.SEVERE, "KOL.007" + e.getMessage(), e);
            LOG.severe(LogStub.getInstance().getTag() + " - matchingId: " + matchingId +
                    " Unable to retrieve eUICC challenge. Exception in Decoder:" + e.getMessage());

            throw new RuntimeException("Unable to retrieve eUICC challenge: " + matchingId);
        } catch (IOException ioe) {
            LOG.log(Level.SEVERE, "KOL.007" + ioe.getMessage(), ioe);
            LOG.severe(LogStub.getInstance().getTag() + " - matchingId: " + matchingId +
                    " Unable to retrieve eUICC challenge. IOException:" + ioe.getMessage());

            throw new RuntimeException("Unable to retrieve eUICC challenge");
        }

        progress.stepExecuted(DOWNLOAD_PROFILE_CONVERTED_EUICC_CHALLENGE, "convertEuiccChallenge converted...");

        return euiccChallenge;
    }

    private String decodeGetEuiccChallengeResponse(String euiccChallengeApduResponse) throws DecoderException, IOException {
        InputStream is = null;

        try {
            GetEuiccChallengeResponse euiccChallengeResponse = new GetEuiccChallengeResponse();

            is = new ByteArrayInputStream(Hex.decodeHex(euiccChallengeApduResponse.toCharArray()));

            euiccChallengeResponse.decode(is);

            if (LogStub.getInstance().isDebugEnabled()) {
                LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Decoded euiccChallengeResponse: " + euiccChallengeResponse.toString());
            }

            return euiccChallengeResponse.getEuiccChallenge().toString();
        } finally {
            CloseResources.closeResources(is);
        }
    }

    public void initiateAuthentication(InitialAuthenticationKeys initialAuthenticationKeys) {

        progress.stepExecuted(DOWNLOAD_PROFILE_INITIATE_AUTHENTICATION, "initiateAuthentication retrieving...");

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Initiating Auth with SM-DP+");
        }

        InitiateAuthenticationResp initiateAuthenticationResp = getInitiateAuthenticationResp(initialAuthenticationKeys);

        setServerSigned1(initialAuthenticationKeys, initiateAuthenticationResp);
        setServerSignature1(initialAuthenticationKeys, initiateAuthenticationResp);
        setEuiccCiPKIdToveUsed(initialAuthenticationKeys, initiateAuthenticationResp);
        setServerCertificate(initialAuthenticationKeys, initiateAuthenticationResp);
        setTransactionId(initialAuthenticationKeys, initiateAuthenticationResp);
        setMatchingId(initialAuthenticationKeys);
        setCtxParams1(initialAuthenticationKeys);

        progress.stepExecuted(DOWNLOAD_PROFILE_INITIATED_AUTHENTICATION, "initiateAuthentication initiated...");
    }

    private void setCtxParams1(InitialAuthenticationKeys initialAuthenticationKeys) {

        initialAuthenticationKeys.setCtxParams1(LpaUtils.generateCtxParams1());

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - ctxParams1: " + initialAuthenticationKeys.getCtxParams1());
        }
    }

    private void setMatchingId(InitialAuthenticationKeys initialAuthenticationKeys) {

        initialAuthenticationKeys.setMatchingId(Util.ASCIIToHex(initialAuthenticationKeys.getMatchingId()));

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - matchingId: " + initialAuthenticationKeys.getMatchingId());
        }
    }

    private void setTransactionId(InitialAuthenticationKeys initialAuthenticationKeys, InitiateAuthenticationResp initiateAuthenticationResp) {

        initialAuthenticationKeys.setTransactionId(initiateAuthenticationResp.getTransactionId());

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - transactionId: " + initialAuthenticationKeys.getTransactionId());
        }
    }

    private void setServerCertificate(InitialAuthenticationKeys initialAuthenticationKeys, InitiateAuthenticationResp initiateAuthenticationResp) {

        initialAuthenticationKeys.setServerCertificate(initiateAuthenticationResp.getServerCertificate());
        initialAuthenticationKeys.setServerCertificate(Util.byteArrayToHexString(decodeBase64(initialAuthenticationKeys.getServerCertificate()), ""));

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - serverCertificate: " + initialAuthenticationKeys.getServerCertificate());
        }
    }

    private void setEuiccCiPKIdToveUsed(InitialAuthenticationKeys initialAuthenticationKeys, InitiateAuthenticationResp initiateAuthenticationResp) {

        initialAuthenticationKeys.setEuiccCiPKIdTobeUsed(initiateAuthenticationResp.getEuiccCiPKIdToBeUsed());
        initialAuthenticationKeys.setEuiccCiPKIdTobeUsed(Util.byteArrayToHexString(decodeBase64(initialAuthenticationKeys.getEuiccCiPKIdTobeUsed()), ""));

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - euiccCiPKIdTobeUsed: " + initialAuthenticationKeys.getEuiccCiPKIdTobeUsed());
        }
    }

    private void setServerSignature1(InitialAuthenticationKeys initialAuthenticationKeys, InitiateAuthenticationResp initiateAuthenticationResp) {

        initialAuthenticationKeys.setServerSignature1(initiateAuthenticationResp.getServerSignature1());
        initialAuthenticationKeys.setServerSignature1(Util.byteArrayToHexString(decodeBase64(initialAuthenticationKeys.getServerSignature1()), ""));

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - serverSignature1: " + initialAuthenticationKeys.getServerSignature1());
        }
    }

    private void setServerSigned1(InitialAuthenticationKeys initialAuthenticationKeys, InitiateAuthenticationResp initiateAuthenticationResp) {

        initialAuthenticationKeys.setServerSigned1(initiateAuthenticationResp.getServerSigned1());

        initialAuthenticationKeys.setServerSigned1(Util.byteArrayToHexString(Base64.decodeBase64(initialAuthenticationKeys.getServerSigned1()), ""));


        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - serverSigned1: " + initialAuthenticationKeys.getServerSigned1());
        }
    }

    private InitiateAuthenticationResp getInitiateAuthenticationResp(InitialAuthenticationKeys initialAuthenticationKeys) {
        InitiateAuthenticationResp initiateAuthenticationResp = es9Module.initiateAuthentication(initialAuthenticationKeys.getEuiccChallenge(),
                initialAuthenticationKeys.getEuiccInfo1(), initialAuthenticationKeys.getEuiccConfiguredAddress());

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Initiating Auth with SM-DP+ - Response: " + initiateAuthenticationResp);
        }

        return initiateAuthenticationResp;
    }

    public AuthenticateClientSmDp authenticateClient(InitialAuthenticationKeys initialAuthenticationKeys, String encodedAuthenticateServerResponse) {

        progress.stepExecuted(DOWNLOAD_PROFILE_AUTHENTICATE_CLIENT, "authenticateClient retrieving...");

        AuthenticateClientResp authenticateClientResp = getAuthenticateClientResponse(initialAuthenticationKeys, encodedAuthenticateServerResponse);
        AuthenticateClientSmDp authenticateClientSmDp = convertAuthenticateClientResp(authenticateClientResp);

        if(confirmationCode != null)
        {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                String cCSHA = (ApduUtils.toHexString(md.digest(confirmationCode.getBytes(StandardCharsets.UTF_8)))).toUpperCase();
                hashCc = cCSHA.concat(initialAuthenticationKeys.getTransactionId());
                hashCc = (ApduUtils.toHexString(md.digest(Util.hexStringToByteArray(hashCc)))).toUpperCase();

                LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Phase 2: SHA256 of CC("
                        + cCSHA + ") + Tansaction ID(" + initialAuthenticationKeys.getTransactionId() + "): " + hashCc);
            } catch (NoSuchAlgorithmException e) {
                LOG.log(Level.SEVERE, "KOL.007" + e.getMessage(), e);
                LOG.severe(LogStub.getInstance().getTag() + " - transactionId: " + initialAuthenticationKeys.getTransactionId() +
                        " Unable to calculate SHA256 of Confirmation Code NoSuchAlgorithmException:" + e.getMessage());
            }
            authenticateClientSmDp.setHashCc(hashCc);
        }else {
            authenticateClientSmDp.setHashCc(null);
        }

        progress.stepExecuted(DOWNLOAD_PROFILE_AUTHENTICATED_CLIENT, "authenticateClient authenticated...");

        return authenticateClientSmDp;
    }

    private AuthenticateClientSmDp convertAuthenticateClientResp(AuthenticateClientResp authenticateClientResp) {
        AuthenticateClientSmDp authenticateClientSmDp = new AuthenticateClientSmDp();

        authenticateClientSmDp.setSmdpSigned2(Util.byteArrayToHexString(decodeBase64(authenticateClientResp.getSmdpSigned2()), ""));
        authenticateClientSmDp.setSmdpSignature2(Util.byteArrayToHexString(decodeBase64(authenticateClientResp.getSmdpSignature2()), ""));
        authenticateClientSmDp.setSmdpCertificate(Util.byteArrayToHexString(decodeBase64(authenticateClientResp.getSmdpCertificate()), ""));

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - authenticateClient returning: " + authenticateClientSmDp);
        }

        return authenticateClientSmDp;
    }

    private AuthenticateClientResp getAuthenticateClientResponse(InitialAuthenticationKeys initialAuthenticationKeys, String encodedAuthenticateServerResponse) {

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Authenticate client with SM-DP+ with initialAuthenticationKeys: " +
                    initialAuthenticationKeys +
                    " encodedAuthenticateServerResponse: " +
                    encodedAuthenticateServerResponse);
        }

        AuthenticateClientResp authenticateClientResp = es9Module.authenticateClient(initialAuthenticationKeys.getTransactionId(), encodedAuthenticateServerResponse);

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Authenticate client with SM-DP+ - Response : " + authenticateClientResp);
        }

        return authenticateClientResp;
    }

    public String authenticateWithEuicc(InitialAuthenticationKeys initialAuthenticationKeys) {

        progress.stepExecuted(DOWNLOAD_PROFILE_AUTHENTICATE_WITH_EUICC, "authenticateWithEuicc retrieving...");

        String authenticateServerResponse = apduTransmitter.transmitApdus(ApduUtils.authenticateServerApdu(initialAuthenticationKeys.getServerSigned1(),
                initialAuthenticationKeys.getServerSignature1(),
                initialAuthenticationKeys.getEuiccCiPKIdTobeUsed(), initialAuthenticationKeys.getServerCertificate(),
                initialAuthenticationKeys.getMatchingId()));
        //String encodedAuthenticateServerResponse = Base64.encodeBase64String(Util.hexStringToByteArray(authenticateServerResponse));
        if (authenticateServerResponse.endsWith("9000"))
        {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - EauthenticateServerResponse before: " + authenticateServerResponse);
            authenticateServerResponse = authenticateServerResponse.substring(0,authenticateServerResponse.length() -4);
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - EauthenticateServerResponse mod: " + authenticateServerResponse);
        }

        String encodedString = new String(Base64.encodeBase64(Util.hexStringToByteArray(authenticateServerResponse)));
        //String encodedAuthenticateServerResponse = encodedString.replace('+','-').replace('/','_');
        String encodedAuthenticateServerResponse = encodedString;

        if (LogStub.getInstance().isDebugEnabled()) {
            LogStub.getInstance().logDebug(LOG, LogStub.getInstance().getTag() + " - Authenticate server response (base64): " + encodedAuthenticateServerResponse);
        }

        progress.stepExecuted(DOWNLOAD_PROFILE_AUTHENTICATED_WITH_EUICC, "authenticateWithEuicc authenticated...");

        return encodedAuthenticateServerResponse;
    }
}
