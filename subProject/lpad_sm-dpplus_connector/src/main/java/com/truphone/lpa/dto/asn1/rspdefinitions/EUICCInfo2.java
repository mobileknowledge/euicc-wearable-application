package com.truphone.lpa.dto.asn1.rspdefinitions;

import com.truphone.lpa.dto.asn1.pkix1implicit88.SubjectKeyIdentifier;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerInteger;
import org.openmuc.jasn1.ber.types.BerOctetString;
import org.openmuc.jasn1.ber.types.string.BerUTF8String;







public class EUICCInfo2
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public EUICCInfo2() {}
  
  public static class EuiccCiPKIdListForVerification
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    public static final BerTag tag = new BerTag(0, 32, 16);
    public byte[] code = null;
    private List<SubjectKeyIdentifier> seqOf = null;
    
    public EuiccCiPKIdListForVerification() {
      seqOf = new ArrayList();
    }
    
    public EuiccCiPKIdListForVerification(byte[] code) {
      this.code = code;
    }
    
    public List<SubjectKeyIdentifier> getSubjectKeyIdentifier() {
      if (seqOf == null) {
        seqOf = new ArrayList();
      }
      return seqOf;
    }
    
    public int encode(BerByteArrayOutputStream os) throws IOException {
      return encode(os, true);
    }
    
    public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
    {
      if (code != null) {
        for (int i = code.length - 1; i >= 0; i--) {
          os.write(code[i]);
        }
        if (withTag) {
          return tag.encode(os) + code.length;
        }
        return code.length;
      }
      
      int codeLength = 0;
      for (int i = seqOf.size() - 1; i >= 0; i--) {
        codeLength += seqOf.get(i).encode(os, true);
      }
      
      codeLength += BerLength.encodeLength(os, codeLength);
      
      if (withTag) {
        codeLength += tag.encode(os);
      }
      
      return codeLength;
    }
    
    public int decode(InputStream is) throws IOException {
      return decode(is, true);
    }
    
    public int decode(InputStream is, boolean withTag) throws IOException {
      int codeLength = 0;
      int subCodeLength = 0;
      if (withTag) {
        codeLength += tag.decodeAndCheck(is);
      }
      
      BerLength length = new BerLength();
      codeLength += length.decode(is);
      int totalLength = length.val;
      
      while (subCodeLength < totalLength) {
        SubjectKeyIdentifier element = new SubjectKeyIdentifier();
        subCodeLength += element.decode(is, true);
        seqOf.add(element);
      }
      if (subCodeLength != totalLength) {
        throw new IOException("Decoded SequenceOf or SetOf has wrong length. Expected " + totalLength + " but has " + subCodeLength);
      }
      
      codeLength += subCodeLength;
      
      return codeLength;
    }
    
    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      encode(os, false);
      code = os.getArray();
    }
    
    public String toString() {
      StringBuilder sb = new StringBuilder();
      appendAsString(sb, 0);
      return sb.toString();
    }
    
    public void appendAsString(StringBuilder sb, int indentLevel)
    {
      sb.append("{\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      if (seqOf == null) {
        sb.append("null");
      }
      else {
        Iterator<SubjectKeyIdentifier> it = seqOf.iterator();
        if (it.hasNext()) {
          sb.append(it.next());
          while (it.hasNext()) {
            sb.append(",\n");
            for (int i = 0; i < indentLevel + 1; i++) {
              sb.append("\t");
            }
            sb.append(it.next());
          }
        }
      }
      
      sb.append("\n");
      for (int i = 0; i < indentLevel; i++) {
        sb.append("\t");
      }
      sb.append("}");
    }
  }
  

  public static class EuiccCiPKIdListForSigning
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    public static final BerTag tag = new BerTag(0, 32, 16);
    public byte[] code = null;
    private List<SubjectKeyIdentifier> seqOf = null;
    
    public EuiccCiPKIdListForSigning() {
      seqOf = new ArrayList();
    }
    
    public EuiccCiPKIdListForSigning(byte[] code) {
      this.code = code;
    }
    
    public List<SubjectKeyIdentifier> getSubjectKeyIdentifier() {
      if (seqOf == null) {
        seqOf = new ArrayList();
      }
      return seqOf;
    }
    
    public int encode(BerByteArrayOutputStream os) throws IOException {
      return encode(os, true);
    }
    
    public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
    {
      if (code != null) {
        for (int i = code.length - 1; i >= 0; i--) {
          os.write(code[i]);
        }
        if (withTag) {
          return tag.encode(os) + code.length;
        }
        return code.length;
      }
      
      int codeLength = 0;
      for (int i = seqOf.size() - 1; i >= 0; i--) {
        codeLength += seqOf.get(i).encode(os, true);
      }
      
      codeLength += BerLength.encodeLength(os, codeLength);
      
      if (withTag) {
        codeLength += tag.encode(os);
      }
      
      return codeLength;
    }
    
    public int decode(InputStream is) throws IOException {
      return decode(is, true);
    }
    
    public int decode(InputStream is, boolean withTag) throws IOException {
      int codeLength = 0;
      int subCodeLength = 0;
      if (withTag) {
        codeLength += tag.decodeAndCheck(is);
      }
      
      BerLength length = new BerLength();
      codeLength += length.decode(is);
      int totalLength = length.val;
      
      while (subCodeLength < totalLength) {
        SubjectKeyIdentifier element = new SubjectKeyIdentifier();
        subCodeLength += element.decode(is, true);
        seqOf.add(element);
      }
      if (subCodeLength != totalLength) {
        throw new IOException("Decoded SequenceOf or SetOf has wrong length. Expected " + totalLength + " but has " + subCodeLength);
      }
      
      codeLength += subCodeLength;
      
      return codeLength;
    }
    
    public void encodeAndSave(int encodingSizeGuess) throws IOException {
      BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
      encode(os, false);
      code = os.getArray();
    }
    
    public String toString() {
      StringBuilder sb = new StringBuilder();
      appendAsString(sb, 0);
      return sb.toString();
    }
    
    public void appendAsString(StringBuilder sb, int indentLevel)
    {
      sb.append("{\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      if (seqOf == null) {
        sb.append("null");
      }
      else {
        Iterator<SubjectKeyIdentifier> it = seqOf.iterator();
        if (it.hasNext()) {
          sb.append(it.next());
          while (it.hasNext()) {
            sb.append(",\n");
            for (int i = 0; i < indentLevel + 1; i++) {
              sb.append("\t");
            }
            sb.append(it.next());
          }
        }
      }
      
      sb.append("\n");
      for (int i = 0; i < indentLevel; i++) {
        sb.append("\t");
      }
      sb.append("}");
    }
  }
  

  public static final BerTag tag = new BerTag(128, 32, 34);
  
  public byte[] code = null;
  private VersionType profileVersion = null;
  private VersionType svn = null;
  private VersionType euiccFirmwareVer = null;
  private BerOctetString extCardResource = null;
  private UICCCapability uiccCapability = null;
  private VersionType javacardVersion = null;
  private VersionType globalplatformVersion = null;
  private RspCapability rspCapability = null;
  private EuiccCiPKIdListForVerification euiccCiPKIdListForVerification = null;
  private EuiccCiPKIdListForSigning euiccCiPKIdListForSigning = null;
  private BerInteger euiccCategory = null;
  private PprIds forbiddenProfilePolicyRules = null;
  private VersionType ppVersion = null;
  private BerUTF8String sasAcreditationNumber = null;
  private CertificationDataObject certificationDataObject = null;
  


  public EUICCInfo2(byte[] code)
  {
    this.code = code;
  }
  
  public void setProfileVersion(VersionType profileVersion) {
    this.profileVersion = profileVersion;
  }
  
  public VersionType getProfileVersion() {
    return profileVersion;
  }
  
  public void setSvn(VersionType svn) {
    this.svn = svn;
  }
  
  public VersionType getSvn() {
    return svn;
  }
  
  public void setEuiccFirmwareVer(VersionType euiccFirmwareVer) {
    this.euiccFirmwareVer = euiccFirmwareVer;
  }
  
  public VersionType getEuiccFirmwareVer() {
    return euiccFirmwareVer;
  }
  
  public void setExtCardResource(BerOctetString extCardResource) {
    this.extCardResource = extCardResource;
  }
  
  public BerOctetString getExtCardResource() {
    return extCardResource;
  }
  
  public void setUiccCapability(UICCCapability uiccCapability) {
    this.uiccCapability = uiccCapability;
  }
  
  public UICCCapability getUiccCapability() {
    return uiccCapability;
  }
  
  public void setJavacardVersion(VersionType javacardVersion) {
    this.javacardVersion = javacardVersion;
  }
  
  public VersionType getJavacardVersion() {
    return javacardVersion;
  }
  
  public void setGlobalplatformVersion(VersionType globalplatformVersion) {
    this.globalplatformVersion = globalplatformVersion;
  }
  
  public VersionType getGlobalplatformVersion() {
    return globalplatformVersion;
  }
  
  public void setRspCapability(RspCapability rspCapability) {
    this.rspCapability = rspCapability;
  }
  
  public RspCapability getRspCapability() {
    return rspCapability;
  }
  
  public void setEuiccCiPKIdListForVerification(EuiccCiPKIdListForVerification euiccCiPKIdListForVerification) {
    this.euiccCiPKIdListForVerification = euiccCiPKIdListForVerification;
  }
  
  public EuiccCiPKIdListForVerification getEuiccCiPKIdListForVerification() {
    return euiccCiPKIdListForVerification;
  }
  
  public void setEuiccCiPKIdListForSigning(EuiccCiPKIdListForSigning euiccCiPKIdListForSigning) {
    this.euiccCiPKIdListForSigning = euiccCiPKIdListForSigning;
  }
  
  public EuiccCiPKIdListForSigning getEuiccCiPKIdListForSigning() {
    return euiccCiPKIdListForSigning;
  }
  
  public void setEuiccCategory(BerInteger euiccCategory) {
    this.euiccCategory = euiccCategory;
  }
  
  public BerInteger getEuiccCategory() {
    return euiccCategory;
  }
  
  public void setForbiddenProfilePolicyRules(PprIds forbiddenProfilePolicyRules) {
    this.forbiddenProfilePolicyRules = forbiddenProfilePolicyRules;
  }
  
  public PprIds getForbiddenProfilePolicyRules() {
    return forbiddenProfilePolicyRules;
  }
  
  public void setPpVersion(VersionType ppVersion) {
    this.ppVersion = ppVersion;
  }
  
  public VersionType getPpVersion() {
    return ppVersion;
  }
  
  public void setSasAcreditationNumber(BerUTF8String sasAcreditationNumber) {
    this.sasAcreditationNumber = sasAcreditationNumber;
  }
  
  public BerUTF8String getSasAcreditationNumber() {
    return sasAcreditationNumber;
  }
  
  public void setCertificationDataObject(CertificationDataObject certificationDataObject) {
    this.certificationDataObject = certificationDataObject;
  }
  
  public CertificationDataObject getCertificationDataObject() {
    return certificationDataObject;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (certificationDataObject != null) {
      codeLength += certificationDataObject.encode(os, false);
      
      os.write(172);
      codeLength++;
    }
    
    codeLength += sasAcreditationNumber.encode(os, true);
    
    codeLength += ppVersion.encode(os, true);
    
    if (forbiddenProfilePolicyRules != null) {
      codeLength += forbiddenProfilePolicyRules.encode(os, false);
      
      os.write(153);
      codeLength++;
    }
    
    if (euiccCategory != null) {
      codeLength += euiccCategory.encode(os, false);
      
      os.write(139);
      codeLength++;
    }
    
    codeLength += euiccCiPKIdListForSigning.encode(os, false);
    
    os.write(170);
    codeLength++;
    
    codeLength += euiccCiPKIdListForVerification.encode(os, false);
    
    os.write(169);
    codeLength++;
    
    codeLength += rspCapability.encode(os, false);
    
    os.write(136);
    codeLength++;
    
    if (globalplatformVersion != null) {
      codeLength += globalplatformVersion.encode(os, false);
      
      os.write(135);
      codeLength++;
    }
    
    if (javacardVersion != null) {
      codeLength += javacardVersion.encode(os, false);
      
      os.write(134);
      codeLength++;
    }
    
    codeLength += uiccCapability.encode(os, false);
    
    os.write(133);
    codeLength++;
    
    codeLength += extCardResource.encode(os, false);
    
    os.write(132);
    codeLength++;
    
    codeLength += euiccFirmwareVer.encode(os, false);
    
    os.write(131);
    codeLength++;
    
    codeLength += svn.encode(os, false);
    
    os.write(130);
    codeLength++;
    
    codeLength += profileVersion.encode(os, false);
    
    os.write(129);
    codeLength++;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 1)) {
      profileVersion = new VersionType();
      subCodeLength += profileVersion.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 2)) {
      svn = new VersionType();
      subCodeLength += svn.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 3)) {
      euiccFirmwareVer = new VersionType();
      subCodeLength += euiccFirmwareVer.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 4)) {
      extCardResource = new BerOctetString();
      subCodeLength += extCardResource.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 5)) {
      uiccCapability = new UICCCapability();
      subCodeLength += uiccCapability.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 6)) {
      javacardVersion = new VersionType();
      subCodeLength += javacardVersion.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 7)) {
      globalplatformVersion = new VersionType();
      subCodeLength += globalplatformVersion.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 8)) {
      rspCapability = new RspCapability();
      subCodeLength += rspCapability.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 32, 9)) {
      euiccCiPKIdListForVerification = new EuiccCiPKIdListForVerification();
      subCodeLength += euiccCiPKIdListForVerification.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 32, 10)) {
      euiccCiPKIdListForSigning = new EuiccCiPKIdListForSigning();
      subCodeLength += euiccCiPKIdListForSigning.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 11)) {
      euiccCategory = new BerInteger();
      subCodeLength += euiccCategory.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(128, 0, 25)) {
      forbiddenProfilePolicyRules = new PprIds();
      subCodeLength += forbiddenProfilePolicyRules.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    
    if (berTag.equals(VersionType.tag)) {
      ppVersion = new VersionType();
      subCodeLength += ppVersion.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(BerUTF8String.tag)) {
      sasAcreditationNumber = new BerUTF8String();
      subCodeLength += sasAcreditationNumber.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 32, 12)) {
      certificationDataObject = new CertificationDataObject();
      subCodeLength += certificationDataObject.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (profileVersion != null) {
      sb.append("profileVersion: ").append(profileVersion);
    }
    else {
      sb.append("profileVersion: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (svn != null) {
      sb.append("svn: ").append(svn);
    }
    else {
      sb.append("svn: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccFirmwareVer != null) {
      sb.append("euiccFirmwareVer: ").append(euiccFirmwareVer);
    }
    else {
      sb.append("euiccFirmwareVer: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (extCardResource != null) {
      sb.append("extCardResource: ").append(extCardResource);
    }
    else {
      sb.append("extCardResource: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (uiccCapability != null) {
      sb.append("uiccCapability: ").append(uiccCapability);
    }
    else {
      sb.append("uiccCapability: <empty-required-field>");
    }
    
    if (javacardVersion != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("javacardVersion: ").append(javacardVersion);
    }
    
    if (globalplatformVersion != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("globalplatformVersion: ").append(globalplatformVersion);
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (rspCapability != null) {
      sb.append("rspCapability: ").append(rspCapability);
    }
    else {
      sb.append("rspCapability: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccCiPKIdListForVerification != null) {
      sb.append("euiccCiPKIdListForVerification: ");
      euiccCiPKIdListForVerification.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("euiccCiPKIdListForVerification: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (euiccCiPKIdListForSigning != null) {
      sb.append("euiccCiPKIdListForSigning: ");
      euiccCiPKIdListForSigning.appendAsString(sb, indentLevel + 1);
    }
    else {
      sb.append("euiccCiPKIdListForSigning: <empty-required-field>");
    }
    
    if (euiccCategory != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("euiccCategory: ").append(euiccCategory);
    }
    
    if (forbiddenProfilePolicyRules != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("forbiddenProfilePolicyRules: ").append(forbiddenProfilePolicyRules);
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (ppVersion != null) {
      sb.append("ppVersion: ").append(ppVersion);
    }
    else {
      sb.append("ppVersion: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (sasAcreditationNumber != null) {
      sb.append("sasAcreditationNumber: ").append(sasAcreditationNumber);
    }
    else {
      sb.append("sasAcreditationNumber: <empty-required-field>");
    }
    
    if (certificationDataObject != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("certificationDataObject: ");
      certificationDataObject.appendAsString(sb, indentLevel + 1);
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
