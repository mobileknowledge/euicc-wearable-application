package com.truphone.lpa.dto.asn1.pkix1implicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.string.BerBMPString;
import org.openmuc.jasn1.ber.types.string.BerIA5String;
import org.openmuc.jasn1.ber.types.string.BerUTF8String;
import org.openmuc.jasn1.ber.types.string.BerVisibleString;















public class DisplayText
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public byte[] code = null;
  private BerIA5String ia5String = null;
  private BerVisibleString visibleString = null;
  private BerBMPString bmpString = null;
  private BerUTF8String utf8String = null;
  
  public DisplayText() {}
  
  public DisplayText(byte[] code)
  {
    this.code = code;
  }
  
  public void setIa5String(BerIA5String ia5String) {
    this.ia5String = ia5String;
  }
  
  public BerIA5String getIa5String() {
    return ia5String;
  }
  
  public void setVisibleString(BerVisibleString visibleString) {
    this.visibleString = visibleString;
  }
  
  public BerVisibleString getVisibleString() {
    return visibleString;
  }
  
  public void setBmpString(BerBMPString bmpString) {
    this.bmpString = bmpString;
  }
  
  public BerBMPString getBmpString() {
    return bmpString;
  }
  
  public void setUtf8String(BerUTF8String utf8String) {
    this.utf8String = utf8String;
  }
  
  public BerUTF8String getUtf8String() {
    return utf8String;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (utf8String != null) {
      codeLength += utf8String.encode(os, true);
      return codeLength;
    }
    
    if (bmpString != null) {
      codeLength += bmpString.encode(os, true);
      return codeLength;
    }
    
    if (visibleString != null) {
      codeLength += visibleString.encode(os, true);
      return codeLength;
    }
    
    if (ia5String != null) {
      codeLength += ia5String.encode(os, true);
      return codeLength;
    }
    
    throw new IOException("Error encoding CHOICE: No element of CHOICE was selected.");
  }
  
  public int decode(InputStream is) throws IOException {
    return decode(is, null);
  }
  
  public int decode(InputStream is, BerTag berTag) throws IOException
  {
    int codeLength = 0;
    BerTag passedTag = berTag;
    
    if (berTag == null) {
      berTag = new BerTag();
      codeLength += berTag.decode(is);
    }
    
    if (berTag.equals(BerIA5String.tag)) {
      ia5String = new BerIA5String();
      codeLength += ia5String.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(BerVisibleString.tag)) {
      visibleString = new BerVisibleString();
      codeLength += visibleString.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(BerBMPString.tag)) {
      bmpString = new BerBMPString();
      codeLength += bmpString.decode(is, false);
      return codeLength;
    }
    
    if (berTag.equals(BerUTF8String.tag)) {
      utf8String = new BerUTF8String();
      codeLength += utf8String.decode(is, false);
      return codeLength;
    }
    
    if (passedTag != null) {
      return 0;
    }
    
    throw new IOException("Error decoding CHOICE: Tag " + berTag + " matched to no item.");
  }
  
  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    if (ia5String != null) {
      sb.append("ia5String: ").append(ia5String);
      return;
    }
    
    if (visibleString != null) {
      sb.append("visibleString: ").append(visibleString);
      return;
    }
    
    if (bmpString != null) {
      sb.append("bmpString: ").append(bmpString);
      return;
    }
    
    if (utf8String != null) {
      sb.append("utf8String: ").append(utf8String);
      return;
    }
    
    sb.append("<none>");
  }
}
