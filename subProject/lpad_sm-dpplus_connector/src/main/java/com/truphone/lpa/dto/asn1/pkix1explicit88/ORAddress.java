//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;

public class ORAddress implements Serializable {
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  public byte[] code = null;
  private BuiltInStandardAttributes builtInStandardAttributes = null;
  private BuiltInDomainDefinedAttributes builtInDomainDefinedAttributes = null;
  private ExtensionAttributes extensionAttributes = null;

  public ORAddress() {
  }

  public ORAddress(byte[] code) {
    this.code = code;
  }

  public void setBuiltInStandardAttributes(BuiltInStandardAttributes builtInStandardAttributes) {
    this.builtInStandardAttributes = builtInStandardAttributes;
  }

  public BuiltInStandardAttributes getBuiltInStandardAttributes() {
    return this.builtInStandardAttributes;
  }

  public void setBuiltInDomainDefinedAttributes(BuiltInDomainDefinedAttributes builtInDomainDefinedAttributes) {
    this.builtInDomainDefinedAttributes = builtInDomainDefinedAttributes;
  }

  public BuiltInDomainDefinedAttributes getBuiltInDomainDefinedAttributes() {
    return this.builtInDomainDefinedAttributes;
  }

  public void setExtensionAttributes(ExtensionAttributes extensionAttributes) {
    this.extensionAttributes = extensionAttributes;
  }

  public ExtensionAttributes getExtensionAttributes() {
    return this.extensionAttributes;
  }

  public int encode(BerByteArrayOutputStream os) throws IOException {
    return this.encode(os, true);
  }

  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException {
    int codeLength;
    if (this.code == null) {
      codeLength = 0;
      if (this.extensionAttributes != null) {
        codeLength += this.extensionAttributes.encode(os, true);
      }

      if (this.builtInDomainDefinedAttributes != null) {
        codeLength += this.builtInDomainDefinedAttributes.encode(os, true);
      }

      codeLength += this.builtInStandardAttributes.encode(os, true);
      codeLength += BerLength.encodeLength(os, codeLength);
      if (withTag) {
        codeLength += tag.encode(os);
      }

      return codeLength;
    } else {
      for(codeLength = this.code.length - 1; codeLength >= 0; --codeLength) {
        os.write(this.code[codeLength]);
      }

      return withTag ? tag.encode(os) + this.code.length : this.code.length;
    }
  }

  public int decode(InputStream is) throws IOException {
    return this.decode(is, true);
  }

  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }

    BerLength length = new BerLength();
    codeLength += length.decode(is);
    int totalLength = length.val;
    codeLength += totalLength;
    subCodeLength = subCodeLength + berTag.decode(is);
    if (berTag.equals(BuiltInStandardAttributes.tag)) {
      this.builtInStandardAttributes = new BuiltInStandardAttributes();
      subCodeLength += this.builtInStandardAttributes.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      } else {
        subCodeLength += berTag.decode(is);
        if (berTag.equals(BuiltInDomainDefinedAttributes.tag)) {
          this.builtInDomainDefinedAttributes = new BuiltInDomainDefinedAttributes();
          subCodeLength += this.builtInDomainDefinedAttributes.decode(is, false);
          if (subCodeLength == totalLength) {
            return codeLength;
          }

          subCodeLength += berTag.decode(is);
        }

        if (berTag.equals(ExtensionAttributes.tag)) {
          this.extensionAttributes = new ExtensionAttributes();
          subCodeLength += this.extensionAttributes.decode(is, false);
          if (subCodeLength == totalLength) {
            return codeLength;
          }
        }

        throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
      }
    } else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
  }

  public void encodeAndSave(int encodingSizeGuess) throws IOException {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    this.encode(os, false);
    this.code = os.getArray();
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    this.appendAsString(sb, 0);
    return sb.toString();
  }

  public void appendAsString(StringBuilder sb, int indentLevel) {
    sb.append("{");
    sb.append("\n");

    int i;
    for(i = 0; i < indentLevel + 1; ++i) {
      sb.append("\t");
    }

    if (this.builtInStandardAttributes != null) {
      sb.append("builtInStandardAttributes: ");
      this.builtInStandardAttributes.appendAsString(sb, indentLevel + 1);
    } else {
      sb.append("builtInStandardAttributes: <empty-required-field>");
    }

    if (this.builtInDomainDefinedAttributes != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("builtInDomainDefinedAttributes: ");
      this.builtInDomainDefinedAttributes.appendAsString(sb, indentLevel + 1);
    }

    if (this.extensionAttributes != null) {
      sb.append(",\n");

      for(i = 0; i < indentLevel + 1; ++i) {
        sb.append("\t");
      }

      sb.append("extensionAttributes: ");
      this.extensionAttributes.appendAsString(sb, indentLevel + 1);
    }

    sb.append("\n");

    for(i = 0; i < indentLevel; ++i) {
      sb.append("\t");
    }

    sb.append("}");
  }
}
