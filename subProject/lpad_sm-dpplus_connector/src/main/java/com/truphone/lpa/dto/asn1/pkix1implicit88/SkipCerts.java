package com.truphone.lpa.dto.asn1.pkix1implicit88;

import java.math.BigInteger;
import org.openmuc.jasn1.ber.types.BerInteger;





















public class SkipCerts
  extends BerInteger
{
  private static final long serialVersionUID = 1L;
  
  public SkipCerts() {}
  
  public SkipCerts(byte[] code)
  {
    super(code);
  }
  
  public SkipCerts(BigInteger value) {
    super(value);
  }
  
  public SkipCerts(long value) {
    super(value);
  }
}
