package com.truphone.lpa.dto.asn1.rspdefinitions;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.types.BerOctetString;















public class ErrorResult
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public static final BerTag tag = new BerTag(0, 32, 16);
  
  public byte[] code = null;
  private BppCommandId bppCommandId = null;
  private ErrorReason errorReason = null;
  private BerOctetString simaResponse = null;
  
  public ErrorResult() {}
  
  public ErrorResult(byte[] code)
  {
    this.code = code;
  }
  
  public void setBppCommandId(BppCommandId bppCommandId) {
    this.bppCommandId = bppCommandId;
  }
  
  public BppCommandId getBppCommandId() {
    return bppCommandId;
  }
  
  public void setErrorReason(ErrorReason errorReason) {
    this.errorReason = errorReason;
  }
  
  public ErrorReason getErrorReason() {
    return errorReason;
  }
  
  public void setSimaResponse(BerOctetString simaResponse) {
    this.simaResponse = simaResponse;
  }
  
  public BerOctetString getSimaResponse() {
    return simaResponse;
  }
  
  public int encode(BerByteArrayOutputStream os) throws IOException {
    return encode(os, true);
  }
  
  public int encode(BerByteArrayOutputStream os, boolean withTag) throws IOException
  {
    if (code != null) {
      for (int i = code.length - 1; i >= 0; i--) {
        os.write(code[i]);
      }
      if (withTag) {
        return tag.encode(os) + code.length;
      }
      return code.length;
    }
    
    int codeLength = 0;
    if (simaResponse != null) {
      codeLength += simaResponse.encode(os, false);
      
      os.write(130);
      codeLength++;
    }
    
    codeLength += errorReason.encode(os, false);
    
    os.write(129);
    codeLength++;
    
    codeLength += bppCommandId.encode(os, false);
    
    os.write(128);
    codeLength++;
    
    codeLength += BerLength.encodeLength(os, codeLength);
    
    if (withTag) {
      codeLength += tag.encode(os);
    }
    
    return codeLength;
  }
  
  public int decode(InputStream is) throws IOException
  {
    return decode(is, true);
  }
  
  public int decode(InputStream is, boolean withTag) throws IOException {
    int codeLength = 0;
    int subCodeLength = 0;
    BerTag berTag = new BerTag();
    
    if (withTag) {
      codeLength += tag.decodeAndCheck(is);
    }
    
    BerLength length = new BerLength();
    codeLength += length.decode(is);
    
    int totalLength = length.val;
    codeLength += totalLength;
    
    subCodeLength += berTag.decode(is);
    if (berTag.equals(128, 0, 0)) {
      bppCommandId = new BppCommandId();
      subCodeLength += bppCommandId.decode(is, false);
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 1)) {
      errorReason = new ErrorReason();
      subCodeLength += errorReason.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
      subCodeLength += berTag.decode(is);
    }
    else {
      throw new IOException("Tag does not match the mandatory sequence element tag.");
    }
    
    if (berTag.equals(128, 0, 2)) {
      simaResponse = new BerOctetString();
      subCodeLength += simaResponse.decode(is, false);
      if (subCodeLength == totalLength) {
        return codeLength;
      }
    }
    throw new IOException("Unexpected end of sequence, length tag: " + totalLength + ", actual sequence length: " + subCodeLength);
  }
  
  public void encodeAndSave(int encodingSizeGuess)
    throws IOException
  {
    BerByteArrayOutputStream os = new BerByteArrayOutputStream(encodingSizeGuess);
    encode(os, false);
    code = os.getArray();
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendAsString(sb, 0);
    return sb.toString();
  }
  
  public void appendAsString(StringBuilder sb, int indentLevel)
  {
    sb.append("{");
    sb.append("\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (bppCommandId != null) {
      sb.append("bppCommandId: ").append(bppCommandId);
    }
    else {
      sb.append("bppCommandId: <empty-required-field>");
    }
    
    sb.append(",\n");
    for (int i = 0; i < indentLevel + 1; i++) {
      sb.append("\t");
    }
    if (errorReason != null) {
      sb.append("errorReason: ").append(errorReason);
    }
    else {
      sb.append("errorReason: <empty-required-field>");
    }
    
    if (simaResponse != null) {
      sb.append(",\n");
      for (int i = 0; i < indentLevel + 1; i++) {
        sb.append("\t");
      }
      sb.append("simaResponse: ").append(simaResponse);
    }
    
    sb.append("\n");
    for (int i = 0; i < indentLevel; i++) {
      sb.append("\t");
    }
    sb.append("}");
  }
}
