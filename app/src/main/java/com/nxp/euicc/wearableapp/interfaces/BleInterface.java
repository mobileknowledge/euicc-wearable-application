package com.nxp.euicc.wearableapp.interfaces;

public interface BleInterface {
    /// Closes the BLE port connection, sets the isConnected property to false, and disposes of the internal Stream object.
    void close();

    /// Starts LE advertising for other devices to discover this device.
    void startLeAdvertising();

    /// Starts LE Server.
    void startLeServer();

    /// Stops LE advertising for other devices to discover this device.
    void stopLeAdvertising();

    /// Stops LE server
    void stopLeServer();

    /// Closes the BLE port connection, sets the isConnected property to false, and disposes of the internal Stream object.
//    void close();

    /// Gets a value indicating the enabled or disabled status of the BLE port.
    /// Returns the Boolean value indicating enabled or disabled status of the BLE port.
    boolean isEnabled();
}
