package com.nxp.euicc.wearableapp.actitivies;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.nxp.euicc.adapters.ProfileAdapter;
import com.nxp.euicc.models.Profile;
import com.nxp.euicc.wearableapp.R;
import com.nxp.euicc.wearableapp.bluetooth.BluetoothServer;
import com.nxp.euicc.wearableapp.bluetooth.BluetoothTLV;
import com.nxp.euicc.wearableapp.eUICC.LPAImpl;
import com.nxp.euicc.wearableapp.utils.Utils;
import com.nxp.euicc.wearableapp.utils.WrapContentHeightViewPager;
import com.viewpagerindicator.LinePageIndicator;

import org.simalliance.openmobileapi.SEService;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.SerializationUtils.serialize;


public class MainActivity extends WearableActivity implements SEService.CallBack, BluetoothServer.BluetoothProcessCommandListener, ProfileAdapter.ProfileItemListener {

    public String TAG = MainActivity.class.getName();

    public static final byte SUCCESS = (byte) 0x00;
    public static final byte FAILED = (byte) 0xFF;

    private static final int REQUEST_CODE_PERMISSION = 0x01;
    private static final int REQUEST_ENABLE_BT = 0x02;
    private static final int REQUEST_SETTINGS_PROFILE = 0x03;

    private List<Profile> profileList;
    private String confirmationCode = null;

    private ImageView infoIcon;
    private ImageView bluetoothIcon;

    private TextView emptyProfileText;
    private RelativeLayout viewPagerLayout;
    private ProfileAdapter profileAdapter;
    private LinePageIndicator viewPagerLinePageIndicator;
    private WrapContentHeightViewPager viewPager;

    private BluetoothServer mBluetoothServer;
    private LPAImpl mLPAImpl;
    private SEService seService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        checkPermissions();
        getConnectivityStatus();
        initBle();

        // Connect to eUICC if required OMAPI permission has already been granted
        if (ActivityCompat.checkSelfPermission(this, "org.simalliance.openmobileapi.SMARTCARD") == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Proceed to set OMAPI SEService connection");
            seService = new SEService(getApplicationContext(), this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Stop advertising
        if (mBluetoothServer != null) {
            mBluetoothServer.close();
        }

        // Release SEService resources
        if (mLPAImpl != null) {
            mLPAImpl.close();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    Log.d(TAG, "Bluetooth enabled... starting advertising");
                    mBluetoothServer.startLeAdvertising();
                    mBluetoothServer.startLeServer();

                    // Update BLE icon
                    bluetoothIcon.setImageResource(R.drawable.ble_on);
                } else {
                    Utils.showDialog(MainActivity.this,
                            getString(R.string.app_name),
                            getString(R.string.dialog_bluetooth_disabled_error),
                            getString(R.string.dialog_accept),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing
                                }
                            });
                }

                break;

            case REQUEST_SETTINGS_PROFILE:

                if (resultCode == Activity.RESULT_OK) {
                    if (mLPAImpl != null && mLPAImpl.isConnected()) {
                        final ProgressDialog pd = Utils.showProgressDialog(MainActivity.this,
                                getString(R.string.app_name),
                                getString(R.string.progress_dialog_retrieve_profiles_list));

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Log.i(TAG, "Retrive Profile List from eUICC");
                                profileList = mLPAImpl.getProfilesList();
                                if (profileList == null) {
                                    profileList = new ArrayList<>();
                                }

                                pd.dismiss();

                                // Update UI on UI Thread
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        updateUI();
                                    }
                                });
                            }
                        }).start();
                    }
                }
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           @NonNull final String[] permissions,
                                           @NonNull final int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CODE_PERMISSION:
                if (grantResults.length > 0) {
                    if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        Utils.showDialog(MainActivity.this,
                                getString(R.string.app_name),
                                getString(R.string.dialog_permission_error),
                                getString(R.string.dialog_accept),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Do nothing
                                    }
                                });
                    } else {
                        if (ActivityCompat.checkSelfPermission(this, "org.simalliance.openmobileapi.SMARTCARD") == PackageManager.PERMISSION_GRANTED) {
                            seService = new SEService(getApplicationContext(), this);
                        }
                    }
                }
        }
    }

    private void checkPermissions() {
        ArrayList<String> permissionList = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, "org.simalliance.openmobileapi.SMARTCARD") != PackageManager.PERMISSION_GRANTED) {
            permissionList.add("org.simalliance.openmobileapi.SMARTCARD");
        }

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }

        String[] permissionArray = permissionList.toArray(new String[0]);

        if (permissionArray.length > 0) {
            ActivityCompat.requestPermissions(MainActivity.this, permissionArray, REQUEST_CODE_PERMISSION);
        }
    }

    private void getConnectivityStatus() {
        boolean isConnected = Utils.isNetworkConnected(getApplicationContext());
        Log.d(TAG, "Device network connection status: " + isConnected);

        if (!isConnected) {
            new AlertDialog.Builder(this)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    })
                    .setTitle("No Internet connection!")
                    .setMessage("There is no Internet connection. This app may not work.")
                    .create()
                    .show();
        }
    }

    private void initViews() {

        // Enables Always-on
        setAmbientEnabled();

        viewPagerLayout = findViewById(R.id.viewpagerLayout);
        viewPagerLinePageIndicator = findViewById(R.id.viewpagerindicator);
        viewPager = findViewById(R.id.viewpager);
        emptyProfileText = findViewById(R.id.profile_empty);
        bluetoothIcon = findViewById(R.id.bluetooth_icon);
        infoIcon = findViewById(R.id.info_icon);

        infoIcon.setOnClickListener(view -> {
            Utils.showDialog(MainActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.dialog_app_version),
                    getString(R.string.dialog_accept),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                        }
                    });
        });
    }

    private void updateUI() {

        try {
            if (profileList != null && !profileList.isEmpty()) {
                profileAdapter = new ProfileAdapter(MainActivity.this, profileList, this);

                viewPager.setAdapter(profileAdapter);
                viewPagerLinePageIndicator.setViewPager(viewPager, 0);
                viewPagerLinePageIndicator.notifyDataSetChanged();

                emptyProfileText.setVisibility(View.GONE);
                viewPagerLinePageIndicator.setVisibility(View.VISIBLE);
                viewPager.setVisibility(View.VISIBLE);
                viewPagerLayout.setVisibility(View.VISIBLE);
            } else {
                emptyProfileText.setVisibility(View.VISIBLE);
                viewPagerLinePageIndicator.setVisibility(View.GONE);
                viewPager.setVisibility(View.GONE);
                viewPagerLayout.setVisibility(View.GONE);
            }
        } catch (final Exception e) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Utils.showToast(MainActivity.this, "Update UI, Exception: " + e.getMessage());
                }
            });
        }
    }

    private void initBle() {
        Log.d("BluetoothServer", "Activity initBle create server");
        mBluetoothServer = new BluetoothServer(getApplicationContext(), this);
        if (mBluetoothServer.isEnabled()) {
            Log.d(TAG, "Bluetooth enabled... starting advertising");
            mBluetoothServer.startLeAdvertising();
            mBluetoothServer.startLeServer();

            // Update BLE icon
            bluetoothIcon.setImageResource(R.drawable.ble_on);
        } else {
            Log.d(TAG, "Bluetooth is currently disabled... enabling");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

            // Update BLE icon
            bluetoothIcon.setImageResource(R.drawable.ble_off);
        }
    }

    private void sendResponseToCompanion(byte tag, byte[] status) {
        byte[] statusData = BluetoothTLV.getTlvCommand(tag, status);
        mBluetoothServer.transmit(statusData);
    }

    public void serviceConnected(SEService service) {
        seService = service;

        Log.i(TAG, "SEService version " + service.getVersion());
        Log.i(TAG, "Create LPA Implementation helper");
        mLPAImpl = LPAImpl.getInstance(getApplicationContext(), seService);

        final ProgressDialog pd = Utils.showProgressDialog(MainActivity.this,
                getString(R.string.app_name),
                getString(R.string.progress_dialog_initialize));

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "Connect and initialize LPA Implementation");
                mLPAImpl.connect();

                if (mLPAImpl.isConnected()) {
                    profileList = mLPAImpl.getProfilesList();
                    if (profileList == null) {
                        profileList = new ArrayList<>();
                    }

                    pd.dismiss();

                    // Update UI on UI Thread
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateUI();
                        }
                    });
                } else {
                    pd.dismiss();

                    // Update UI on UI Thread
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.showDialog(MainActivity.this,
                                    getString(R.string.app_name),
                                    getString(R.string.dialog_uicc_error),
                                    getString(R.string.dialog_accept),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Do nothing
                                        }
                                    });
                        }
                    });
                }
            }
        }).start();
    }

    protected void startProfileDownload(String qrData) {

        final byte[] status = {FAILED};
        Log.d(TAG, "Initiate Profile Download ");
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Sample QR code data format is ::  1$<SM-DP+-ADDRESS>$<Activation-Code>$<SM-DP+-OID>$<ConfirmationCode-Required-Flag>
        String[] scannedData = qrData.split("\\$");
        final String smdpAddress = scannedData[1];
        final String activationCode = scannedData[2];
        String ccFlag = null;
        if (scannedData.length > 4) {
            ccFlag = scannedData[4];
        }

        Log.d(TAG, "smdpAddress " + smdpAddress);
        Log.d(TAG, "activationCode " + activationCode);

        final ProgressDialog pd = Utils.showProgressDialog(MainActivity.this,
                getString(R.string.app_name),
                getString(R.string.progress_dialog_download_profile));

        final Thread profileDownload = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Proceed with profileDownload");
                String status = mLPAImpl.downloadProfile(smdpAddress, activationCode, confirmationCode);
                pd.dismiss();
                Log.v(TAG, "Profile Downloaded Successfully");

                byte[] buffer = Utils.hexStringToByteArray(Utils.toHex(status, status.length()));
                sendResponseToCompanion(BluetoothTLV.PROFILE_DOWNLOAD, buffer);

                profileList = mLPAImpl.getProfilesList();
                if (profileList == null) {
                    profileList = new ArrayList<>();
                }

                // Update UI on UI Thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateUI();
                    }
                });
            }
        });

        if (ccFlag != null && ccFlag.equals("1")) {
            final EditText editText = new EditText(MainActivity.this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            editText.setLayoutParams(lp);

            Utils.showEditDialog(MainActivity.this,
                    editText,
                    getString(R.string.dialog_enter_conf_code_title),
                    getString(R.string.dialog_enter_conf_code_message),
                    getString(R.string.dialog_cancel),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            pd.dismiss();
                            Log.v(TAG, "No confirmation code entered");
                        }
                    },
                    getString(R.string.dialog_accept),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            confirmationCode = editText.getText().toString();
                            profileDownload.start();
                        }
                    });
        } else {
            confirmationCode = null;
            profileDownload.start();
        }
    }

    @Override
    public void onProcessCommand(byte[] command) {
        Log.d(TAG, "Received command to process: " + Utils.byteArrayToHex(command));

        int tag = command[0];
        int lenSize = BluetoothTLV.getLengthSize(command);
        final byte[] value = new byte[BluetoothTLV.getLength(command)];

        switch (tag) {
            case BluetoothTLV.PROFILE_DOWNLOAD: {
                System.arraycopy(command, 1 + lenSize, value, 0, BluetoothTLV.getLength(command));
                String key = new String(value, StandardCharsets.UTF_8);
                Log.d(TAG, "Key : " + key);

                if (mLPAImpl.isConnected()) {
                    startProfileDownload(key);
                } else {
                    byte[] errorBuffer = new byte[]{FAILED};
                    sendResponseToCompanion(BluetoothTLV.PROFILE_DOWNLOAD, errorBuffer);
                }

                break;
            }

            case BluetoothTLV.GET_EID: {

                if (mLPAImpl.isConnected()) {
                    final ProgressDialog pd = Utils.showProgressDialog(MainActivity.this,
                            getString(R.string.app_name),
                            getString(R.string.progress_dialog_retrieve_eid));

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String eid = mLPAImpl.getEID();
                            byte[] buffer = Utils.hexStringToByteArray(Utils.toHex(eid, eid.length()));
                            sendResponseToCompanion(BluetoothTLV.GET_EID, buffer);
                            pd.dismiss();
                        }
                    }).start();
                } else {
                    byte[] errorBuffer = new byte[]{FAILED};
                    sendResponseToCompanion(BluetoothTLV.GET_EID, errorBuffer);
                }

                break;
            }

            case BluetoothTLV.GET_PROFILE_LIST: {

                if (mLPAImpl.isConnected()) {
                    final ProgressDialog pd = Utils.showProgressDialog(MainActivity.this,
                            getString(R.string.app_name),
                            getString(R.string.progress_dialog_retrieve_profiles_list));

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            byte[] serialList = new byte[1];
                            profileList = mLPAImpl.getProfilesList();
                            if (profileList != null && profileList.size() != 0) {
                                Object[] listArray = profileList.toArray();
                                serialList = serialize(listArray);
                            } else {
                                serialList[0] = SUCCESS;
                                profileList = new ArrayList<>();
                            }

                            sendResponseToCompanion(BluetoothTLV.GET_PROFILE_LIST, serialList);
                            pd.dismiss();
                        }
                    }).start();
                } else {
                    byte[] errorBuffer = new byte[]{FAILED};
                    sendResponseToCompanion(BluetoothTLV.GET_PROFILE_LIST, errorBuffer);
                }

                break;
            }

            case BluetoothTLV.SET_PROFILE_NICKNAME: {
                System.arraycopy(command, 1 + lenSize, value, 0, BluetoothTLV.getLength(command));
                Log.d(TAG, "value : " + Utils.byteArrayToHex(value));

                int iccidLen = value[1];
                int nicknameLen = value[3 + iccidLen];
                byte[] valueIccid = new byte[iccidLen];
                byte[] valueNickname = new byte[nicknameLen];
                System.arraycopy(value, 2, valueIccid, 0, iccidLen);
                System.arraycopy(value, 4 + iccidLen, valueNickname, 0, nicknameLen);
                String iccid = new String(valueIccid, StandardCharsets.UTF_8);
                String nickName = new String(valueNickname, StandardCharsets.UTF_8);
                Log.d(TAG, "iccid : " + iccid);
                Log.d(TAG, "value : " + value);

                if (mLPAImpl.isConnected()) {
                    final ProgressDialog pd = Utils.showProgressDialog(MainActivity.this,
                            getString(R.string.app_name),
                            getString(R.string.progress_dialog_edit_profile));

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String setProfileNickNameStatus = mLPAImpl.setProfileNickName(iccid, Utils.toHex(nickName, nickName.length()));
                            pd.dismiss();

                            byte[] status = Utils.hexStringToByteArray(Utils.toHex(setProfileNickNameStatus, setProfileNickNameStatus.length()));
                            sendResponseToCompanion(BluetoothTLV.SET_PROFILE_NICKNAME, status);

                            profileList = mLPAImpl.getProfilesList();
                            if (profileList == null) {
                                profileList = new ArrayList<>();
                            }

                            // Update UI on UI Thread
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateUI();
                                }
                            });
                        }
                    }).start();
                } else {
                    byte[] errorBuffer = new byte[]{FAILED};
                    sendResponseToCompanion(BluetoothTLV.GET_PROFILE_LIST, errorBuffer);
                }

                break;
            }

            case BluetoothTLV.PROFILE_ENABLE: {
                System.arraycopy(command, 1 + lenSize, value, 0, BluetoothTLV.getLength(command));
                if (mLPAImpl.isConnected()) {
                    final ProgressDialog pd = Utils.showProgressDialog(MainActivity.this,
                            getString(R.string.app_name),
                            getString(R.string.progress_dialog_enable_profile));

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String iccid = new String(value, StandardCharsets.UTF_8);
                            String status = mLPAImpl.enableProfile(iccid);
                            pd.dismiss();

                            byte[] buffer = Utils.hexStringToByteArray(Utils.toHex(status, status.length()));
                            sendResponseToCompanion(BluetoothTLV.PROFILE_ENABLE, buffer);

                            profileList = mLPAImpl.getProfilesList();
                            if (profileList == null) {
                                profileList = new ArrayList<>();
                            }

                            // Update UI on UI Thread
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateUI();
                                }
                            });
                        }
                    }).start();
                } else {
                    byte[] errorBuffer = new byte[]{FAILED};
                    sendResponseToCompanion(BluetoothTLV.PROFILE_ENABLE, errorBuffer);
                }

                break;
            }

            case BluetoothTLV.PROFILE_DISABLE: {
                System.arraycopy(command, 1 + lenSize, value, 0, BluetoothTLV.getLength(command));
                if (mLPAImpl.isConnected()) {
                    final ProgressDialog pd = Utils.showProgressDialog(MainActivity.this,
                            getString(R.string.app_name),
                            getString(R.string.progress_dialog_disable_profile));

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String iccid = new String(value, StandardCharsets.UTF_8);
                            String status = mLPAImpl.disableProfile(iccid);
                            pd.dismiss();

                            byte[] buffer = Utils.hexStringToByteArray(Utils.toHex(status, status.length()));
                            sendResponseToCompanion(BluetoothTLV.PROFILE_DISABLE, buffer);

                            profileList = mLPAImpl.getProfilesList();
                            if (profileList == null) {
                                profileList = new ArrayList<>();
                            }

                            // Update UI on UI Thread
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateUI();
                                }
                            });
                        }
                    }).start();
                } else {
                    byte[] errorBuffer = new byte[]{FAILED};
                    sendResponseToCompanion(BluetoothTLV.PROFILE_DISABLE, errorBuffer);
                }

                break;
            }

            case BluetoothTLV.PROFILE_DELETE: {
                System.arraycopy(command, 1 + lenSize, value, 0, BluetoothTLV.getLength(command));
                if (mLPAImpl.isConnected()) {
                    final ProgressDialog pd = Utils.showProgressDialog(MainActivity.this,
                            getString(R.string.app_name),
                            getString(R.string.progress_dialog_delete_profile));

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String iccid = new String(value, StandardCharsets.UTF_8);
                            if (profileList.get(0).getState().equals("Enabled")) {
                                mLPAImpl.disableProfile(iccid);
                            }

                            String status = mLPAImpl.deleteProfile(iccid);
                            mLPAImpl.sendPendingNotification();
                            pd.dismiss();

                            byte[] buffer = Utils.hexStringToByteArray(Utils.toHex(status, status.length()));
                            sendResponseToCompanion(BluetoothTLV.PROFILE_DELETE, buffer);

                            // Refresh the profile information
                            profileList = mLPAImpl.getProfilesList();
                            if (profileList == null) {
                                profileList = new ArrayList<>();
                            }

                            // Update UI on UI Thread
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateUI();
                                }
                            });
                        }
                    }).start();
                } else {
                    byte[] errorBuffer = new byte[]{FAILED};
                    sendResponseToCompanion(BluetoothTLV.PROFILE_DELETE, errorBuffer);
                }

                break;
            }
        }
    }

    @Override
    public void manageProfile(int position) {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        intent.putExtra(SettingsActivity.EXTRA_PROFILE, profileList.get(position));
        startActivityForResult(intent, REQUEST_SETTINGS_PROFILE);
    }
}
