package com.nxp.euicc.wearableapp.bluetooth;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;

import java.util.UUID;

public class CustomProfile {
    private static final String TAG = CustomProfile.class.getSimpleName();

    public static UUID qpp_Service = UUID.fromString("0000fee9-0000-1000-8000-00805f9b34fb");
    public static UUID qpp_CharWrite = UUID.fromString("d44bc439-abfd-45a2-b575-925416129600");

    /* Mandatory Client Characteristic Config Descriptor */
    public static UUID CLIENT_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    /**
     * Return a configured {@link BluetoothGattService} instance for the
     * Current Time Service.
     */
    public static BluetoothGattService createCustomService() {
        BluetoothGattService service = new BluetoothGattService(qpp_Service,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);

        BluetoothGattCharacteristic writeCharacteristic = new BluetoothGattCharacteristic(qpp_CharWrite,
                //Read/Write characteristic, supports notifications
                BluetoothGattCharacteristic.PROPERTY_WRITE | BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                BluetoothGattCharacteristic.PERMISSION_WRITE);

        BluetoothGattDescriptor configDescriptor = new BluetoothGattDescriptor(CLIENT_CONFIG,
                //Read/write descriptor
                BluetoothGattDescriptor.PERMISSION_READ | BluetoothGattDescriptor.PERMISSION_WRITE);
        writeCharacteristic.addDescriptor(configDescriptor);

        service.addCharacteristic(writeCharacteristic);

        return service;
    }
}
