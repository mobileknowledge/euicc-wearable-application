package com.nxp.euicc.wearableapp.eUICC;

import android.content.Context;
import android.os.SystemClock;
import android.util.Log;

import com.nxp.euicc.models.Profile;
import com.nxp.euicc.wearableapp.interfaces.LPAInterface;
import com.nxp.euicc.wearableapp.utils.Utils;
import com.truphone.lpa.impl.LocalProfileAssistantImpl;
import com.truphone.lpa.impl.ProfileKey;
import com.truphone.lpa.progress.DownloadProgress;
import com.truphone.lpad.progress.Progress;

import org.simalliance.openmobileapi.SEService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LPAImpl implements LPAInterface {

    private static String TAG = "LPAImpl";

    private Context mContext;
    private SEService mSEService;
    private ApduChannelImp mApduChannel;
    private LocalProfileAssistantImpl localProfileAssistant;

    // Used to know whether setting the communication with LPA Applet was correct
    private boolean isConnected = false;

    // Make LPAImpl Singleton
    private static LPAImpl INSTANCE = null;

    public static LPAImpl getInstance (Context context,  SEService seService) {
        Log.d(TAG, "Get LPAImpl instance");
        if (INSTANCE == null) {
            Log.d(TAG, "Create LPAImpl instance");
            INSTANCE = new LPAImpl(context, seService);
        } else {
            Log.d(TAG, "LPAImpl already exists");
        }

        return INSTANCE;
    }

    private LPAImpl(Context context, SEService seService) {
        mContext = context;
        mSEService = seService;
        mApduChannel = new ApduChannelImp(mSEService);
    }

    @Override
    public void connect() {
        if (mApduChannel != null && mApduChannel.CreateApduSession()) {
            Log.i(TAG, "Create LPA Assistant");
            localProfileAssistant = new LocalProfileAssistantImpl(mApduChannel, "https://127.0.0.1");
            try {
                Log.i(TAG, "Open APDU Channel");
                mApduChannel.openAPDUChannel();
                localProfileAssistant.SetDefaultRspServer();
                mApduChannel.closeAPDUChannel();

                isConnected = true;
            } catch (Exception e) {
                Log.i(TAG, "SetDefaultRspServer Failed, Exception: " + e.getMessage());
                isConnected = false;
            }
        }
    }

    @Override
    public boolean isConnected() {
        return mSEService.isConnected() && isConnected;
    }

    @Override
    public void close() {
        if (mSEService != null) {
            mSEService.shutdown();
        }

        INSTANCE = null;
    }

    @Override
    public String sendPendingNotification() {
        String sendPendingNotificationStatus = "1";

        try {
            if(Utils.isNetworkConnected(mContext)) {
                boolean bStatus = mApduChannel.openAPDUChannel();
                if (!bStatus) {
                    SystemClock.sleep(100);
                    mApduChannel.openAPDUChannel();
                }

                localProfileAssistant.processPendingNotifications();
                mApduChannel.closeAPDUChannel();

                // Send pending notifications was successful
                sendPendingNotificationStatus = "0";
            }
        } catch (final Exception e) {
            Log.e(TAG, "Send Pending Notification failure, Exception: " + e.getMessage());
        }

        Log.d(TAG, "LPA sendPendingNotificationStatus: " + sendPendingNotificationStatus);
        return sendPendingNotificationStatus;
    }

    @Override
    public String enableProfile(String iccid) {
        String enableProfileStatus = "1";

        try {
            boolean bStatus = mApduChannel.openAPDUChannel();
            if (!bStatus) {
                SystemClock.sleep(100);
                mApduChannel.openAPDUChannel();
            }

            enableProfileStatus = localProfileAssistant.enableProfile(iccid, new Progress());
            mApduChannel.closeAPDUChannel();
        } catch (Exception e) {
            Log.e(TAG, iccid + " Profile Enabling Failed, Exception ::" + e.getMessage());
        }

        Log.d(TAG, "LPA enableProfileStatus: " + enableProfileStatus);
        return enableProfileStatus;
    }

    @Override
    public String disableProfile(String iccid) {
        String disableProfileStatus = "1";

        try {
            boolean bStatus = mApduChannel.openAPDUChannel();
            if (!bStatus) {
                SystemClock.sleep(100);
                mApduChannel.openAPDUChannel();
            }

            disableProfileStatus = localProfileAssistant.disableProfile(iccid, new Progress());
            mApduChannel.closeAPDUChannel();
        } catch (Exception e) {
            Log.e(TAG, iccid + " Profile Disabling Failed, Exception ::" + e.getMessage());
        }

        Log.d(TAG, "LPA disableProfileStatus: " + disableProfileStatus);
        return disableProfileStatus;
    }

    @Override
    public String deleteProfile(String iccid) {
        String deleteProfileStatus = "1";

        try {
            if(Utils.isNetworkConnected(mContext)) {
                boolean bStatus = mApduChannel.openAPDUChannel();
                if (!bStatus) {
                    SystemClock.sleep(100);
                    mApduChannel.openAPDUChannel();
                }
                deleteProfileStatus = localProfileAssistant.deleteProfile(iccid, new Progress());
                mApduChannel.closeAPDUChannel();
            }
        } catch (Exception e) {
            Log.e(TAG, iccid + " Profile Delete Failed, Exception: " + e.getMessage());
        }

        Log.d(TAG, "LPA deleteProfileStatus: " + deleteProfileStatus);
        return deleteProfileStatus;
    }

    @Override
    public String downloadProfile(String smdpAddress, String activationCode, String confirmationCode) {
        String downloadProfileStatus = "1";

        try {
            if(Utils.isNetworkConnected(mContext)) {
                mApduChannel.openAPDUChannel();
                localProfileAssistant.processSetDefaultSMDPAddress(Utils.toHex(smdpAddress, smdpAddress.length()));
                localProfileAssistant.downloadProfile(smdpAddress, activationCode, confirmationCode, new DownloadProgress());
                localProfileAssistant.processPendingNotifications();
                mApduChannel.closeAPDUChannel();

                // Download profile was successful
                downloadProfileStatus = "0";
            }
        } catch (Exception e) {
            Log.e(TAG, "Download Profile failure, Exception: " + e.getMessage());
        }

        return downloadProfileStatus;
    }

    @Override
    public String setProfileNickName(String iccid, String NickName) {
        String setProfileNickNameStatus = "1";

        try {
            boolean bStatus = mApduChannel.openAPDUChannel();
            if (!bStatus) {
                SystemClock.sleep(100);
                mApduChannel.openAPDUChannel();
            }

            setProfileNickNameStatus = localProfileAssistant.processSetProfileNickName(iccid, NickName);
            mApduChannel.closeAPDUChannel();
        } catch (final Exception e) {
            Log.e(TAG, iccid + "Set Profile NickName failure, Exception: " + e.getMessage());
        }

        Log.d(TAG, "LPA setProfileNickNameStatus: " + setProfileNickNameStatus);
        return setProfileNickNameStatus;
    }

    @Override
    public String getEID() {
        String eid;

        mApduChannel.openAPDUChannel();
        eid = localProfileAssistant.getEID();
        mApduChannel.closeAPDUChannel();

        Log.d(TAG, "LPA EID: " + eid);
        return eid;
    }

    @Override
    public List<Profile> getProfilesList() {
        List<Profile> profileList = null;

        try {
            boolean bStatus = mApduChannel.openAPDUChannel();
            if (!bStatus) {
                SystemClock.sleep(100);
                mApduChannel.openAPDUChannel();
            }

            List<Map<String, String>> profileMapList = localProfileAssistant.getProfiles();
            mApduChannel.closeAPDUChannel();

            // Parse map to Profile list
            profileList = new ArrayList<>();
            for (int i = 0; i < profileMapList.size(); i++) {
                final Map<String, String> myMap = profileMapList.get(i);
                final Profile profile = new Profile();

                for (final Map.Entry<String, String> entrySet : myMap.entrySet()) {
                    if (entrySet.getKey().equals(ProfileKey.ICCID.toString())) {
                        profile.setIccid(entrySet.getValue());
                        Log.d(TAG, "iccid = " + profile.getIccid());
                    }

                    if (entrySet.getKey().equals(ProfileKey.NAME.toString())) {
                        profile.setName(entrySet.getValue());
                        Log.d(TAG, "name = " + profile.getName());
                    }

                    if (entrySet.getKey().equals(ProfileKey.PROFILE_CLASS.toString())) {
                        // Map Profile Class to corresponding value
                        switch (entrySet.getValue()) {
                            case "0":
                                profile.setProfileClass("Test");
                                break;

                            case "1":
                                profile.setProfileClass("Provisioning");
                                break;

                            case "2":
                                profile.setProfileClass("Operational");
                                break;

                            default:
                                profile.setProfileClass("Unknown");
                                break;
                        }

                        Log.d(TAG, "profileClass = " + profile.getProfileClass());
                    }

                    if (entrySet.getKey().equals(ProfileKey.PROFILE_ICON.toString())) {
                        profile.setProfileIcon(entrySet.getValue());
                        Log.d(TAG, "profileIcon = " + profile.getProfileIcon());
                    }

                    if (entrySet.getKey().equals(ProfileKey.PROFILE_NICKNAME.toString())) {
                        profile.setProfileNickname(entrySet.getValue());
                        Log.d(TAG, "profileNickname = " + profile.getProfileNickname());
                    }

                    if (entrySet.getKey().equals(ProfileKey.PROVIDER_NAME.toString())) {
                        profile.setProviderName(entrySet.getValue());
                        Log.d(TAG, "providerName = " + profile.getProviderName());
                    }

                    if (entrySet.getKey().equals(ProfileKey.STATE.toString())) {
                        profile.setState(entrySet.getValue());
                        Log.d(TAG, "state = " + profile.getState());
                    }
                }

                profileList.add(profile);
            }
        } catch (final Exception e) {
            Log.v(TAG, "Get Profile failure, Exception ::" + e.getMessage());
        }

        if(profileList != null) {
            Log.d(TAG, "Number of profiles retrieved from eUICC: " + profileList.size());
        } else {
            Log.d(TAG, "profileList is null");
        }

        return profileList;
    }
}