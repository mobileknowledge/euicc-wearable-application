package com.nxp.euicc.wearableapp.actitivies;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nxp.euicc.models.Profile;
import com.nxp.euicc.wearableapp.R;
import com.nxp.euicc.wearableapp.eUICC.LPAImpl;
import com.nxp.euicc.wearableapp.utils.Utils;

public class SettingsActivity extends WearableActivity {

    public String TAG = SettingsActivity.class.getName();

    public static final String EXTRA_PROFILE = "com.nxp.euicc.wearableapp.PROFILE";

    private Button enableButton;
    private Button deleteButton;

    private LPAImpl mLPAImpl;
    private Profile mProfile;

    private TextView profileTitle;
    private TextView profileName;
    private ImageView profileIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mLPAImpl = LPAImpl.getInstance(getApplicationContext(), null);
        mProfile = (Profile) getIntent().getSerializableExtra(EXTRA_PROFILE);

        // Do not enforce profile reading back in MainActivity if not needed
        setResult(Activity.RESULT_CANCELED);

        initViews();
    }

    private void initViews() {

        profileTitle = findViewById(R.id.profile_title);
        profileName = findViewById(R.id.profile_name);
        profileIcon = findViewById(R.id.profile_icon);
        enableButton = findViewById(R.id.button_enable);
        deleteButton = findViewById(R.id.button_delete);

        if (mProfile != null) {
            profileTitle.setText(getString(R.string.profile_registered));
            profileName.setText(mProfile.getName());
            profileIcon.setImageBitmap(Utils.getProfileIcon(getApplicationContext(), mProfile));

            // Refer activation status
            if (mProfile.getState().equals("Enabled")) {
                profileName.setTextColor(Color.GREEN);
            } else {
                profileName.setTextColor(Color.RED);
            }
        }

        enableButton.setOnClickListener(view -> {
            if (mProfile != null) {
                if (mProfile.getState().equals("Enabled")) {
                    disableProfile(mProfile);
                } else {
                    enableProfile(mProfile);
                }
            } else {
                Log.e(TAG, "There was an error receiving profile information");
            }
        });

        deleteButton.setOnClickListener(view -> {
            if (mProfile != null) {
                deleteProfile(mProfile);
            } else {
                Log.e(TAG, "There was an error receiving profile information");
            }
        });

        if (mProfile != null) {
            if (mProfile.getState().equalsIgnoreCase("Enabled")) {
                enableButton.setText("Disable");
            } else {
                enableButton.setText("Enable");
            }
        }
    }

    private void enableProfile(Profile profile) {
        if (mLPAImpl.isConnected()) {
            setResult(Activity.RESULT_OK);
            final ProgressDialog pd = Utils.showProgressDialog(SettingsActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.progress_dialog_enable_profile));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    //heavy job here
                    try {
                        String status = mLPAImpl.enableProfile(profile.getIccid());
                        pd.dismiss();
                        if (status.equals("0")) {
                            Log.v(TAG, profile.getIccid() + " profile enabled!");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    enableButton.setText(getString(R.string.button_disable));
                                    profileName.setTextColor(Color.GREEN);
                                    mProfile.setState("Enabled");
                                    Utils.showDialog(SettingsActivity.this,
                                            getString(R.string.app_name),
                                            getString(R.string.dialog_profile_enable_success),
                                            getString(R.string.dialog_accept),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Do nothing
                                                }
                                            });
                                }
                            });
                        } else {
                            Log.v(TAG, profile.getIccid() + " enable profile Failed with status: " + status);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    enableButton.setText("Enable");
                                    Utils.showDialog(SettingsActivity.this,
                                            getString(R.string.app_name),
                                            getString(R.string.dialog_profile_enable_error),
                                            getString(R.string.dialog_accept),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Do nothing
                                                }
                                            });
                                }
                            });
                        }
                    } catch (Exception e) {
                        Log.v(TAG, profile.getIccid() + " enable profile Failed, Exception: " + e.getMessage());
                        pd.dismiss();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.showDialog(SettingsActivity.this,
                                        getString(R.string.app_name),
                                        getString(R.string.dialog_lpa_error),
                                        getString(R.string.dialog_accept),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // Do nothing
                                            }
                                        });
                            }
                        });
                    }
                }
            }).start();
        } else {
            Utils.showDialog(SettingsActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.dialog_lpa_error),
                    getString(R.string.dialog_accept),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                        }
                    });
        }
    }

    private void disableProfile(Profile profile) {
        if (mLPAImpl.isConnected()) {
            setResult(Activity.RESULT_OK);
            final ProgressDialog pd = Utils.showProgressDialog(SettingsActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.progress_dialog_disable_profile));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    //heavy job here
                    try {
                        String status = mLPAImpl.disableProfile(profile.getIccid());
                        pd.dismiss();
                        if (status.equals("0")) {
                            Log.v(TAG, profile.getIccid() + " profile disabled!");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    enableButton.setText(getString(R.string.button_enable));
                                    profileName.setTextColor(Color.RED);
                                    mProfile.setState("Disabled");
                                    Utils.showDialog(SettingsActivity.this,
                                            getString(R.string.app_name),
                                            getString(R.string.dialog_profile_disable_success),
                                            getString(R.string.dialog_accept),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Do nothing
                                                }
                                            });
                                }
                            });
                        } else {
                            Log.v(TAG, profile.getIccid() + " disable profile Failed with status: " + status);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Utils.showDialog(SettingsActivity.this,
                                            getString(R.string.app_name),
                                            getString(R.string.dialog_profile_disable_error),
                                            getString(R.string.dialog_accept),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Do nothing
                                                }
                                            });
                                }
                            });
                        }
                    } catch (Exception e) {
                        Log.v(TAG, profile.getIccid() + " disable profile Failed, Exception: " + e.getMessage());
                        pd.dismiss();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.showDialog(SettingsActivity.this,
                                        getString(R.string.app_name),
                                        getString(R.string.dialog_lpa_error),
                                        getString(R.string.dialog_accept),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // Do nothing
                                            }
                                        });
                            }
                        });
                    }
                }
            }).start();
        } else {
            Utils.showDialog(SettingsActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.dialog_lpa_error),
                    getString(R.string.dialog_accept),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                        }
                    });
        }
    }

    private void deleteProfile(final Profile profile) {
        if (mLPAImpl.isConnected()) {
            setResult(Activity.RESULT_OK);
            final ProgressDialog pd = Utils.showProgressDialog(SettingsActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.progress_dialog_delete_profile));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    //heavy job here
                    try {
                        if (profile.getState().equals("Enabled")) {
                            mLPAImpl.disableProfile(profile.getIccid());
                        }

                        String status = mLPAImpl.deleteProfile(profile.getIccid());
                        mLPAImpl.sendPendingNotification();
                        pd.dismiss();
                        if (status.equals("0")) {
                            Log.v(TAG, profile.getIccid() + " profile deleted!");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Utils.showDialog(SettingsActivity.this,
                                            getString(R.string.app_name),
                                            getString(R.string.dialog_profile_delete_success),
                                            getString(R.string.dialog_accept),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Profile is deleted, nothing else we can do here
                                                    finish();
                                                }
                                            });
                                }
                            });
                        } else {
                            Log.v(TAG, profile.getIccid() + " delete profile failed with status: " + status);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Utils.showDialog(SettingsActivity.this,
                                            getString(R.string.app_name),
                                            getString(R.string.dialog_profile_delete_error),
                                            getString(R.string.dialog_accept),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Do nothing
                                                }
                                            });
                                }
                            });
                        }
                    } catch (Exception e) {
                        Log.v(TAG, profile.getIccid() + " profile deletion failed, Exception: " + e.getMessage());
                        pd.dismiss();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.showDialog(SettingsActivity.this,
                                        getString(R.string.app_name),
                                        getString(R.string.dialog_lpa_error),
                                        getString(R.string.dialog_accept),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // Do nothing
                                            }
                                        });
                            }
                        });
                    }
                }
            }).start();
        } else {
            Utils.showDialog(SettingsActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.dialog_lpa_error),
                    getString(R.string.dialog_accept),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                        }
                    });
        }
    }
}
