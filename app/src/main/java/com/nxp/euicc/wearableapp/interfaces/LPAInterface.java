package com.nxp.euicc.wearableapp.interfaces;

import com.nxp.euicc.models.Profile;

import java.util.List;

public interface LPAInterface {

    /// Returns the Boolean value indicating the connected or disconnected status with the eUICC
    void connect();

    /// Returns the Boolean value indicating the connected or disconnected status with the eUICC
    boolean isConnected();

    /// Retrieves the EID
    String getEID();

    /// Retrieves the list of available profiles
    List<Profile> getProfilesList();

    /// Download the profile from the QR code
    String downloadProfile(String smdpAddress, String activationCode, String confirmationCode) throws Exception;

    /// Modifies the profile Nickname
    String setProfileNickName(String iccid, String NickName);

    /// Enables the profile
    String enableProfile(String iccid);

    /// Disables the profile
    String disableProfile(String iccid);

    /// Performs the deletion of the profile
    String deleteProfile(String iccid);

    /// Sends the deletion of the profile
    String sendPendingNotification();

    /// Releases SEService allocated resources
    void close();
}
