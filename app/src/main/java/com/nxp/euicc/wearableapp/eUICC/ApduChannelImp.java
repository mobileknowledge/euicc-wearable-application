package com.nxp.euicc.wearableapp.eUICC;

import android.os.SystemClock;
import android.util.Log;

import com.nxp.euicc.wearableapp.utils.Utils;
import com.truphone.lpa.ApduChannel;
import com.truphone.lpa.ApduTransmittedListener;

import org.simalliance.openmobileapi.Channel;
import org.simalliance.openmobileapi.Reader;
import org.simalliance.openmobileapi.SEService;
import org.simalliance.openmobileapi.Session;

import java.io.Serializable;
import java.util.List;

public class ApduChannelImp implements ApduChannel, Serializable {

    private static String TAG = "ApduChannelImp";

    private SEService mseService;
    private Channel mChannel;
    private Session mSession;
    byte[] appletId = {(byte) 0xA0, (byte) 0x00, (byte) 0x00, (byte) 0x05, (byte) 0x59, (byte) 0x10, (byte) 0x10, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0x89, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00};

    public ApduChannelImp(SEService seService) {
        mseService = seService;
    }

    public String transmitAPDU(String apdu) {

        byte[] response = {0x6F, 0x00};

        Log.v(TAG, "transmitAPDU, cmd :" + apdu);

        try {
            response = mChannel.transmit(Utils.hexStringToByteArray(apdu));

        } catch (Exception e) {
            Log.e(TAG, "Error occured:", e);
            return Utils.byteArrayToHex(response);
        }
        Log.v(TAG, "transmitAPDU rsp :" + Utils.byteArrayToHex(response));

        return Utils.byteArrayToHex(response);
    }

    public void sendStatus() {

    }

    public void setApduTransmittedListener(ApduTransmittedListener apduTransmittedListener) {

    }

    public void removeApduTransmittedListener(ApduTransmittedListener apduTransmittedListener) {

    }

    public String transmitAPDUS(List<String> apdus) {
        Log.v(TAG, "transmitAPDUS Enter:");
        String Rsp = null;
        for (int i = 0; i < apdus.size(); i++) {
            Rsp = transmitAPDU(apdus.get(i));
        }
        Log.v(TAG, "transmitAPDUS Exit:");
        return Rsp;
    }

    public boolean CreateApduSession() {
        Log.i(TAG, "CreateApduSession...");
        boolean bStatus = false;
        try {
            Log.i(TAG, "Retrieve available readers...");
            Reader[] readers = mseService.getReaders();
            for (int i = 0; i < readers.length; i++) {
                Log.i(TAG, "Reader[" + i + "]" + readers[i].getName());
                if (readers[i].getName().contains("SIM"))
                    bStatus = true;
            }
            if (bStatus) {
                Log.i(TAG, "Create Session from the first reader...");
                mSession = readers[0].openSession();
            }
        } catch (Exception e) {
            Log.e(TAG, "CreateApduSession Exception ::" + e.getMessage());
            bStatus = false;
            e.printStackTrace();
        }
        return bStatus;
    }

    public boolean openAPDUChannel() {
        boolean bStatus = false;
        boolean retry = true;
        int retryCount = 0;
        do {
            try {
                Log.i(TAG, "openLogicalChannel Enter");
                if (mChannel != null && !(mChannel.isClosed())) {
                    mChannel.close();
                }
                mChannel = mSession.openLogicalChannel(appletId);
                if (mChannel == null) {
                    retryCount++;
                    SystemClock.sleep(20);
                } else {
                    retry = false;
                    bStatus = true;
                }
            } catch (Exception e) {
                Log.e(TAG, "openLogicalChannel Exception ::" + e.getMessage());
                retry = true;
                retryCount++;
                SystemClock.sleep(20);
            }
        } while (retry && retryCount < 5);
        return bStatus;
    }

    public void closeAPDUChannel() {
        try {
            Log.i(TAG, "closeLogicalChannel Enter");
            if (mChannel != null && !(mChannel.isClosed())) {
                mChannel.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "closeLogicalChannel Exception ::" + e.getMessage());
        }
    }
}
