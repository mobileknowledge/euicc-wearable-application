package com.nxp.euicc.wearableapp.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;

import com.nxp.euicc.wearableapp.utils.Utils;
import com.nxp.euicc.wearableapp.interfaces.BleInterface;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static android.content.Context.BLUETOOTH_SERVICE;

public class BluetoothServer implements BleInterface {
    private static final String TAG = BluetoothServer.class.getSimpleName();

    /* Bluetooth API */
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothManager mBluetoothManager;
    private BluetoothGattServer mBluetoothGattServer;
    private BluetoothLeAdvertiser mBluetoothLeAdvertiser;

    /* Collection of notification subscribers */
    private Set<BluetoothDevice> mRegisteredDevices = new HashSet<>();

    private Context context;
    public static final int serverBufferSize = 20;

    private boolean isConnected = false;
    private boolean qppSendDataState = false;
    private SendThread sendDataThread = null;

    private BluetoothGattService customService;
    private Context mContext;

    public BluetoothProcessCommandListener mBluetoothProcessCommandListener;

    // Buffer, where I get all the command from client
    private byte[] mBufferDataCmd = null;
    private byte[] mStatusData = null;

    private int mBufferOffset = 0;
    private int mExpectedSize = 0;

    public BluetoothServer(Context context, BluetoothProcessCommandListener processCommandListener) {
        this.mContext = context;
        this.mBluetoothManager = (BluetoothManager) mContext.getSystemService(BLUETOOTH_SERVICE);
        this.mBluetoothAdapter = mBluetoothManager.getAdapter();
        this.mBluetoothProcessCommandListener = processCommandListener;

        Log.d("BluetoothServer", "BluetoothServer object created");

        // Set the callback to receive data
        //receiveDataCallback();
    }

    public void close() {
        stopLeServer();
        stopLeAdvertising();

        if (mBluetoothGattServer != null) {
            mBluetoothGattServer.close();
            mBluetoothGattServer = null;
        }
    }

    /**
     * Initialize the GATT server instance with the services/characteristics
     * from the Time Profile.
     */
    public void startLeServer() {
        Log.d("BluetoothServer", "start Le Server");

        mBluetoothGattServer = mBluetoothManager.openGattServer(mContext, mGattServerCallback);
        if (mBluetoothGattServer == null) {
            Log.w(TAG, "Unable to create GATT server");
            return;
        }

        customService = CustomProfile.createCustomService();
        boolean startService = mBluetoothGattServer.addService(customService);
        Log.d(TAG, "BluetoothServer startServer " + startService);
    }

    /**
     * Shut down the GATT server.
     */
    public void stopLeServer() {
        Log.w(TAG, "Stop Le server");
        if (mBluetoothGattServer == null) return;

        //mBluetoothGattServer.removeService(customService);
        mBluetoothGattServer.close();
    }

    @Override
    public void startLeAdvertising() {
        Log.d("BluetoothServer", "StartAdvertising from server");

        mBluetoothLeAdvertiser = mBluetoothAdapter.getBluetoothLeAdvertiser();
        if (mBluetoothLeAdvertiser == null) {
            Log.w(TAG, "Failed to create advertiser");
            return;
        }

        AdvertiseSettings settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setConnectable(true)
                .setTimeout(0)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                .build();

        AdvertiseData data = new AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .setIncludeTxPowerLevel(false)
                .addServiceUuid(new ParcelUuid(CustomProfile.qpp_Service))
                .build();

        mBluetoothLeAdvertiser
                .startAdvertising(settings, data, mAdvertiseCallback);
    }

    @Override
    public void stopLeAdvertising() {
        if (mBluetoothLeAdvertiser == null) return;

        mBluetoothLeAdvertiser.stopAdvertising(mAdvertiseCallback);
    }

    @Override
    public boolean isEnabled() {
        return mBluetoothAdapter.isEnabled();
    }

    public void transmit(byte[] data) {
        Log.d(TAG, "Data to transmit: " + Utils.byteArrayToHex(data));
        if (isConnected) {
            sendDataThread = new SendThread(data, true);
            sendDataThread.start();
        }
    }


    /**
     * Callback to receive information about the advertisement process.
     */
    private AdvertiseCallback mAdvertiseCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            //Toast.makeText(mContext, "BLE Advertise Started!", Toast.LENGTH_LONG).show();
            Log.i(TAG, "LE Advertise Started." + settingsInEffect.toString());
        }

        @Override
        public void onStartFailure(int errorCode) {
            //Toast.makeText(mContext, "BLE Advertise Failed!", Toast.LENGTH_LONG).show();
            Log.w(TAG, "LE Advertise Failed: " + errorCode);
        }
    };

    /**
     * Send a custom service notification to any devices that are subscribed
     * to the characteristic.
     */
    private void notifyRegisteredDevices(byte[] data) {
        Log.i(TAG, "notifyRegisteredDevices " + Utils.byteArrayToHex(data));

        if (mRegisteredDevices.isEmpty()) {
            Log.i(TAG, "No subscribers registered");
            return;
        }

        Log.i(TAG, "Sending update to " + mRegisteredDevices.size() + " subscribers");
        for (BluetoothDevice device : mRegisteredDevices) {
            BluetoothGattCharacteristic customCharacteristic = mBluetoothGattServer
                    .getService(CustomProfile.qpp_Service)
                    .getCharacteristic(CustomProfile.qpp_CharWrite);
            customCharacteristic.setValue(data);
            mBluetoothGattServer.notifyCharacteristicChanged(device, customCharacteristic, false);
        }

        sendDataThread.setSendData(true);
    }

    /**
     * Listener that catches BLE info sent from Companion App and processes it
     */
    public void onDataReceived (byte[] status) {
        Log.d(TAG, "BLE Message received " + Utils.byteArrayToHex(status));

        int bytesToCopy = 0;

        // This is the first message that I receive for this command
        if (mBufferOffset == 0) {
            // This is the total length of the command that I will receive
            mExpectedSize = BluetoothTLV.getLength(status) + BluetoothTLV.getLengthSize(status) + 1; //1 Byte for Tag
            //prepare the buffer size
            mBufferDataCmd = new byte[mExpectedSize];
            if (mExpectedSize > BluetoothServer.serverBufferSize)
                bytesToCopy = BluetoothServer.serverBufferSize;
            else
                bytesToCopy = mExpectedSize;

            // Copy the data in the first position
            System.arraycopy(status, 0, mBufferDataCmd, 0, bytesToCopy);
            mBufferOffset = bytesToCopy;
        } else {
            if (mExpectedSize - mBufferOffset > BluetoothServer.serverBufferSize)
                bytesToCopy = BluetoothServer.serverBufferSize;
            else
                bytesToCopy = mExpectedSize - mBufferOffset;

            System.arraycopy(status, 0, mBufferDataCmd, mBufferOffset, bytesToCopy);
            mBufferOffset = mBufferOffset + bytesToCopy;
        }
        // If we have received the whole command we can proceed
        if (mBufferOffset == mExpectedSize) {
            // Send callback to app on the UI Thread
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    // Send callback to app on the UI Thread
                    mBluetoothProcessCommandListener.onProcessCommand(mBufferDataCmd);

                    mBufferDataCmd = new byte[0];
                    mBufferOffset = 0;
                    mExpectedSize = 0;
                }
            });
        }
    };

    /**
     * Callback to handle incoming requests to the GATT server.
     * All read/write requests for characteristics and descriptors are handled here.
     */
    private BluetoothGattServerCallback mGattServerCallback = new BluetoothGattServerCallback() {

        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            Log.d(TAG, "onConnectionStateChange new state: " + newState + " status " + status);
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Intent broadCastIntent = new Intent();
                broadCastIntent.setAction("BLEStatusChange");
                broadCastIntent.putExtra("BLEStatus", "Connected");
                mContext.sendBroadcast(broadCastIntent);
//                new Handler(Looper.getMainLooper()).post(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(mContext,
//                                "Bluetooth connection successful!", Toast.LENGTH_LONG).show();
//                    }
//                });
                Log.i(TAG, "BluetoothDevice CONNECTED: " + device);
                mRegisteredDevices.add(device);
                isConnected = true;
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Intent broadCastIntent = new Intent();
                broadCastIntent.setAction("BLEStatusChange");
                broadCastIntent.putExtra("BLEStatus", "Disconnected");
                mContext.sendBroadcast(broadCastIntent);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext,
                                "Bluetooth connection lost!", Toast.LENGTH_LONG).show();
                    }
                });
                Log.i(TAG, "BluetoothDevice DISCONNECTED: " + device);
                //Remove device from any active subscriptions
                mRegisteredDevices.remove(device);
                isConnected = false;
            }
        }

        @Override
        public void onCharacteristicReadRequest(BluetoothDevice device, int requestId, int offset,
                                                BluetoothGattCharacteristic characteristic) {
            Log.d(TAG, "onCharacteristicReadRequest " + characteristic.getUuid());
            if (CustomProfile.qpp_CharWrite.equals(characteristic.getUuid())) {
                Log.i(TAG, "Read CurrentTime");
                mBluetoothGattServer.sendResponse(device,
                        requestId,
                        BluetoothGatt.GATT_SUCCESS,
                        0,
                        null);
            } else {
                // Invalid characteristic
                Log.w(TAG, "Invalid Characteristic Read: " + characteristic.getUuid());
                mBluetoothGattServer.sendResponse(device,
                        requestId,
                        BluetoothGatt.GATT_FAILURE,
                        0,
                        null);
            }
        }

        // The Gatt will reject Characteristic Write requests that do not have the permission set,
        // so there is no need to check inside the callback
        @Override
        public void onCharacteristicWriteRequest(BluetoothDevice device,
                                                 int requestId,
                                                 BluetoothGattCharacteristic characteristic,
                                                 boolean preparedWrite,
                                                 boolean responseNeeded,
                                                 int offset,
                                                 byte[] value) {
            super.onCharacteristicWriteRequest(device,
                    requestId,
                    characteristic,
                    preparedWrite,
                    responseNeeded,
                    offset,
                    value);

            Log.i(TAG, "onCharacteristicWriteRequest: " + characteristic.getUuid().toString());
            if (CustomProfile.qpp_CharWrite.equals(characteristic.getUuid())) {
                updateReceivedData(mBluetoothGattServer, characteristic, value);
                mBluetoothGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, null);
            }
        }

        @Override
        public void onDescriptorReadRequest(BluetoothDevice device, int requestId, int offset,
                                            BluetoothGattDescriptor descriptor) {

            Log.i(TAG, "onDescriptorReadRequest: " + descriptor.getUuid().toString());
            if (CustomProfile.CLIENT_CONFIG.equals(descriptor.getUuid())) {
                Log.d(TAG, "Config descriptor read");
                byte[] returnValue;
                if (mRegisteredDevices.contains(device)) {
                    returnValue = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;
                } else {
                    returnValue = BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE;
                }
                mBluetoothGattServer.sendResponse(device,
                        requestId,
                        BluetoothGatt.GATT_SUCCESS,
                        0,
                        returnValue);
            } else {
                Log.w(TAG, "Unknown descriptor read request");
                mBluetoothGattServer.sendResponse(device,
                        requestId,
                        BluetoothGatt.GATT_FAILURE,
                        0,
                        null);
            }
        }

        @Override
        public void onDescriptorWriteRequest(BluetoothDevice device, int requestId,
                                             BluetoothGattDescriptor descriptor,
                                             boolean preparedWrite, boolean responseNeeded,
                                             int offset, byte[] value) {
            if (CustomProfile.CLIENT_CONFIG.equals(descriptor.getUuid())) {
                if (Arrays.equals(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE, value)) {
                    Log.d(TAG, "Subscribe device to notifications: " + device);
                    mRegisteredDevices.add(device);
                } else if (Arrays.equals(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE, value)) {
                    Log.d(TAG, "Unsubscribe device from notifications: " + device);
                    mRegisteredDevices.remove(device);
                }

                if (responseNeeded) {
                    mBluetoothGattServer.sendResponse(device,
                            requestId,
                            BluetoothGatt.GATT_SUCCESS,
                            0,
                            null);
                }
            } else {
                Log.w(TAG, "Unknown descriptor write request");
                if (responseNeeded) {
                    mBluetoothGattServer.sendResponse(device,
                            requestId,
                            BluetoothGatt.GATT_FAILURE,
                            0,
                            null);
                }
            }
        }
    };

    // Interface callback for Bluetooth data read
    public interface BluetoothProcessCommandListener {
        void onProcessCommand(byte[] command);
    }

    public void updateReceivedData(BluetoothGattServer bleGattServer,
                                   BluetoothGattCharacteristic characteristic, byte[] data) {
        if (bleGattServer == null || characteristic == null) {
            Log.e(TAG, "invalid arguments");
            return;
        }

        final byte[] qppData = data;

        if (qppData != null && qppData.length > 0) {
            onDataReceived(qppData);
        }
    }

    public void updateSendData(byte[] status) {
        notifyRegisteredDevices(status);
    }

    private class SendThread extends Thread {
        byte[] data;
        boolean sendData;

        public SendThread(byte[] data, boolean send) {
            this.data = data;
            this.sendData = send;
        }

        public void setSendData(boolean send) {
            this.sendData = send;
        }

        public void run() {
            if (data == null) {
                return;
            }

            int length = data.length;
            int count = 0;
            int offset = 0;

            //Log.d(TAG, "BLE transmission starts");

            while (offset < length) {
                if (sendData == true) {
                    sendData = false;

                    if ((length - offset) < serverBufferSize)
                        count = length - offset;
                    else
                        count = serverBufferSize;

                    byte tempArray[] = new byte[count];
                    System.arraycopy(data, offset, tempArray, 0, count);
                    notifyRegisteredDevices(tempArray);
                    offset = offset + count;
                }
            }
        }
    }
}
