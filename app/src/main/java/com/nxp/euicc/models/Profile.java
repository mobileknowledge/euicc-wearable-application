package com.nxp.euicc.models;

import java.io.Serializable;

public class Profile implements Serializable {

    private String iccid;
    private String profileIcon;
    private String profileNickname;
    private String state;
    private String profileClass;
    private String providerName;
    private String name;

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getProfileIcon() {
        return profileIcon;
    }

    public void setProfileIcon(String profileIcon) {
        this.profileIcon = profileIcon;
    }

    public String getProfileNickname() {
        return profileNickname;
    }

    public void setProfileNickname(String profileNickname) {
        this.profileNickname = profileNickname;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getProfileClass() {
        return profileClass;
    }

    public void setProfileClass(String profileClass) {
        this.profileClass = profileClass;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
