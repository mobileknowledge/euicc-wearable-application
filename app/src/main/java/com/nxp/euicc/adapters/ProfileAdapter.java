package com.nxp.euicc.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.nxp.euicc.models.Profile;
import com.nxp.euicc.wearableapp.R;
import com.nxp.euicc.wearableapp.utils.Utils;

import java.util.List;

public class ProfileAdapter extends PagerAdapter {

    private final List<Profile> profileList;
    private final Context context;
    private final ProfileItemListener profileItemListener;

    public ProfileAdapter(Context context, List<Profile> profileList, ProfileItemListener profileItemListener) {
        super();

        this.profileList = profileList;
        this.context = context;
        this.profileItemListener = profileItemListener;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return profileList != null ? profileList.size() : 0;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.profile_item, collection, false);

        TextView profileTitle = layout.findViewById(R.id.profile_title);
        TextView profileName = layout.findViewById(R.id.profile_name);
        ImageView profileIcon = layout.findViewById(R.id.profile_icon);
        ImageView settingsIcon = layout.findViewById(R.id.settings_icon);

        if (profileList != null && !profileList.isEmpty()) {
            profileTitle.setText(context.getString(R.string.profile_registered));
            profileName.setText(profileList.get(position).getName());
            profileName.setVisibility(View.VISIBLE);
            profileIcon.setImageBitmap(Utils.getProfileIcon(context, profileList.get(position)));
            profileIcon.setVisibility(View.VISIBLE);

            // Refer activation status
            if (profileList.get(position).getState().equals("Enabled")) {
                profileName.setTextColor(Color.GREEN);
            } else {
                profileName.setTextColor(Color.RED);
            }

            // Let the user manage the profile
            settingsIcon.setVisibility(View.VISIBLE);
            settingsIcon.setOnClickListener(view -> {
                profileItemListener.manageProfile(position);
            });
        } else {
            profileTitle.setText(context.getString(R.string.profile_not_registered));
            profileName.setVisibility(View.GONE);
            profileIcon.setVisibility(View.GONE);
            settingsIcon.setVisibility(View.GONE);
        }

        collection.addView(layout);
        return layout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public Parcelable saveState() {
        // TODO Auto-generated method stub
        return null;
    }

    public interface ProfileItemListener {
        void manageProfile(int position);
    }
}
